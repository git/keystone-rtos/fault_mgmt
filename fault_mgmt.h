/**
 *   @file  fault_mgmt.h
 *
 *   @brief
 *      Header file for the Fault Management library. The file exposes the 
 *      data structures and exported API which are available for use by 
 *      applications.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012-2014 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**  @mainpage Fault Management Module
 *
 *   @section intro  Introduction
 *
 *   The Fault Management Module can be used to save DSP state and notify the Host
 *   when an exception occurs.  The module's APIs provide the following specific
 *   functionalities:
 *      - Notification of a local DSP core fault on all remote DSP cores through
 *        manual fault generation on remote DSP cores via the remote core's NMI.
 *      - ARM notification of a DSP core fault
 *      - Halting of all hardware data transfer mechanisms
 *          - Ability to exclude resources from the halt capability in case
 *            something the like the Linux kernel must continue to run after
 *            the DSP fault causes the IO halt code to execute.
 *      - Saving of system core register status.  The register status can be used
 *        in a core dump to extract information as to where and how the exception
 *        occurred.
 *      - Cleanup of DSP peripherals after a DSP application crash.  The peripheral
 *        resources can be excluded so that any resources used by another core
 *        (ARM Linux for example) will not be part of the cleanup process.  The
 *        cleanup allows a DSP application to be reloaded to the DSP cores after
 *        exception without a hard or soft reset of the board.  Only a DSP local
 *        reset is needed on all DSP cores..
 *
 *   The IO halt feature currently allows disabling of the following IO
 *      - All CPDMA transmit and receive channels
 *      - AIF PE (transmit) and PD (receive) channels
 *      - SGMII
 *      - EDMA3 DMA, QDMA, and INT channels
 *
 *   The exception APIs can be called from a system exception context in
 *   order to completely stop all data transfer, to preserve system crash state, and
 *   then notify all remote DSP cores and the ARM of an exception.
 *
 *
 *   The cleanup API is only available for TCI6614 devices
 *
 *   The cleanup API is called from a standalone DSP application loaded onto a DSP
 *   core after an exception has occurred and a DSP local reset is performed.  The
 *   fault management library provides the standalone DSP application in the 
 *   fault_mgmt/util directory.  On the Linux side the dsp_cleanup.sh script can be
 *   found at /sbin/dsp_cleanup.sh.  The prebuilt cleanup utility can be copied
 *   into the Linux filesystem and executed using the dsp_cleanup.sh script.  The
 *   script will handle the reset of DSPs and loading of the cleanup utility via
 *   remoteproc.  Executing the dsp_cleanup.sh script with no input parameters
 *   will list the help.  The cleanup utility will not clean a single DSP.  It
 *   assumed all DSPs will be reloaded once the cleanup is complete.
 *
 *   The cleanup API performs the following actions when cleanup for each of the
 *   following peripherals is enabled:
 *      - CPPI:       Resets CPDMA tx channels, rx channels, and rx flows
 *      - QMSS:       Disables QMSS accumulator channels and clears any pending
 *                    accumulator channel INTD interrupts.  Resets memory
 *                    regions.  Empties all QMSS queues.  Queues owned by
 *                    Linux are swept of all descriptors that originate
 *                    from memory regions inserted by a DSP application
 *      - PA:         Redownloads firmware for PDSPs 1-5.  Resets LUT2. Delets
 *                    all entries in LUT10 inserted by DSP.  Resets PA global
 *                    config to default values
 *      - EDMA3:      Disables DMA channels and clears the primary and secondary
 *                    channel events.  Disables QDMA channels and clears primary
 *                    and secondary events.  Disables interrupts and clears 
 *                    pending events
 *      - AIF2:       Resets AIF and powers down the AIF2 via the PSC
 *      - TCP3D:      Powers down TCP3D A & B via the PSC
 *      - BCP:        Powers down BCP via the PSC
 *      - FFTC:       Powers down FFTC A & B via the PSC
 *      - VCP:        Powers down the VCP via the PSC
 *      - SA:         Clears the security context cache
 *      - CIC:        Removes all CPINTC system to host interrupt mappings
 *                    whose host interrupt is routed to the DSP.  Clears all
 *                    pending CPINTC events.
 *      - Timers:     Disables and resets timers.
 *      - Semaphores: Soft resets semaphores
 *
 *   The fault_mgmt/device/[platform]/fm_device.c contains the FM global configuration
 *   parameters and the exclusion list for the device.  The global config list
 *   contains device peripheral information that could not be extracted from
 *   CSL or externally-visible LLD APIs.  The exclusion list contains peripherals
 *   resources that are in use by Linux.  These resources will not be halted or
 *   cleaned up when the IO halt and cleanup APIs are called, respectively.
 *
 */

/** @defgroup FAULT_MGMT_API   Fault Management
 */

#ifndef __FAULT_MGMT_H__
#define __FAULT_MGMT_H__

/* Standard includes */
#include <stdint.h>

/* CSL RL includes */
#include <ti/csl/cslr_cpintc.h>
#include <ti/csl/cslr_tmr.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
@defgroup FAULT_MGMT_SYMBOL                  Fault Management Defined Symbols
@ingroup FAULT_MGMT_API
*/
/**
@defgroup FAULT_MGMT_FUNCTION                Fault Management Functions
@ingroup FAULT_MGMT_API
*/
/**
@defgroup FAULT_MGMT_DATA_STRUCTURE          Fault Management Data Structures
@ingroup FAULT_MGMT_API
*/
/**
@defgroup FAULT_MGMT_OSAL_API                Fault Management OSAL API
@ingroup FAULT_MGMT_API
*/

/** @addtogroup FAULT_MGMT_SYMBOL
 @{ */

/* FM return and error codes */
/** FM successful return code */
#define FM_OK                                        0

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && !defined(DEVICE_K2L) && !defined(DEVICE_K2E))
/** FM fault cleanup successful return code */
#define FM_FAULT_CLEANUP_OK                          1

/** FM fault cleanup status base */
#define FM_STATUS_BASE                               64
/** FM fault cleanup process is being initialized */
#define FM_STATUS_CLEANUP_INITIALIZATION             (FM_STATUS_BASE+1)
/** FM fault cleanup process has entered CPDMA cleanup */
#define FM_STATUS_CLEANUP_CPDMA                      (FM_STATUS_BASE+2)
/** FM fault cleanup process has entered QMSS accumulator cleanup */
#define FM_STATUS_CLEANUP_QMSS_ACCUM                 (FM_STATUS_BASE+3)
/** FM fault cleanup process has entered PA cleanup */
#define FM_STATUS_CLEANUP_PA                         (FM_STATUS_BASE+4)
/** FM fault cleanup process has entered EDMA3 cleanup */
#define FM_STATUS_CLEANUP_EDMA3                      (FM_STATUS_BASE+5)
/** FM fault cleanup process has entered semaphore cleanup */
#define FM_STATUS_CLEANUP_SEMAPHORE                  (FM_STATUS_BASE+6)
/** FM fault cleanup process has entered QMSS queue cleanup */
#define FM_STATUS_CLEANUP_QMSS_QUEUE                 (FM_STATUS_BASE+7)
/** FM fault cleanup process has entered AIF2 cleanup */
#define FM_STATUS_CLEANUP_AIF2                       (FM_STATUS_BASE+8)
/** FM fault cleanup process has entered TCP3D cleanup */
#define FM_STATUS_CLEANUP_TCP3D                      (FM_STATUS_BASE+9)
/** FM fault cleanup process has entered BCP cleanup */
#define FM_STATUS_CLEANUP_BCP                        (FM_STATUS_BASE+10)
/** FM fault cleanup process has entered FFTC cleanup */
#define FM_STATUS_CLEANUP_FFTC                       (FM_STATUS_BASE+11)
/** FM fault cleanup process has entered VCP cleanup */
#define FM_STATUS_CLEANUP_VCP                        (FM_STATUS_BASE+12)
/** FM fault cleanup process has entered SA cleanup */
#define FM_STATUS_CLEANUP_SA                         (FM_STATUS_BASE+13)
/** FM fault cleanup process has entered CIC cleanup */
#define FM_STATUS_CLEANUP_CIC                        (FM_STATUS_BASE+14)
/** FM fault cleanup process has entered Timer cleanup */
#define FM_STATUS_CLEANUP_TIMER                      (FM_STATUS_BASE+15)
#endif /* !(K2H && K2K && K2L && K2E) */

/** FM error base */
#define FM_ERROR_BASE                                (-64)
/** EDMA3 initialization failed during io halt initialization */
#define FM_ERROR_EDMA3_INIT_FAILED                   (FM_ERROR_BASE-1)
/** CPPI initialization failed during io halt or fault cleanup */
#define FM_ERROR_CPPI_INIT_FAILED                    (FM_ERROR_BASE-2)
/** QMSS initialization failed during io halt or fault cleanup */
#define FM_ERROR_QMSS_INIT_FAILED                    (FM_ERROR_BASE-3)
/** PA initialization failed during fault cleanup */
#define FM_ERROR_PA_INIT_FAILED                      (FM_ERROR_BASE-4)
/** An exclusion list entry's specified CPDMA type is incorrect based on 
 *  fmGblCfgParams */
#define FM_ERROR_INVALID_CPDMA_IN_EXCLUSION_LIST     (FM_ERROR_BASE-5)
/** An exclusion list entry's specified EDMA3 CC number is incorrect based on 
 *  fmGblCfgParams */
#define FM_ERROR_INVALID_EDMA3_CC_IN_EXCLUSION_LIST  (FM_ERROR_BASE-6)
/** An exclusion list entry's specified LUT number is incorrect based on 
 *  fmGblCfgParams */
#define FM_ERROR_INVALID_LUT_IN_EXCLUSION_LIST       (FM_ERROR_BASE-7)
/** Could not continue with cleanup because cleanup initialization did not 
 *  complete */
#define FM_ERROR_CLEANUP_INIT_NOT_COMPLETE           (FM_ERROR_BASE-8)
/** PA's command response indicated a LUT1 index could not be removed */
#define FM_ERROR_LUT1_INDEX_NOT_REMOVED              (FM_ERROR_BASE-9)
/** CPPI initialization failed during the PA cleanup process */
#define FM_ERROR_CPPI_INIT_FAILED_DURING_PA_RECOV    (FM_ERROR_BASE-10)
/** QMSS initialization failed during the PA cleanup process */
#define FM_ERROR_QMSS_INIT_FAILED_DURING_PA_RECOV    (FM_ERROR_BASE-11)
/** CPPI channel that is to be disabled is invalid */
#define FM_ERROR_CPPI_TX_CHANNEL_INVALID             (FM_ERROR_BASE-13)
/** Pa_control returned error when creating the command needed to reset
 *  the PA global configuration to default values */
#define FM_ERROR_PASS_SETTING_DEF_GLBL_CMD           (FM_ERROR_BASE-14)
/** The command sent to PA to reset the global configuration failed */
#define FM_ERROR_PASS_GBL_DEF_CFG_NOT_SET            (FM_ERROR_BASE-15)
/** Pa_control returned error when creating the command needed to reset
 *  the PA nat-t global configuration to default values */
#define FM_ERROR_PASS_SETTING_DEF_NATT_CMD           (FM_ERROR_BASE-16)
/** The command sent to PA to reset the nat-t global configuration failed */
#define FM_ERROR_PASS_NATT_DEF_CFG_NOT_SET           (FM_ERROR_BASE-17)
/** The monolithic descriptor buffer is not large enough to fit the 
 *  LUT entry delete or reset global configuration commands sent to PA */
#define FM_ERROR_DESC_BUF_TOO_SMALL                  (FM_ERROR_BASE-18)
/** An exclusion list entry's specified CIC number is incorrect based on 
 *  fmGblCfgParams */
#define FM_ERROR_INVALID_CIC_IN_EXCLUSION_LIST       (FM_ERROR_BASE-19)
/** An exclusion list entry's specified Timer range is incorrect based on 
 *  fmGblCfgParams */
#define FM_ERROR_INVALID_TIMER_IN_EXCLUSION_LIST     (FM_ERROR_BASE-20)
/** Accumulator channel specified during INTD cleanup is not within the
 *  highAccum or loAccum ranges defined within the fmGblCfgParams */
#define FM_ERROR_INVALID_ACCUM_CH                    (FM_ERROR_BASE-21)

/**
 * @brief 
 *  Size of data section used by fault management library to store coredump data that is to be 
 *  sent to ARM.  The section size must be pre-defined at compile time since Linux Remoteproc
 *  must know the FM's coredump region location and size at the time of DSP image download.
 */
#define FAULT_MGMT_DATA_SIZE                         1024

/**
@}
*/

/** @addtogroup FAULT_MGMT_DATA_STRUCTURE
 @{ */

/**********************************************************************
 ****************** EXPORTED DATA STRUCTURES **************************
 **********************************************************************/

/**
 * @brief 
 *  This is the memory used by the fault management library to store any data that will be 
 *  sent to the fault handling module. The region location must be pre-defined at compile time 
 *  since the Linux DSP image downloader must know the FM's data dump region location and size 
 *  at the time of download.
 *
 *  The fault_mgmt_data section is placed in the ".crash" data section.  The linker must place
 *  ".crash" section in memory visible to the ARM. 
 */
extern uint8_t  fault_mgmt_data[FAULT_MGMT_DATA_SIZE];

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && !defined(DEVICE_K2L) && !defined(DEVICE_K2E))
/**
 * @brief
 *  First 32-bit word is used by FM cleanup code to report cleanup status to Host
 */
extern int32_t  fmCleanupStatus[32];
#endif /* !(K2H && K2K && K2L && K2E) */

/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/

/** 
 * @brief Fault Management return code.
 */
typedef int32_t Fm_Result;

/**
 * @brief Accumulator channel range
 */
typedef struct {
    /* Accumulator channel index start */
    uint32_t start;
    /* Accumulator channel index end */    
    uint32_t end;
} Fm_AccumChRange;

/** 
 * @brief CIC parameter structure containing information about each
 *        CIC on the device.  A pointer to an array of these structures 
 *        equal to the number of CICs on the device should be passed 
 *        as part of the fmGlobalConfigParams structure.
 */
typedef struct {
    /** Pointer to the CIC's register base address */
    volatile CSL_CPINTCRegs *cicRegs;
    /** Input system events or interrupts */
    uint32_t                 maxNumSysInt;
    /** Output host events or inputerrupts */
    uint32_t                 maxNumHostInt;
} Fm_CicParams;

/** 
 * @brief Timer parameter structure containing information about each
 *        Timer on the device.  A pointer to an array of these structures 
 *        equal to the number of Timers on the device should be passed 
 *        as part of the fmGlobalConfigParams structure.
 */
typedef struct {
    /** Pointer to the Timer's register base address */
    volatile CSL_TmrRegs *timerRegs;
} Fm_TimerParams;

/** 
 * @brief Fault Management global configuration structure
 */
typedef struct {
    /** Number of device cores to notify on exception */
    uint32_t        maxNumCores;
    /** Number of EDMA3 CCs */
    uint32_t        maxEdma3Cc;
    /** Max number of AIF PE (TX) channels */
    uint32_t        maxAifPeCh;
    /** Max number of AIF PD (RX) channels */
    uint32_t        maxAifPdCh;
    /** Max number of QMSS accumulator channels */
    uint32_t        maxQmssAccumCh;
    /** High priority accumulator channels */
    Fm_AccumChRange highAccum;
    /** Low priority accumulator channels */
    Fm_AccumChRange loAccum;
    /** Max number of PA PDSPs */
    uint32_t        maxPaPdsps;
    /** Max number of PA LUT1 entries */
    uint32_t        maxLut1Entries;
    /** Number of CICs on device */
    uint32_t        maxCic;
    /** Pointer to list of CIC param structures.  One param structure
     *  per CIC */
    Fm_CicParams   *cicParams;
    /** Number of Timers on device */
    uint32_t        maxTimers;
    /** Pointer to list of timer param structures.  One param structure
     *  per timers */
    Fm_TimerParams *timerParams;
} Fm_GlobalConfigParams;

/**
 * @brief IO halt resource exclusion types
 */
typedef enum {
    /** CPDMA RX channel */
    Fm_res_CpdmaRxCh = 0,
    /** CPDMA TX channel */
    Fm_res_CpdmaTxCh,
    /** CPDMA RX flow */
    Fm_res_CpdmaRxFlow,
    /** AIF PE (TX) channel */
    Fm_res_AifPeCh,
    /** AIF PD (RX) channel */
    Fm_res_AifPdCh,
    /** EDMA3 DMA channel */
    Fm_res_Edma3DmaCh, 
    /** EDMA3 QDMA channel */
    Fm_res_Edma3QdmaCh, 
    /** EDMA3 INT channel */
    Fm_res_Edma3IntCh,
    /** QMSS Accumulator channels */
    Fm_res_QmssAccumCh,
    /** QMSS Queues */
    Fm_res_QmssQueue,
    /** QMSS Memory Regions */
    Fm_res_QmssMemRegion,
    /** PA PDSPs */
    Fm_res_PaPdsp,
    /** PA LUT Entries */
    Fm_res_PaLutEntry,
    /** Chip Interrupt Controller Host interrupts */
    Fm_res_CicHostInt,
    /** Timers */
    Fm_res_Timer,
    /** End of exclusion array identifier */
    Fm_res_END
} Fm_ResType;

/**
 * @brief IO halt resource peripheral resource exclusion range
 */
typedef struct {
    /** Range start for which exclusion will apply */
    int32_t exStart;
    /** Range end for which exclusion will apply */
    int32_t exEnd;
} Fm_ExRange;

/**
 * @brief Describes a resource range that will be excluded from 
 *        the halt logic in the Fault_Mgmt_haltIoProcessing API
 */
typedef struct {
    /** Defines the type of resource to be excluded */
    Fm_ResType resType;
    /** Peripheral resources exclusion range */
    Fm_ExRange exRange;
    /** Excluded resource's extra parsing info as described below:
     *      #Fm_res_CpdmaRxCh
     *      #Fm_res_CpdmaTxCh
     *      #Fm_res_CpdmaRxFlow
     *          - Maps to Cppi_CpDma type
     *
     *      #Fm_res_Edma3DmaCh
     *      #Fm_res_Edma3QdmaCh
     *      #Fm_res_Edma3IntCh
     *          - Maps to CSL_InstNum EDMA3 Channel Controller (CC)
     *
     *      #Fm_res_AifPeCh
     *      #Fm_res_AifPdCh
     *      #Fm_res_QmssAccumCh
     *      #Fm_res_QmssQueue
     *      #Fm_res_QmssMemRegion
     *      #Fm_res_PaPdsp
     *      #Fm_res_Timer
     *          - NULL
     *
     *      #Fm_res_PaLutEntry
     *          - Maps to LUT 0, 1, or 2
     *
     *      #Fm_res_CicHostInt
     *          - Maps to CIC number
     */
    uint32_t   exResInfo;
} Fm_ExcludedResource;

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && !defined(DEVICE_K2L) && !defined(DEVICE_K2E))
/**
 * @brief
 *  DSP fault cleanup configuration parameters.
 */
typedef struct {
    /** Cleans CPDMA to PoR state if a non-zero value is written to this field */
    int32_t              cleanCpdma;
    /** Cleans QMSS to PoR state if a non-zero value is written to this field */
    int32_t              cleanQmss;
    /** Cleans PA to PoR state if a non-zero value is written to this field */
    int32_t              cleanPa;
    /** Cleans EDMA3 to PoR state if a non-zero value is written to this field */
    int32_t              cleanEdma3;
    /** Cleans AIF2 to PoR state if a non-zero value is written to this field */
    int32_t              cleanAif2;
    /** Powers down TCP3D if a non-zero value is written to this field */
    int32_t              cleanTcp3d;
    /** Powers down BCP if a non-zero value is written to this field */
    int32_t              cleanBcp;
    /** Powers down FFTC (A & B) if a non-zero value is written to this field */
    int32_t              cleanFftc;
    /** Powers down VCP if a non-zero value is written to this field */
    int32_t              cleanVcp;
    /** Cleans SA security context entries */
    int32_t              cleanSa;
    /** Cleans the Chip Interrupt Controllers (CIC) */
    int32_t              cleanCics;
    /** Cleans the device timers */
    int32_t              cleanTimers;
    /** List of resources to not reset when trying to cleanup from
     *  a DSP fault.  Used to avoid resetting Linux kernel features, such 
     *  as the network driver, that use may use CPDMA channels.
     *
     *  Pass NULL to reset all peripherals to their PoR state. */
    Fm_ExcludedResource *excludedResources;
} Fm_CleanupCfg;
#endif /* !(K2H && K2K && K2L && K2E) */

/**
 * @brief
 *  DSP subsystem halt configuration parameters.
 */
typedef struct {
    /** Disables the AIF2 send and receive channels if a non-zero value is written to
     *  this field */
    int32_t              haltAif2;
    /** Halts SGMII tx/rx logic if a non-zero value is written to this field */
    int32_t              haltSGMII;
    /** Halts EDMA3 channels if a non-zero value is written to this field */
    int32_t              haltEdma3;
    /** Halts CPDMA tx/rx channels if a non-zero value is written to this field */
    int32_t              haltCpdma;
    /** List of resources to exclude from IO halt feature.  Can be used
     *  to avoid halting key Linux kernel features, such as the network driver, that
     *  use may use CPDMA channels.
     *
     *  Pass NULL to disable all receive channels for all resource types. */
    Fm_ExcludedResource *excludedResources;
} Fm_HaltCfg;

/**
@}
*/

/** @addtogroup FAULT_MGMT_FUNCTION
 @{ */

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && !defined(DEVICE_K2L) && !defined(DEVICE_K2E))
/**
 *  @b Description
 *  @n  
 *      The function is used to clean the DSP peripherals to their power on reset state
 *      after a DSP crash occurs.  The reset can be augmented to ignore peripheral resources
 *      in use by the ARM (specifically, Linux).  The peripherals that are supported in 
 *      PoR cleanup are:
 *          - CPPI
 *          - QMSS
 *          - PA
 *          - EDMA3
 *          - AIF2
 *          - TCP3D
 *          - BCP
 *          - FFTC
 *          - VCP
 *          - SA
 *          - CIC
 *          - Timers
 *          - Semaphores
 *
 *  @param[in]  *fmGblCfgParams
 *      Pointer to the fault managment device specific global configuration parameters 
 *  @param[in]  *cleanupCfg
 *      Configuration structure that allows control over which peripheral's should be recovered
 *      and which peripheral resources to avoid during cleanup.
 *
 *  @retval
 *      Success - #FM_FAULT_CLEANUP_OK
 *  @retval
 *      Failure - return value <0
 */
extern Fm_Result Fault_Mgmt_faultCleanup(Fm_GlobalConfigParams *fmGblCfgParams, Fm_CleanupCfg *cleanupCfg);
#endif /* !(K2H && K2K && K2L && K2E) */

/**
 *  @b Description
 *  @n  
 *      The function is used to halt IO processing when a crash occurs.  Currently disables:
 *          - EDMA3 DMA, QDMA, and INT channels
 *          - AIF PE/PD channels
 *          - SGMII switch
 *          - CPDMA tx/rx channels
 *
 *  @param[in]  *fmGblCfgParams
 *      Pointer to the fault managment device specific global configuration parameters 
 *  @param[in]  *haltCfg
 *      Configuration structure that allows control over what portions of the system can be
 *      disabled.
 *
 *  @retval
 *      Success - #FM_OK
 *  @retval
 *      Failure - return value <0 
 */
extern Fm_Result Fault_Mgmt_haltIoProcessing(Fm_GlobalConfigParams *fmGblCfgParams, Fm_HaltCfg *haltCfg);

/**
 *  @b Description
 *  @n  
 *      The function is used to get the register status and store it in the fault management
 *      data region.
 *
 *  @retval
 *      Not Applicable    
 */
extern void Fault_Mgmt_getLastRegStatus(void);

/**
 *  @b Description
 *  @n  
 *      The function is used to notify ARM the DSP core has faulted. The fault 
 *      information collected are read by the ARM from the relevant sections. 
 *
 *  @retval
 *      Not Applicable
 */
extern void Fault_Mgmt_notify(void);

/**
 *  @b Description
 *  @n  
 *      The function is used to notify and manually fault a remote DSP core because
 *      a fault has occurred on the on the local core.  The fault on the remote
 *      core is generated by a NMI pulse to the GEM. The fault information collected 
 *      is read by the ARM from the relevant sections. 
 *
 *  @retval
 *      Not Applicable
 */
extern void Fault_Mgmt_notify_remote_core(uint32_t core_id);

/**
 *  @b Description
 *  @n  
 *      Returns the required size of the fault management data
 *      region containing the crash dump data.
 *
 *  @retval
 *      Required size of the fault management crash dump data region in bytes
 */
extern uint32_t Fault_Mgmt_getSizes(void);

/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /* __FAULT_MGMT_H__ */

