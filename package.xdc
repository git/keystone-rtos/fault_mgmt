/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the Fault Management
 *
 * Copyright (C) 2012-2015 Texas Instruments, Inc.
 *****************************************************************************/

package ti.instrumentation.fault_mgmt[1, 0, 1, 4] {
    module Settings;
}
