/**
 *   @file  fm_exclusion.c
 *
 *   @brief   
 *      Fault Management exclusion list source
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* CSL Includes */
#include <ti/csl/csl_sem.h>
#include <ti/csl/cslr.h>

/* FM API Include */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

/* FM Internal Includes */
#include <ti/instrumentation/fault_mgmt/include/fm_loc.h>
#include <ti/instrumentation/fault_mgmt/include/fm_exclusionloc.h>

/**********************************************************************
 ********************* Exclusion Globals ******************************
 **********************************************************************/


/**********************************************************************
 ******************** External Variables ******************************
 **********************************************************************/


/**********************************************************************
 ******************* Local Exclusion Functions ***********************
 **********************************************************************/

/* FUNCTION PURPOSE: Generic check if a resource is in exclusion list
 ***********************************************************************
 * DESCRIPTION: Compares input resource with the list of resources
 *              in the supplied exclusion list
 */
static uint32_t isExcludedGenericResource(Fm_ExclusionParams *exParams)
{
   int i;
  
   if (exParams->exclusionList) {
       for (i = 0; i < exParams->numListEntries; i++) {
           if (exParams->exclusionList[i].resType == exParams->resType) {
               if ((exParams->resourceNum >= exParams->exclusionList[i].exRange.exStart) && 
                   (exParams->resourceNum <= exParams->exclusionList[i].exRange.exEnd)) {
                   return (FM_TRUE);
               }
           }
       }
   }
  
   return (FM_FALSE);
}

/* FUNCTION PURPOSE: Checks if CPDMA channel is in exclusion list
 ***********************************************************************
 * DESCRIPTION: Compares input channel/flow of a given CPDMA with the
 *              list of CPDMA channels in the supplied exclusion list
 */
static uint32_t isExcludedCpdmaResource(Fm_ExclusionParams *exParams)
{
   Fm_ExclusionCpdmaParams *cpdmaParams = &(exParams->u.cpdmaParams);
   int                      i;
  
   if (exParams->exclusionList) {
       for (i = 0; i < exParams->numListEntries; i++) {
           if (exParams->exclusionList[i].resType == exParams->resType) {
               if ((cpdmaParams->dma == exParams->exclusionList[i].exResInfo) &&
                   (exParams->resourceNum >= exParams->exclusionList[i].exRange.exStart) && 
                   (exParams->resourceNum <= exParams->exclusionList[i].exRange.exEnd)) {
                   return (FM_TRUE);
               }
           }
       }
   }
  
   return (FM_FALSE);
}

/* FUNCTION PURPOSE: Checks if EDMA3 channel is in exclusion list
 ***********************************************************************
 * DESCRIPTION: Compares input channel with the list of EDMA3 channels
 *              in the supplied exclusion list
 */
static uint32_t isExcludedEdma3Resource(Fm_ExclusionParams *exParams)
{
   Fm_ExclusionEdma3Params *edma3Params = &(exParams->u.edma3Params);
   int                      i;
  
   if (exParams->exclusionList) {
       for (i = 0; i < exParams->numListEntries; i++) {
           if (exParams->exclusionList[i].resType == exParams->resType) {
               if ((edma3Params->edma3Num == exParams->exclusionList[i].exResInfo) &&
                   (exParams->resourceNum >= exParams->exclusionList[i].exRange.exStart) && 
                   (exParams->resourceNum <= exParams->exclusionList[i].exRange.exEnd)) {
                   return (FM_TRUE);
               }
           }
       }
   }
  
   return (FM_FALSE);
}

/* FUNCTION PURPOSE: Checks if LUT entry is in exclusion list
 ***********************************************************************
 * DESCRIPTION: Compares input LUT entry with the list of LUT entries
 *              in the supplied exclusion list
 */
static uint32_t isExcludedLutEntry(Fm_ExclusionParams *exParams)
{
    Fm_ExclusionLutEntryParams *lutParams = &(exParams->u.lutParams);
    int                         i;
    
    if (exParams->exclusionList) {
        for (i = 0; i < exParams->numListEntries; i++) {
            if (exParams->exclusionList[i].resType == exParams->resType) {
                if ((lutParams->lutInst == exParams->exclusionList[i].exResInfo) &&
                    (exParams->resourceNum >= exParams->exclusionList[i].exRange.exStart) && 
                    (exParams->resourceNum <= exParams->exclusionList[i].exRange.exEnd)) {
                    return (FM_TRUE);
                }
            }
        }
    }
    
    return (FM_FALSE);
}

/* FUNCTION PURPOSE: Checks if CIC host interrupt is in exclusion list
 ***********************************************************************
 * DESCRIPTION: Compares input CIC host interrupt with the list of 
 *              CIC host interrupts in the supplied exclusion list
 */
static uint32_t isExcludedCicResource(Fm_ExclusionParams *exParams)
{
    Fm_ExclusionCicParams *cicParams = &(exParams->u.cicParams);
    int                    i;
    
    if (exParams->exclusionList) {
        for (i = 0; i < exParams->numListEntries; i++) {
            if (exParams->exclusionList[i].resType == exParams->resType) {
                if ((cicParams->cic == exParams->exclusionList[i].exResInfo) &&
                    (exParams->resourceNum >= exParams->exclusionList[i].exRange.exStart) && 
                    (exParams->resourceNum <= exParams->exclusionList[i].exRange.exEnd)) {
                    return (FM_TRUE);
                }
            }
        }
    }
    
    return (FM_FALSE);
}

/**********************************************************************
 ************************** Exclusion APIs ****************************
 **********************************************************************/

/* FUNCTION PURPOSE: Validates the excluded resource list passed by the app
 ***********************************************************************
 * DESCRIPTION: Validates the exResInfo field for the different possible
 *              exclusion types that can be passed in.  Will return a -1
 *              and the exclusion list entry that failed if the exResInfo
 *              does not match what is expected.  Otherwise, returns 0 in the
 *              status field and the number of exclusion entries
 *              as the return value.
 */
uint32_t fmExclusionValidateList(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *exResList, 
                                 int32_t *status)
{
    int32_t exEntries = 0;

    *status  = FM_OK;

    if (exResList) {
        while(exResList[exEntries].resType != Fm_res_END) {
            switch(exResList[exEntries].resType) {
                case Fm_res_CpdmaRxCh:
                case Fm_res_CpdmaTxCh:
                case Fm_res_CpdmaRxFlow:
                    if (exResList[exEntries].exResInfo > CPPI_MAX_CPDMA) {
                        *status = FM_ERROR_INVALID_CPDMA_IN_EXCLUSION_LIST;
                        return(exEntries);
                    }
                    break;
                case Fm_res_Edma3DmaCh:
                case Fm_res_Edma3QdmaCh:
                case Fm_res_Edma3IntCh:
                    if (exResList[exEntries].exResInfo > fmGblCfgParams->maxEdma3Cc) {
                        *status = FM_ERROR_INVALID_EDMA3_CC_IN_EXCLUSION_LIST;
                        return(exEntries);
                    }
                    break;
                case Fm_res_PaLutEntry:
                    if (exResList[exEntries].exResInfo > 2) {
                        *status = FM_ERROR_INVALID_LUT_IN_EXCLUSION_LIST;
                        return(exEntries);
                    }
                    break;
                case Fm_res_CicHostInt:                    
                    if (exResList[exEntries].exResInfo >= fmGblCfgParams->maxCic) {
                        *status = FM_ERROR_INVALID_CIC_IN_EXCLUSION_LIST;
                        return(exEntries);
                    }
                    break;
                case Fm_res_Timer:
                    if ((exResList[exEntries].exRange.exStart < 0) ||
                        (exResList[exEntries].exRange.exStart >= fmGblCfgParams->maxTimers) ||
                        (exResList[exEntries].exRange.exEnd < 0) ||
                        (exResList[exEntries].exRange.exEnd >= fmGblCfgParams->maxTimers)) {
                        *status = FM_ERROR_INVALID_TIMER_IN_EXCLUSION_LIST;
                        return(exEntries);
                    }
                    break;
                case Fm_res_AifPeCh:
                case Fm_res_AifPdCh:
                case Fm_res_QmssAccumCh:
                case Fm_res_QmssQueue:
                case Fm_res_QmssMemRegion:
                default:
                    break;
            }
            exEntries++;
        }
    }

    return(exEntries);
}

/* FUNCTION PURPOSE: Gets the exclusion result for a resource
 ***********************************************************************
 * DESCRIPTION: Returns the exclusion result for the provided resource
 */
uint32_t fmExclusionIsExcluded(Fm_ExclusionParams *exParams)
{
    uint32_t isExcluded = FM_TRUE;
    
    switch(exParams->resType) {
        case Fm_res_CpdmaRxCh:
        case Fm_res_CpdmaTxCh:
        case Fm_res_CpdmaRxFlow:
            isExcluded = isExcludedCpdmaResource(exParams);
            break;
        case Fm_res_Edma3DmaCh:
        case Fm_res_Edma3QdmaCh:
        case Fm_res_Edma3IntCh:
            isExcluded = isExcludedEdma3Resource(exParams);
            break;
        case Fm_res_PaLutEntry:
            isExcluded = isExcludedLutEntry(exParams);
            break;
        case Fm_res_CicHostInt:
            isExcluded = isExcludedCicResource(exParams);
            break;
        case Fm_res_QmssAccumCh:
        case Fm_res_QmssQueue:
        case Fm_res_QmssMemRegion:
        case Fm_res_AifPeCh:
        case Fm_res_AifPdCh:
        case Fm_res_PaPdsp:
        case Fm_res_Timer:
            isExcluded = isExcludedGenericResource(exParams);
            break;
        case Fm_res_END:
        default:
            /* Exclude an invalid peripheral */
            break;
    }

    return (isExcluded);
}

