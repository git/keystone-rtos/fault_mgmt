/**
 *   @file  k2k/src/fm_device.c
 *
 *   @brief   
 *      This file contains the device specific configuration and initialization routines
 *      for Fault Management Module.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2011-2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* Standard Include Files */
#include <string.h>

/* CSL include */
#include <ti/csl/cslr_device.h>

/* LLD includes */
#include <ti/drv/cppi/cppi_drv.h>

/* Fault Management Include File */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

/** @addtogroup FAULT_MGMT_DATA_STRUCTURE
@{ 
*/

/** @brief FM Module initialization parameters */
Fm_GlobalConfigParams fmGblCfgParams =
{
    /** Number of cores on device to notify on exception */
    8u,
    /** Maximum number of EDMA3 CCs */
    5u,
    /** Maximum number of AIF PE channels */
    128u,
    /** Maximum number of AIF PD channels */
    128u,

    /* Below only valid for the fault cleanup feature which is not available for K2K.
     * Therefore, set remaining values to 0 */

    /** Maximum number of QMSS Accumulator channels */
    0u,
    /** High priority accumulator channels */
    { 0u, 0u},
    /** Low priority accumulator channels */
    { 0u, 0u},
    /** Maximum number of PA PDSPs */
    0u,
    /** Maximum number of LUT1 entries */
    0u,
    /** Maximum number of CICs */
    0u,
    /** Pointer to CIC params */
    NULL,
    /** Maximum number of Timers */
    0u,
    /** Pointer to Timer params */
    NULL,
};

/** @brief FM Resource exclusion list containing resources used by Linux*/
Fm_ExcludedResource linuxResources[] = {
    /** CPDMA PA Rx channels used by Linux */
    {Fm_res_CpdmaRxCh,     {    0,   23}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Rx channels used by Linux */        
    {Fm_res_CpdmaRxCh,     {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA PA Tx channels used by Linux (first range) */
    {Fm_res_CpdmaTxCh,     {    0,    6}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA PA Tx channels used by Linux (second range) */        
    {Fm_res_CpdmaTxCh,     {    8,    8}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Tx channels used by Linux */
    {Fm_res_CpdmaTxCh,     {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA PA Rx flows used by Linux (first range) */
    {Fm_res_CpdmaRxFlow,   {   22,   26}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA PA Rx flows used by Linux (second range) */
    {Fm_res_CpdmaRxFlow,   {   30,   31}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Rx flows used by Linux */
    {Fm_res_CpdmaRxFlow,   {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA XGE Rx channels used by Linux */
    {Fm_res_CpdmaRxFlow,   {    0,   15}, Cppi_CpDma_XGE_CPDMA},
    /** CPDMA XGE Tx channels used by Linux */
    {Fm_res_CpdmaRxFlow,   {    0,    7}, Cppi_CpDma_XGE_CPDMA},
    /** CPDMA XGE Rx flows used by Linux (first range) */
    {Fm_res_CpdmaRxFlow,   {    0,    0}, Cppi_CpDma_XGE_CPDMA},
    /** CPDMA XGE Rx flows used by Linux (second range) */
    {Fm_res_CpdmaRxFlow,   {    8,    8}, Cppi_CpDma_XGE_CPDMA},        
    /** End of exclusion array */
    {Fm_res_END}
};

/**
@}
*/

