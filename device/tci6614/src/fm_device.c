/**
 *   @file  tci6614/src/fm_device.c
 *
 *   @brief   
 *      This file contains the device specific configuration and initialization routines
 *      for Fault Management Module.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2011-2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* Standard Include Files */
#include <string.h>

/* CSL include */
#include <ti/csl/csl_cppi.h>
#include <ti/csl/cslr_device.h>

/* Fault Management Include File */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

/** @addtogroup FAULT_MGMT_DATA_STRUCTURE
@{ 
*/

/** Maximum number of CICs - 4 CICs on Appleton */
#define NUM_CIC           4u

/** Maximum number of Timers - 12 timers on Appleton */
#define NUM_TIMERS        12u

/** @brief FM Module CIC parameters */
Fm_CicParams cicInfo[NUM_CIC] = {
    {
        (volatile CSL_CPINTCRegs *) CSL_CP_INTC_0_REGS,
        202,
        152,
    },
    {
        (volatile CSL_CPINTCRegs *) CSL_CP_INTC_1_REGS,
        160,
        59,
    },
    {
        (volatile CSL_CPINTCRegs *) CSL_CP_INTC_2_REGS,
        64,
        42,
    },
    {
        (volatile CSL_CPINTCRegs *) CSL_CP_INTC_3_REGS,
        256,
        48,
    },
};

/** @brief FM Module Timer paramers */
Fm_TimerParams timerInfo[NUM_TIMERS] = {
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_0_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_1_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_2_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_3_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_4_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_5_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_6_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_7_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_8_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_9_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_10_REGS,
    },
    {
        (volatile CSL_TmrRegs *) CSL_TIMER_11_REGS,
    },
};

/** @brief FM Module initialization parameters */
Fm_GlobalConfigParams fmGblCfgParams =
{
    /** Number of cores on device to notify on exception */
    4u,
    /** Maximum number of EDMA3 CCs */
    3u,
    /** Maximum number of AIF PE channels */
    128u,
    /** Maximum number of AIF PD channels */
    128u,
    /** Maximum number of QMSS Accumulator channels */
    48u,
    /** High priority accumulator channels */
    { 0u, 31u},
    /** Low priority accumulator channels */
    {32u, 47u},
    /** Maximum number of PA PDSPs */
    6u,
    /** Maximum number of LUT1 entries */
    64u,
    /** Maximum number of CICs */
    NUM_CIC,
    /** Pointer to CIC params */
    &cicInfo[0],
    /** Maximum number of Timers */
    NUM_TIMERS,
    /** Pointer to Timer params */
    &timerInfo[0],
};

/** @brief FM Resource exclusion list containing resources used by Linux*/
Fm_ExcludedResource linuxResources[] = {
    /** CPDMA PA Rx channels used by Linux */
    {Fm_res_CpdmaRxCh,     {    0,   23}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Rx channels used by Linux */        
    {Fm_res_CpdmaRxCh,     {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA PA Tx channels used by Linux (first range) */
    {Fm_res_CpdmaTxCh,     {    0,    6}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA PA Tx channels used by Linux (second range) */        
    {Fm_res_CpdmaTxCh,     {    8,    8}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Tx channels used by Linux */
    {Fm_res_CpdmaTxCh,     {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA PA Rx flows used by Linux (first range) */
    {Fm_res_CpdmaRxFlow,   {   22,   23}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA PA Rx flows used by Linux (second range) */
    {Fm_res_CpdmaRxFlow,   {   30,   31}, Cppi_CpDma_PASS_CPDMA},
    /** CPDMA QMSS Rx flows used by Linux */
    {Fm_res_CpdmaRxFlow,   {    0,   11}, Cppi_CpDma_QMSS_CPDMA},
    /** CPDMA SRIO Rx flows used by Linux */
    {Fm_res_CpdmaRxFlow,   {   21,   21}, Cppi_CpDma_SRIO_CPDMA},
    /** QMSS Accumulator channels owned by Linux */
    {Fm_res_QmssAccumCh,   {   32,   35}, NULL},
    /** QMSS Queues owned by Linux */
    {Fm_res_QmssQueue,     {    0,  127}, NULL},
    {Fm_res_QmssQueue,     {  640,  648}, NULL},
    {Fm_res_QmssQueue,     {  650,  657}, NULL},
    {Fm_res_QmssQueue,     {  672,  672}, NULL},
    {Fm_res_QmssQueue,     {  800,  811}, NULL},
    {Fm_res_QmssQueue,     { 4000, 4063}, NULL},
    {Fm_res_QmssQueue,     { 8000, 8191}, NULL},     
    /** QMSS Memory regions owned by Linux */
    {Fm_res_QmssMemRegion, {   12,   12}, NULL},
    /** PA PDSPs */
    {Fm_res_PaPdsp,        {    0,    0}, NULL},
    /** PA LUT entries */
    {Fm_res_PaLutEntry,    {    0,   43}, 1},
    {Fm_res_PaLutEntry,    {   56,   63}, 1},
    /** EDMA3 CC0 channels, and interrupts */
    {Fm_res_Edma3DmaCh,    {    0,   15}, 0},
    {Fm_res_Edma3IntCh,    {    0,   15}, 0},
    /** All CIC3 host events owned by Linux */
    {Fm_res_CicHostInt,    {    0,   47}, 3},
    /** Timers 7 and 8 used by Linux for clockevent/source and watchdog, respectively */
    {Fm_res_Timer,         {    7,    8}, NULL},
    /** End of exclusion array */
    {Fm_res_END}
};

/**
@}
*/

