/*
 *  Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <string.h>

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>

#include <ti/csl/csl_chipAux.h>

/* Fault Management Include File */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

/* Remoteproc include file */
#include <ti/instrumentation/remoteproc/remoteproc.h>

/* Exclude Linux owned resources from being reset during recovery */
#define EXCLUDE_LINUX_RESOURCES_FROM_RECOVERY 1

/* Add trace buffer information to the resource table */
extern char * xdc_runtime_SysMin_Module_State_0_outbuf__A;
#define TRACEBUFADDR (uint32_t)&xdc_runtime_SysMin_Module_State_0_outbuf__A
#define TRACEBUFSIZE sysMinBufSize

#pragma DATA_SECTION(resources, ".resource_table")
#pragma DATA_ALIGN(resources, 4096)
struct rproc_resource resources[] = {
    {TYPE_TRACE, 0, TRACEBUFADDR,0,0,0, TRACEBUFSIZE, 0,
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"trace:appleton"},
};

/**********************************************************************
 ************************** External Variables ************************
 **********************************************************************/

extern Fm_GlobalConfigParams fmGblCfgParams;
extern Fm_ExcludedResource   linuxResources[];

/**********************************************************************
 ************************* Unit Test Functions ************************
 **********************************************************************/

void main(void)
{  
    Fm_CleanupCfg cleanupCfg;
    uint32_t      coreNum = CSL_chipReadDNUM();

    System_printf ("\nPrint resource table value to avoid compiler optimizing out table: %d\n", 
                   resources[0].type);
          
    memset(&cleanupCfg, 0, sizeof(cleanupCfg));

#if EXCLUDE_LINUX_RESOURCES_FROM_RECOVERY
    cleanupCfg.excludedResources = &linuxResources[0];
#else
    cleanupCfg.excludedResources = NULL;
#endif
    cleanupCfg.cleanCpdma = 1;
    cleanupCfg.cleanQmss = 1;
    cleanupCfg.cleanPa = 1;
    cleanupCfg.cleanSa = 1;
    cleanupCfg.cleanEdma3 = 1;
    cleanupCfg.cleanAif2 = 1;
    cleanupCfg.cleanTcp3d = 1;
    cleanupCfg.cleanBcp = 1;
    cleanupCfg.cleanFftc = 1;
    cleanupCfg.cleanVcp = 1;
    cleanupCfg.cleanCics = 1;
    cleanupCfg.cleanTimers = 1;
    
    Fault_Mgmt_faultCleanup(&fmGblCfgParams, &cleanupCfg);

    while(1){};
}
