/*
 *  file  fm_loc.h
 *
 *  Internal prototypes and data structures for Fault Management.
 *
 *  ============================================================================
 *      (C) Copyright 2014 - 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef FM_LOC_H_
#define FM_LOC_H_

#ifdef __cplusplus
extern "C" {
#endif

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && \
     !defined(DEVICE_K2L) && !defined(DEVICE_K2E))
#include <ti/csl/csl_cppi.h>
#else
#include <ti/drv/cppi/cppi_drv.h>
#endif

#define FM_FALSE 0
#define FM_TRUE  1

#if (defined(DEVICE_K2E) || defined(DEVICE_K2H) || \
     defined(DEVICE_K2K) || defined(DEVICE_K2L))
/* ARM Corepacs start at IPCGR8 for K2 devices */
#define FM_ARM_HOST_IPCGR_INDEX 8
#endif

/* Fault interrupt to Host is above the bits used by regular IPC. This is the
 * offset to be used for setting the IPCGRH register bit */
#define FM_HOST_IPCGR_OFFSET 8

uint32_t fmIsWirelessPeriphPoweredOnForCpdma(Cppi_CpDma dmaNum);

#ifdef __cplusplus
}
#endif

#endif /* FM_LOC_H_ */

