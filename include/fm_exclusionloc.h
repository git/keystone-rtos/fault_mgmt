/*
 *  file  fm_exclusionloc.h
 *
 *  Internal prototypes and data structures for Fault Management resource
 *  exclusion from IO halt and fault recovery features.
 *
 *  ============================================================================
 *      (C) Copyright 2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef FM_EXCLUSIONLOC_H_
#define FM_EXCLUSIONLOC_H_

#ifdef __cplusplus
extern "C" {
#endif

#if (!defined(DEVICE_K2H) && !defined(DEVICE_K2K) && !defined(DEVICE_K2L) && !defined(DEVICE_K2E)) 
/* CSL Includes */
#include <ti/csl/csl_cppi.h>
#else
#include <ti/drv/cppi/cppi_drv.h>
#endif

/* FM external includes */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

typedef struct {
    Cppi_CpDma dma;
} Fm_ExclusionCpdmaParams;

typedef struct {
    CSL_InstNum edma3Num;
} Fm_ExclusionEdma3Params;

typedef struct {
    int32_t lutInst;
} Fm_ExclusionLutEntryParams;

typedef struct {
    int32_t cic;
} Fm_ExclusionCicParams;

typedef struct {
    Fm_ResType           resType;
    Fm_ExcludedResource *exclusionList;
    uint32_t             numListEntries;
    uint32_t             resourceNum;
    union {
        Fm_ExclusionCpdmaParams    cpdmaParams;
        Fm_ExclusionEdma3Params    edma3Params;
        Fm_ExclusionLutEntryParams lutParams;
        Fm_ExclusionCicParams      cicParams;
    } u;
} Fm_ExclusionParams;

uint32_t fmExclusionValidateList(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *exResList, 
                                 int32_t *status);
uint32_t fmExclusionIsExcluded(Fm_ExclusionParams *exParams);

#ifdef __cplusplus
}
#endif

#endif /* FM_EXCLUSIONLOC_H_ */

