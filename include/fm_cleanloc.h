/*
 *  file  fm_cleanloc.h
 *
 *  Internal prototypes and data structures for Fault Management fault 
 *  cleanup.
 *
 *  ============================================================================
 *      (C) Copyright 2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef FM_CLEANLOC_H_
#define FM_CLEANLOC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* CSL Includes */
#include <ti/csl/csl_cppi.h>

/* FM external includes */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

uint32_t fmGetDmaMaxRxCh(Cppi_CpDma dmaNum);
uint32_t fmGetDmaMaxTxCh(Cppi_CpDma dmaNum);

Fm_Result fmCleanupInit(uint32_t fullInit);
Fm_Result fmCleanCppi(Fm_ExcludedResource *excludedResList, uint32_t listSize);
Fm_Result fmCleanQmssAccum(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList,
                           uint32_t listSize);
Fm_Result fmCleanQmssQueue(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList,
                           uint32_t listSize);
Fm_Result fmCleanPa(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList, 
                    uint32_t listSize);
Fm_Result fmCleanSa(Fm_ExcludedResource *excludedResList, uint32_t listSize);
Fm_Result fmCleanEdma3(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList,
                       uint32_t listSize, uint32_t provideStatus);
Fm_Result fmCleanSemaphores(Fm_ExcludedResource *excludedResList, uint32_t listSize);
Fm_Result fmCleanCics(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList,
                      uint32_t listSize);
Fm_Result fmCleanTimers(Fm_GlobalConfigParams *fmGblCfgParams, Fm_ExcludedResource *excludedResList,
                        uint32_t listSize);
Fm_Result fmCleanAif2(void);
Fm_Result fmCleanTcp3d(void);
Fm_Result fmCleanBcp(void);
Fm_Result fmCleanFftc(void);
Fm_Result fmCleanVcp(void);

#ifdef __cplusplus
}
#endif

#endif /* FM_CLEANLOC_H_ */

