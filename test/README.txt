Fault Management Library Example



A simple example demonstrating how to use the Fault Management module to send the DSP core dump to the Host processor when a DSP exception occurs.



Steps to build the benchmark example:



1. Import the CCS faultManagementTestProject from instrumentation\fault_mgmt\test directory. (in CCSv5.1, File->Import... Select Existing CCS/CCE Eclipse Projects)


2. Clean the faultManagementTestProject project, delete the Debug and Release directories, and re-build the project.  After the build is complete, faultManagementTestProject.out and faultManagementTestProject.map will be generated under instrumentation\fault_mgmt\test\Debug (or \Release depending on the build configuration) directory.



Steps to run faultManagementTestProject in CCSv5.1:



1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM.


2. Connect to Core 0.
3. Load instrumentation\fault_mgmt\test\Debug\faultManagementTestProject.out on core 0


4. Run the project.  A DSP exception should occur.
5. Open a new Memory Browser (View->Memory Browser) and type the symbol name "fault_mgmt_data" (minus quotes) into the memory window.  The data collected by the fault management library upon exception is located here.  Alternatively, one can open the faultManagementTestProject.map file and search for the fault_mgmt_data symbol and its associated placement address.  Then type that address into the Memory Browser.
6. The output located at the address input in the memory browser should follow the below format for ELF note sections and contain the DSP core dump information (i.e. registers A0/B0 through A31/B31 and NTSR, ITSR, IRP, SSR, AMR, RILC, and ILC should all be in the fault_mgmt_data section:

ELF Note Section:

byte            +0        +1       +2        +3
            +--------+--------+---------+--------+
namesz | # bytes in name field excluding pad|    Should contain number of bytes in the name field
            +--------+--------+---------+--------+
descsz   | # bytes in desc field excluding pad|     Should contain number of byes in the desc field
            +--------+--------+---------+--------+
type      |      Not Used (should be zero)     |
            +--------+--------+---------+--------+
name    | char 0 | char 1 | char 2  | char 3 |
            +--------+--------+---------+--------+
            | char 4 |  ...   | char n  |  pad   |      
            +--------+--------+---------+--------+
 desc    |              word 0                |   
            +--------+--------+---------+--------+
            |              word 1                |
            +--------+--------+---------+--------+

7. Open a new Memory Browser and input the address of the device's IPCGRH register.  Use the device's Data Manual to find the address for this register.
8. The SRCSx bit, where x is the DSP core number that the example project was loaded on, should be set.
