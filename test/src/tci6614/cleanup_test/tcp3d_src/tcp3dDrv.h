#ifndef _TCP3D_DRV_H_
#define _TCP3D_DRV_H_

#include <ti/csl/csl_edma3.h>

typedef struct EDMA_PARAM_SET
{
#ifdef _LITTLE_ENDIAN
    /*OPT register*/
    uint32_t sam          :  1;
    uint32_t dam          :  1;   
    uint32_t syncDim      :  1;   
    uint32_t stat         :  1;   
    uint32_t rsvd0        :  4;
    uint32_t fwid         :  3;
    uint32_t tccMode      :  1;
    uint32_t tcc          :  6;
    uint32_t rsvd1        :  1;
    uint32_t rsvd2        :  1;
    uint32_t tcintEn      :  1;
    uint32_t itcintEn     :  1;
    uint32_t tcchEn       :  1;
    uint32_t itcchEn      :  1;
    uint32_t privId       :  4;
    uint32_t rsvd3        :  3;
    uint32_t priv         :  1;

    
    uint32_t src          : 32;
    
    uint32_t aCnt         : 16;
    uint32_t bCnt         : 16;
    
    uint32_t dst          : 32;
    
    uint32_t srcBidx      : 16;
    uint32_t dstBidx      : 16;
    
    uint32_t link         : 16;
    uint32_t bCntRld      : 16;
    
    uint32_t srcCidx      : 16;
    uint32_t dstCidx      : 16;
    
    uint32_t cCnt         : 16;
    uint32_t rsvd         : 16;
#else
    /*OPT register*/
    uint32_t priv         :  1;
    uint32_t rsvd3        :  3;
    uint32_t privId       :  4;
    uint32_t itcchEn      :  1; 
    uint32_t tcchEn       :  1;
    uint32_t itcintEn     :  1;
    uint32_t tcintEn      :  1;
    uint32_t rsvd2        :  1;
    uint32_t rsvd1        :  1;
    uint32_t tcc          :  6;
    uint32_t tccMode      :  1;
    uint32_t fwid         :  3;
    uint32_t rsvd0        :  4;
    uint32_t stat         :  1;
    uint32_t syncDim      :  1;
    uint32_t dam          :  1;
    uint32_t sam          :  1;
    
    uint32_t src          : 32;
    
    uint32_t bCnt         : 16;
    uint32_t aCnt         : 16;
    
    uint32_t dst          : 32;
    
    uint32_t dstBidx      : 16;
    uint32_t srcBidx      : 16;
    
    uint32_t bCntRld      : 16;
    uint32_t link         : 16;
    
    uint32_t dstCidx      : 16;
    uint32_t srcCidx      : 16;
    
    uint32_t rsvd         : 16;
    uint32_t cCnt         : 16;
#endif  
} EDMA_PARAM_SET;


/*
  0         OPT
  1         SRC
  2  BCNT    | ACNT
  3         DST
  4  DSTBIDX | SRCBIDX
  5  BCNTRLD | LINK
  6  DSTCIDX | SRCCIDX
  7  Rsvd    | CCNT
*/
typedef struct EDMA_PARAM_SET_WORD{
    Uint32 opt  ;
    Uint32 src  ;
    Uint32 abCnt;
    Uint32 dst  ;
    Uint32 bIdx ;
    Uint32 link ;
    Uint32 cIdx ;
    Uint32 cCnt ;
} EDMA_PARAM_SET_WORD;

typedef struct DEMO_SEdmaTbl
{
    Uint16  ChIdx;
    Uint16  link;
    Uint8   syncDim;
    Uint8   tcchEn;
    Uint8   tcc;
    Uint16  aCnt;
    Uint16  bCnt;
    Uint16  cCnt;
    Uint32  srcAddr;
    Uint32  dstAddr;
    Uint16  srcBidx;
    Uint16  dstBidx;
    Uint16  srcCidx;
    Uint16  dstCidx;
    Uint32  chHandler;
    Uint32  PaRAM_Addr;
} DEMO_SEdmaTbl;

/*
 * TCP3D configuration parameters. These parameters are used to construct the user info field passed to BCP. 
 * These value will be read by the EDMA from the BCP packet to program TCP3D.
 */
typedef struct  {
#if 0    
    /* CB length */
    UInt16 cblength;
    /* CB length+4 */
    UInt16 cblengthplus4;
    /* Beta state are located at the end of code block = llrOffset * 3; */
    UInt16 betaStateLoc;
    /* HD ouput size = (CB length/8) */
    UInt16 cblengthover8;
    /* The next 3 field MUST be located at an offset of 2 bytes */
    /* CB length/2 Populate ACNT */
    UInt16 cblengthover2;
    /* CB length/2 Populate SRCBIDX */
    UInt16 cblengthover2_1;
    /* LLR Offset to Parity bits. Populate SRCCIDX */
    UInt16 llroffset;
    /* Padding */
    UInt16 reserved;
#endif
    /* Number of SW0 and block size */
    UInt32 TCP3D_IC_CFG0;
    /* SW0, SW1, SW2 length */
    UInt32 TCP3D_IC_CFG1;
    /* Interleaver table load, Max star, output status register, 
     * swap HD bit ordering, extrinsic scaling, soft output flag, 
     * soft output format, number of iterations, SNR reporting,
     * stopping criteria, CRC selection */
    UInt32 TCP3D_IC_CFG2;
    /* Max start threshold and value */
    UInt32 TCP3D_IC_CFG3;
    /* Beta state 0-3 for MAP0 */
    UInt32 TCP3D_IC_CFG4;
    /* Beta state 5-7 for MAP0 */
    UInt32 TCP3D_IC_CFG5;
    /* Beta state 0-3 for MAP1 */
    UInt32 TCP3D_IC_CFG6;
    /* Beta state 5-7 for MAP1 */
    UInt32 TCP3D_IC_CFG7;
    /* Extrinsic values 0-3 */
    UInt32 TCP3D_IC_CFG8;
    /* Extrinsic values 4-7 */
    UInt32 TCP3D_IC_CFG9;
    /* Extrinsic values 8-11 */
    UInt32 TCP3D_IC_CFG10;
    /* Extrinsic values 12-15 */
    UInt32 TCP3D_IC_CFG11;
    /* ITG param 0-1 */
    UInt32 TCP3D_IC_CFG12;
    /* ITG param 2-3 */
    UInt32 TCP3D_IC_CFG13;
    /* ITG param 4 */
    UInt32 TCP3D_IC_CFG14;
    /* Trigger to start new decode */
    UInt32 TCP3D_TRIG;
} BCP_userInfo;

/*
 * TCP3D configuration parameters. These parameters are used to construct the user info field passed to BCP. 
 * These value will be read by the EDMA from the BCP packet to program TCP3D.
 */
typedef struct cbConfig
{
    /* Control */
    Int32 Mode;         //TCP3_MODE

    /* Input */
    Int32 NumInfoBits;      //CFG0
    UInt8 sw0Length;       //CFG1
    Int32 maxStar;         //CFG2
    Int32 outStsRead;
    Int32 extScale;
    Int32 softOutRead;
    Int32 softOutFrmtSel;
    Int32 minIter;
    Int32 maxIter;
    Int32 snrVal;
    Int32 snrReport;
    Int32 stopSel;
    Int32 crcIterSel;
    Int32 crcPolySel;
    Int32 maxStarThres;        //CFG3
    Int32 maxStarValue;
    Int32 extrScale_0;        //CFG8
    Int32 extrScale_1;
    Int32 extrScale_2;
    Int32 extrScale_3;
    Int32 extrScale_4;        //CFG9
    Int32 extrScale_5;
    Int32 extrScale_6;
    Int32 extrScale_7;
    Int32 extrScale_8;        //CFG10
    Int32 extrScale_9;
    Int32 extrScale_10;
    Int32 extrScale_11;
    Int32 extrScale_12;       //CFG11
    Int32 extrScale_13;
    Int32 extrScale_14;
    Int32 extrScale_15;
} cbConfig;

#define MAXNUM_EDMA_PHYCH       (3)
#define MAXNUM_EDMA_LINKCH      (10)
#define A_SYMC                  (0)
#define AB_SYMC                 (1)
#define CFG                     (0)
#define TCP3D                   (0)
void DEMO_initEDMA();
void DEMO_setupEDMA();

extern Uint32 tcp3d_cfg[2][16];
extern Int8   tcp3d_llr[2][6144*3];
extern Uint32 tcp3d_hd [2][6144/32+1];

extern Int16    TCP3_LteInterleaverTable [220][7];
extern Int16    TCP3_WimaxInterleaverTable[17][4];
extern Int32    WIMAX_FRAME_LENGTHS[17];
extern DEMO_SEdmaTbl DEMO_EdmaTbl[MAXNUM_EDMA_PHYCH + MAXNUM_EDMA_LINKCH];

extern Uint32 glbCoreId;

void prepare_user_info (BCP_userInfo *userInfo, Int8 *tailBits, Uint16 cblen);

#define IN
#define OUT

void Tcp3d_betaStates(
                    IN  Int8     * tailBits,
                    IN  int32_t   signChange,
                    IN  int32_t   Kt,
                    OUT Int8     * beta0Ptr,
                    OUT Int8     * beta1Ptr);

Int32 LTE_interleaver_index(Int32 K);

Int32 WIMAX_interleaver_index(Int32 K);

Void tcp3d_config (UInt8 instanceNum);

Int32 enable_tcp3dA();
Int32 enable_tcp3dB();
#if 0
#define hTCP3dACfgReg               ((CSL_Tcp3d_cfgRegs *) CSL_TCP3D_A_CFG_REGS)
#define hTCP3dADmaReg               ((CSL_Tcp3d_dmaRegs *) CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_IC_CFG0_P0_OFFSET)

#define hTCP3dALLR0Reg              (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)
#define hTCP3dALLR1Reg              (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P1_OFFSET)

#define hTCP3dAHD0Reg               (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD0_OFFSET)
#define hTCP3dAHD1Reg               (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD1_OFFSET)

#define hTCP3dBCfgReg               ((CSL_Tcp3d_cfgRegs *) CSL_TCP3D_B_CFG_REGS
#define hTCP3dBDmaReg               ((CSL_Tcp3d_dmaRegs *) CSL_TCP3D_B_DATA_REGS + CSL_TCP3D_DMA_TCP3D_IC_CFG0_P0_OFFSET)

#define hTCP3dBLLR0Reg              (CSL_TCP3D_B_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)
#define hTCP3dBLLR1Reg              (CSL_TCP3D_B_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P1_OFFSET)

#define hTCP3dBHD0Reg               (CSL_TCP3D_B_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD0_OFFSET)
#define hTCP3dBHD1Reg               (CSL_TCP3D_B_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD1_OFFSET)

#define hTCP3dCCfgReg               ((CSL_Tcp3d_cfgRegs *) CSL_TCP3D_C_CFG_REGS)
#define hTCP3dCDmaReg               ((CSL_Tcp3d_dmaRegs *) CSL_TCP3D_C_DATA_REGS + CSL_TCP3D_DMA_TCP3D_IC_CFG0_P0_OFFSET)

#define hTCP3dCLLR0Reg              (CSL_TCP3D_C_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)
#define hTCP3dCLLR1Reg              (CSL_TCP3D_C_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P1_OFFSET)

#define hTCP3dCHD0Reg               (CSL_TCP3D_C_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD0_OFFSET)
#define hTCP3dCHD1Reg               (CSL_TCP3D_C_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD1_OFFSET)
#endif

/* Handle to access QMSS INTD Register overlay structure */
#define hQmssIntdReg                ((CSL_Qm_intdRegs *) CSL_QM_SS_CFG_INTD_REGS)

/* Handle to access CPINTC Register overlay structure */
#define hCpIntcReg                  ((CSL_CPINTCRegs *) CSL_CP_INTC_1_REGS)

#endif

