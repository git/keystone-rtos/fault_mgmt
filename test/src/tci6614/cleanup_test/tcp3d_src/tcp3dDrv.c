#include <stdio.h>
#include <string.h>
#include <c6x.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/csl_edma3.h>
#include <ti/csl/csl_edma3Aux.h>
#include <ti/csl/cslr_tcp3d_cfg.h>
#include <ti/csl/cslr_tcp3d_dma.h>
#include <ti/csl/cslr_tcp3d_dma_offsets.h>

#include "tcp3dDrv.h"
#include "tcp3d_utils.h"

#define MAP_L2toGLB(a,coreId)  (0x10000000+a+(coreId<<24))


#define TCP3D_CFG0  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_IC_CFG0_P0_OFFSET)
#define TCP3D_LLR0  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)
#define TCP3D_OUT0  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD0_OFFSET)

#define TCP3D_CFG1  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_IC_CFG0_P1_OFFSET)
#define TCP3D_LLR1  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_SYS_P1_OFFSET)
#define TCP3D_OUT1  (CSL_TCP3D_A_DATA_REGS + CSL_TCP3D_DMA_TCP3D_OUT_HD1_OFFSET)

#define LLR_DSTCIDX (CSL_TCP3D_DMA_TCP3D_PAR0_P0_OFFSET - CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)
#define LLR_DSTBIDX (CSL_TCP3D_DMA_TCP3D_PAR0_P0_OFFSET - CSL_TCP3D_DMA_TCP3D_SYS_P0_OFFSET)

Uint32 tcp3d_cfg[2][16];
Int8   tcp3d_llr[2][6144*3];
Uint32 tcp3d_hd [2][6144/32+1];



/*
  dummy TRIGGER -> CFG0 -> LLR0(sys stream) -> LLR0(par1 stream) -> LLR0(par2 stream) -> dummy
  dummy TRIGGER -> CFG1 -> LLR1(sys stream) -> LLR1(par1 stream) -> LLR1(par2 stream) -> dummy
  REVT0 -> HD0
  REVT1 -> HD1
 */
// In the following table, CFG means configurable, 
DEMO_SEdmaTbl DEMO_EdmaTbl[MAXNUM_EDMA_PHYCH + MAXNUM_EDMA_LINKCH]={
                     /* ChIdx    link  syncDim  tcchEn    tcc     aCnt    bCnt    cCnt    srcAddr             dstAddr            srcBidx dstBidx      srcCidx dstCidx       chHandler PaRAM_Addr*/
/* 0 dummy REVT0    */  { 0,      70,   A_SYMC,     1,      0,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/* 1 dummy REVT1    */  { 1,      72,   A_SYMC,     1,      1,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/* 2 dummy TRIGGER  */  { 2,      64,   A_SYMC,     1,      2,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/* 3 LINK_CFG0      */  {64,      65,   A_SYMC,     1,      2,      60,     1,      1,  (Uint32)tcp3d_cfg[0],   TCP3D_CFG0,           0,      0,           0,      0,             NULL,      NULL},
/* 4 LINK_LLR0      */  {65,      66,   A_SYMC,     0,      2,      CFG,    3,      1,  (Uint32)tcp3d_llr[0],   TCP3D_LLR0,          CFG,    LLR_DSTBIDX,  CFG,   LLR_DSTCIDX,    NULL,      NULL},
/* 5 END_LLR0       */  {66,      67,   A_SYMC,     1,      2,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/* 6 LINK_CFG1      */  {67,      68,   A_SYMC,     1,      2,      60,     1,      1,  (Uint32)tcp3d_cfg[1],   TCP3D_CFG1,           0,      0,           0,      0,             NULL,      NULL},
/* 7 LINK_LLR1      */  {68,      69,   A_SYMC,     0,      2,      CFG,    3,      1,  (Uint32)tcp3d_llr[1],   TCP3D_LLR1,          CFG,    LLR_DSTBIDX,  CFG,   LLR_DSTCIDX,    NULL,      NULL},
/* 8 END_LLR1       */  {69,      64,   A_SYMC,     1,      2,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/* 9 LINK_HD0       */  {70,      71,   A_SYMC,     0,      0,      CFG,    1,      1,   TCP3D_OUT0,          (Uint32)tcp3d_hd[0],    0,      0,           0,      0,             NULL,      NULL},
/*10 END_HD0        */  {71,      70,   A_SYMC,     1,      0,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
/*11 LINK_HD1       */  {72,      73,   A_SYMC,     0,      1,      CFG,    1,      1,   TCP3D_OUT0,          (Uint32)tcp3d_hd[1],    0,      0,           0,      0,             NULL,      NULL},
/*12 END_HD1        */  {73,      72,   A_SYMC,     1,      1,      0,      1,      1,      NULL,                  NULL,              0,      0,           0,      0,             NULL,      NULL},
};

CSL_Edma3ChannelObj     glbEdmaChObj[MAXNUM_EDMA_PHYCH];

void DEMO_initEDMA()
{
    CSL_Edma3Obj        edmaObj;
    Uint8               instNum = CSL_TPCC_2;
    CSL_Status          status; 
    CSL_Edma3Handle     hModule;
    CSL_Edma3ChannelHandle  hEdmaCh;
    CSL_Edma3ChannelAttr    chAttr;
    Uint32 i, PaRAM_Addr;
    CSL_Edma3Context        context;

    if (CSL_edma3Init(&context) != CSL_SOK) 
    {
        printf ("EDMA module initialization failed\n");
    }
    
    /* Open EDMA3 */
    hModule = CSL_edma3Open (&edmaObj, instNum, NULL, &status);
    if ((hModule == NULL) || (status != CSL_SOK)) 
    {
        printf("Open EDMA3 failed");
    }
    for( i = 0; i < MAXNUM_EDMA_PHYCH; i++ )
    {
        /* Open EDMA channel */
        chAttr.regionNum = CSL_EDMA3_REGION_GLOBAL;
        chAttr.chaNum    = DEMO_EdmaTbl[i].ChIdx;
        hEdmaCh = CSL_edma3ChannelOpen (&glbEdmaChObj[i], instNum, &chAttr, &status);
        if ((hEdmaCh == NULL) || (status != CSL_SOK)) 
        {
             printf("Open EDMA channel failed");
        }
        else
        {
            DEMO_EdmaTbl[i].chHandler = (Uint32)hEdmaCh;
            printf("EDMA CH obj %08x\n",hEdmaCh);
        }
        
        /* Enable EDMA channel */
        if ((status = CSL_edma3HwChannelControl (hEdmaCh, CSL_EDMA3_CMD_CHANNEL_ENABLE, NULL)) != CSL_SOK)
        {
            printf("Enable EDMA channel failed");
        }
        
        /* Map EDMA channel to PaRAM set */
        CSL_edma3MapDMAChannelToParamBlock (hModule, DEMO_EdmaTbl[i].ChIdx, DEMO_EdmaTbl[i].ChIdx);
    }
    
    /* Get the PaRAM set address */
    for( i = 0; i < (MAXNUM_EDMA_PHYCH + MAXNUM_EDMA_LINKCH); i++ )
    {
        hEdmaCh = (CSL_Edma3ChannelHandle)DEMO_EdmaTbl[0].chHandler;
        PaRAM_Addr = (Uint32)CSL_edma3GetParamHandle (hEdmaCh, DEMO_EdmaTbl[i].ChIdx, &status);
        if (PaRAM_Addr == NULL) 
        {
            printf("Get PaRAM failed");
        }
        else
        {
            DEMO_EdmaTbl[i].PaRAM_Addr = PaRAM_Addr;
            printf("PaRAM address %08x\n",PaRAM_Addr);
        }
    }
}

void DEMO_setupEDMA()
{
    Uint32 i, j;
    Uint32 tmpBuf[8];
    Uint32 * wordPtr;
    EDMA_PARAM_SET * par;
    /* Program PaRAM set */
    for( i = 0; i < (MAXNUM_EDMA_PHYCH + MAXNUM_EDMA_LINKCH); i++ )
    {
        //par = (EDMA_PARAM_SET *)DEMO_EdmaTbl[i].PaRAM_Addr;
        for( j = 0; j < 8; j++ )
            tmpBuf[j] = 0;
        par = (EDMA_PARAM_SET *)tmpBuf;
        par->sam      = 0;
        par->dam      = 0; 
        par->syncDim  = DEMO_EdmaTbl[i].syncDim;   
        par->stat     = 0;
        par->fwid     = 0;
        par->tccMode  = 0;
        par->tcc      = DEMO_EdmaTbl[i].tcc;
        par->tcintEn  = 0;
        par->itcintEn = 0;
        par->tcchEn   = DEMO_EdmaTbl[i].tcchEn;
        if( (i==4) || (i==7) )
        {
            par->itcchEn  = 1;
        }
        else
        {
            par->itcchEn  = 0;
        }
        par->privId   = 0;
        par->priv     = 0;
        if( (DEMO_EdmaTbl[i].srcAddr >= 0x800000) && (DEMO_EdmaTbl[i].srcAddr < 0x900000) )
        {
            par->src      = MAP_L2toGLB(DEMO_EdmaTbl[i].srcAddr,glbCoreId);
        }
        else
        {
            par->src      = DEMO_EdmaTbl[i].srcAddr;
        }
        par->aCnt     = DEMO_EdmaTbl[i].aCnt;
        par->bCnt     = DEMO_EdmaTbl[i].bCnt;
        if( (DEMO_EdmaTbl[i].dstAddr >= 0x800000) && (DEMO_EdmaTbl[i].dstAddr < 0x900000) )
        {
            par->dst      = MAP_L2toGLB(DEMO_EdmaTbl[i].dstAddr,glbCoreId);
        }
        else
        {
            par->dst      = DEMO_EdmaTbl[i].dstAddr;
        }
        par->srcBidx  = DEMO_EdmaTbl[i].srcBidx;
        par->dstBidx  = DEMO_EdmaTbl[i].dstBidx;
        par->link     = DEMO_EdmaTbl[i].link*32+0x4000;
        par->bCntRld  = 0;
        par->srcCidx  = DEMO_EdmaTbl[i].srcCidx;
        par->dstCidx  = DEMO_EdmaTbl[i].dstCidx;
        par->cCnt     = DEMO_EdmaTbl[i].cCnt;
        
        wordPtr = (Uint32 *)DEMO_EdmaTbl[i].PaRAM_Addr;
        for( j = 0; j < 8; j++ )
        {
            wordPtr[j] = tmpBuf[j];
        }
    }

//    post_write_uart("EDMA Setup for TCP3d Done\n");
}


Void tcp3d_config (Uint8 instanceNum)
{
    Uint32  reg = 0;
    CSL_Tcp3d_cfgRegs * hTCP3dACfgReg;
    
    if(instanceNum == 0)
	    hTCP3dACfgReg = ((CSL_Tcp3d_cfgRegs *) CSL_TCP3D_A_CFG_REGS);
	else
	    hTCP3dACfgReg = ((CSL_Tcp3d_cfgRegs *) CSL_TCP3D_B_CFG_REGS);
	    
    /* Soft Reset the TCP3D */
    hTCP3dACfgReg->TCP3_SOFT_RESET = 1;
    hTCP3dACfgReg->TCP3_SOFT_RESET = 0;

    /* Mode 3=SPLIT MODE HSUPA */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_MODE_SEL, 3);

    /* Input memory, 0 = double buffer disable */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_IN_MEM_DB_EN, 0);

    /* 1 = Internal Generation of Interleaver table enabled */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_ITG_EN, CSL_TCP3D_CFG_TCP3_MODE_ITG_EN_ENABLE);

    /* Error Ignore bit, 1 = Don't stop TCP3D on enabled errors */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_ERROR_IGNORE_EN, CSL_TCP3D_CFG_TCP3_MODE_ERROR_IGNORE_EN_DONT_STOP);

    /* 1 = Auto trigger enabled */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_AUTO_TRIG_EN, CSL_TCP3D_CFG_TCP3_MODE_AUTO_TRIG_EN_ENABLE);

    /* LTE CRC value selection - 0 for LTE */
    CSL_FINS (reg, TCP3D_CFG_TCP3_MODE_LTE_CRC_ISEL, 0);

    hTCP3dACfgReg->TCP3_MODE = reg;

    /* Set ENDIAN register parameters */
    reg = 0;
#ifdef _BIG_ENDIAN
    CSL_FINS (reg, TCP3D_CFG_TCP3_END_ENDIAN_INTR, CSL_TCP3D_CFG_TCP3_END_ENDIAN_INTR_16_BIT_NATIVE);
    CSL_FINS (reg, TCP3D_CFG_TCP3_END_ENDIAN_INDATA, CSL_TCP3D_CFG_TCP3_END_ENDIAN_INDATA_8_BIT_NATIVE);
#else
    CSL_FINS (reg, TCP3D_CFG_TCP3_END_ENDIAN_INTR, CSL_TCP3D_CFG_TCP3_END_ENDIAN_INTR_32_BIT_PACKED);
    CSL_FINS (reg, TCP3D_CFG_TCP3_END_ENDIAN_INDATA, CSL_TCP3D_CFG_TCP3_END_ENDIAN_INDATA_32_BIT_PACKED);
#endif

    hTCP3dACfgReg->TCP3_END = reg;

    /* Set EXECUTE P0 register parameters 1 = Normal mode */
    CSL_FINS (hTCP3dACfgReg->TCP3_EXE_P0, TCP3D_CFG_TCP3_EXE_P0_EXE_CMD, CSL_TCP3D_CFG_TCP3_EXE_P0_EXE_CMD_ENABLE);

    /* Set EXECUTE P1 register parameters 1 = Normal mode */
    CSL_FINS (hTCP3dACfgReg->TCP3_EXE_P1, TCP3D_CFG_TCP3_EXE_P1_EXE_CMD, CSL_TCP3D_CFG_TCP3_EXE_P1_EXE_CMD_ENABLE);

    return;
}

/** TCP3D SW0 nominal values */
Int32 tcp3d_sw0_Tab[] = {16, 32, 48,  64, 96, 128};

/** Used for getting the sw0LenSel index values */
Int32 TAB[] = {0, 1, 2, 3, 3, 4, 4, 5};

/** Table used for division optimization logic */
Int32 shiftValTab [] = {4, 5, 4, 6, 5, 7};

/** Table used for division optimization logic */
Uint32 mulValTab [] = {32768, 32768, 10923, 32768, 10923, 32768};

/** Table used for checking bounds */
Uint32 frameLenTab[2][2] = {40,5114,40,6144};


Int32 TCP3D_codeBlkSeg (
            UInt32  blockLengthK,
            UInt8   numMAP,
            UInt8  *sw0NomLen,
            UInt8  *sw0LenSel,
            UInt8  *sw1Len,
            UInt8  *sw2LenSel,
            UInt8  *numsw0)
{
    Int32  status = 0;
    Int32 K, Kext;
    Int32 numSWrem;
    Int32 subFrameLen;
    Int32 sw0LenSelTmp;
    Int32 sw1LenTmp;
    Int32 sw2LenSelTmp;
    Int32 numsw0Tmp;
    Int32 numSW;
    Int32 shiftVal, mulVal;
    Int32 sw0Len = *sw0NomLen;

    /**
     * Check the bounds based on numMAP value. frameLenTab is for the bound values
     *      numMAP  -   mode    - block length bounds
     *        1     -   3GPP    -   [40,5114]
     *        2     - LTE/WIMAX -   [40,6144]
     */
    if ( (blockLengthK < frameLenTab[numMAP-1][0]) ||
         (blockLengthK > frameLenTab[numMAP-1][1]) )
    {
        status = 1;
        return (status);
    }

    K = blockLengthK;
    Kext = ((K + 0x3)>>2)<<2;

    //Calculate sw0LenSelTmp, SW1Len, SW2LenSel, numsw0Tmp
    subFrameLen = Kext >> numMAP;   //Kext / (2*numMAP);

    sw0LenSelTmp = TAB[((sw0Len>>4)-1)&0x7];

    //Check that this holds: (reg->NumInfoBits <= 128 * sparms->tcp3_SW0_length * numMap)
    while((Kext > 128 * sw0Len * numMAP) && sw0LenSelTmp<6)
    {
        sw0LenSelTmp++;
        sw0Len = tcp3d_sw0_Tab[sw0LenSelTmp];
    }

    //numSW = subFrameLen/sw0Len;  Replaced by:
    shiftVal = shiftValTab[sw0LenSelTmp];
    mulVal   = mulValTab[sw0LenSelTmp];
    numSW = _mpysu((subFrameLen >> shiftVal), mulVal)>>15;

    numSWrem = subFrameLen - numSW*sw0Len;
    if(numSWrem)
    {
        numSW++;
    }

    if(numSW == 1)
    {
        numsw0Tmp = 0;
        sw1LenTmp = subFrameLen-1;             //stored value is (sw1_length -1)
        sw2LenSelTmp = 0;                      //SW2 is Off.
    }
    else if(numSW == 2)
    {
        numsw0Tmp = 0;
        if(subFrameLen & 0x3)
        {
            sw1LenTmp = 2*(subFrameLen>>2) + 1;    //stored value is (sw1_length -1)
            sw2LenSelTmp = 2;                      //sw1LenTmp > SW2Len
        }
        else
        {
            sw1LenTmp = (subFrameLen>>1) - 1;  //stored value is (sw1_length -1)
            sw2LenSelTmp = 1;                  //sw1LenTmp = SW2Len
        }
    }
    else if( numSWrem <= (sw0Len>>1) )
    {
        numsw0Tmp = numSW-2;
        numSWrem = subFrameLen - (numSW-2)*sw0Len;
        if((numSWrem) & 0x3)
        {
            sw1LenTmp = 2*(numSWrem>>2) + 1;   //stored value is (sw1_length -1)
            sw2LenSelTmp = 2;                  //sw1LenTmp > SW2Len
        }
        else
        {
            sw1LenTmp = (numSWrem>>1) - 1;     //stored value is (sw1_length -1)
            sw2LenSelTmp = 1;                  //sw1LenTmp = SW2Len
        }
    }
    else
    {
        numsw0Tmp = numSW-1;
        sw1LenTmp = numSWrem - 1;              //stored value is (sw1_length -1)
        sw2LenSelTmp = 0;                      //SW2 is Off.
    }


    *sw0LenSel  = (Uint8) sw0LenSelTmp;
    *sw1Len     = (Uint8) sw1LenTmp;
    *sw2LenSel  = (Uint8) sw2LenSelTmp;
    *numsw0     = (Uint8) numsw0Tmp;
    *sw0NomLen  = (Uint8) sw0Len;

    return ( status );

}

CSL_PscRegs *    dbghPscRegs = ((CSL_PscRegs *) (CSL_PSC_REGS));

Int32 enable_tcp3dA()
{
#ifndef SIMULATOR_SUPPORT

    /* TCP3d A power domain is turned OFF by default. It
     * needs to be turned on before doing any TCP3d A device
     * register access.
     */
    /* Set TCP3d A Power domain to ON */        
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_TCP3D_A);

    /* Enable the clocks too for TCP3d A */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_TCP3D_A, PSC_MODSTATE_ENABLE);

 
    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_TCP3D_A);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_TCP3D_A));
    /* Return TCP3d A PSC status */
    if ((CSL_PSC_getPowerDomainState(CSL_PSC_PD_TCP3D_A) == PSC_PDSTATE_ON) &&
        (CSL_PSC_getModuleState (CSL_PSC_LPSC_TCP3D_A) == PSC_MODSTATE_ENABLE))
    {
        /* TCP3d A ON. Ready for use */            
        return 0;
    }
    else
    {
        /* TCP3d A Power on failed. Return error */            
        return -1;            
    }
#else
    /* No power up needed on Sim */
    return 0;
#endif
}

CSL_Tcp3d_cfgRegs   *tcp3dA_CfgRegs = (CSL_Tcp3d_cfgRegs *) CSL_TCP3D_A_CFG_REGS;
CSL_Tcp3d_cfgRegs   *tcp3dB_CfgRegs = (CSL_Tcp3d_cfgRegs *) CSL_TCP3D_B_CFG_REGS;

Int32 enable_tcp3dB()
{
#ifndef SIMULATOR_SUPPORT

    /* TCP3d B power domain is turned OFF by default. It
     * needs to be turned on before doing any FFTC device
     * register access.
     */
    /* Set TCP3d B Power domain to ON */        
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_TCP3D_B);

    /* Enable the clocks too for TCP3d B */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_TCP3D_B, PSC_MODSTATE_ENABLE);

 
    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_TCP3D_B);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_TCP3D_B));
    /* Return FFTC PSC status */
    if ((CSL_PSC_getPowerDomainState(CSL_PSC_PD_TCP3D_B) == PSC_PDSTATE_ON) &&
        (CSL_PSC_getModuleState (CSL_PSC_LPSC_TCP3D_B) == PSC_MODSTATE_ENABLE))
    {
        /* TCP3d B ON. Ready for use */            
        return 0;
    }
    else
    {
        /* TCP3d B Power on failed. Return error */            
        return -1;            
    }
#else
    /* No power up needed on Sim */
    return 0;
#endif
}



void prepare_user_info (BCP_userInfo *userInfo, Int8 *tailBits, Uint16 cblen)
{
    UInt32      reg;
    UInt8       mode, sw0LenSel, sw2LenSel, sw1Len, numsw0;
    cbConfig    param; 
    Int32 signChange;
    Int32 Kt;
    Int8 beta0[8], beta1[8];

    memset ((void *) &param, 0, sizeof (cbConfig)); 

    /* Assigning values for configuration. */
    param.sw0Length        = 96;
    param.maxStar          = 0;
    param.outStsRead       = 0;
    param.extScale         = 1;
    param.softOutRead      = 0;
    param.softOutFrmtSel   = 1;
    param.minIter          = 1;
    param.maxIter          = 8;
    param.snrVal           = 14;
    param.snrReport        = 1;
    param.stopSel          = 0;
    param.crcIterSel       = 1;
    param.crcPolySel       = 0;
    param.maxStarThres     = 4;
    param.maxStarValue     = 2;
    param.extrScale_0      = 24;
    param.extrScale_1      = 24;
    param.extrScale_2      = 24;
    param.extrScale_3      = 24;
    param.extrScale_4      = 24;
    param.extrScale_5      = 24;
    param.extrScale_6      = 24;
    param.extrScale_7      = 24;
    param.extrScale_8      = 24;
    param.extrScale_9      = 24;
    param.extrScale_10     = 24;
    param.extrScale_11     = 24;
    param.extrScale_12     = 24;
    param.extrScale_13     = 24;
    param.extrScale_14     = 24;
    param.extrScale_15     = 24;

    mode = CSL_TCP3D_CFG_TCP3_MODE_MODE_SEL_HSUPA;

    //userInfo->cblength = cblen;
    //userInfo->cblengthover2 = cblen >> 1;
    //userInfo->cblengthover2_1 = userInfo->cblengthover2;
    //userInfo->cblengthplus4 = cblen + 4;
    //userInfo->llroffset = (((cblen + 4) + 15) >> 4 ) * 16;
    //userInfo->betaStateLoc = (userInfo->llroffset) * 3;
    //userInfo->cblengthover8 = cblen >> 3;
    //userInfo->reserved = 0;
    TCP3D_codeBlkSeg (cblen, 1,
                        &param.sw0Length,
                        &sw0LenSel,
                        &sw1Len,
                        &sw2LenSel,
                        &numsw0);

    /* Number of SW0 and block size (Range: 39 to 8191; block size - 1) */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG0_P0_NUM_SW0, numsw0);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG0_P0_BLK_LN, cblen - 1);

    userInfo->TCP3D_IC_CFG0 = reg;

    /* SW0, SW1, SW2 length */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG1_P0_SW0_LN_SEL, sw0LenSel);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG1_P0_SW2_LN_SEL, sw2LenSel);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG1_P0_SW1_LN, sw1Len);


    userInfo->TCP3D_IC_CFG1 = reg;

    /* Interleaver table load, Max star, output status register, 
     * swap HD bit ordering, extrinsic scaling, soft output flag, 
     * soft output format, number of iterations, SNR reporting,
     * stopping criteria, CRC selection */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_INTER_LOAD_SEL, CSL_TCP3D_DMA_TCP3D_IC_CFG2_P0_INTER_LOAD_SEL_SET);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_MAXST_EN, param.maxStar);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_OUT_FLAG_EN, param.outStsRead);
#ifdef _BIG_ENDIAN
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_OUT_ORDER_SEL, CSL_TCP3D_DMA_TCP3D_IC_CFG2_P0_OUT_ORDER_SEL_SWAP);
#else
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_OUT_ORDER_SEL, CSL_TCP3D_DMA_TCP3D_IC_CFG2_P0_OUT_ORDER_SEL_NO_SWAP);
#endif
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_EXT_SCALE_EN, param.extScale);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_FLAG_EN, param.softOutRead);
#ifdef _BIG_ENDIAN
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_ORDER_SEL, CSL_TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_ORDER_SEL_8_BIT);
#else
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_ORDER_SEL, CSL_TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_ORDER_SEL_32_BIT);
#endif
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SOFT_OUT_FMT, param.softOutFrmtSel);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_MIN_ITR, param.minIter);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_MAX_ITR, param.maxIter);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SNR_VAL, param.snrVal);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_SNR_REP, param.snrReport);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_STOP_SEL, param.stopSel);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_CRC_ITER_PASS, param.crcIterSel);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG2_P0_CRC_SEL, param.crcPolySel);

    userInfo->TCP3D_IC_CFG2 = reg;

    /* Max star threshold and value */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG3_P0_MAXST_THOLD, param.maxStarThres);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG3_P0_MAXST_VALUE, param.maxStarValue);

    userInfo->TCP3D_IC_CFG3 = reg;
    
    Kt = COMPUTE_KT(cblen);
    signChange = 1;
    /* Beta states */
    Tcp3d_betaStates(tailBits, signChange, Kt, beta0, beta1);
                    
    /*
    7:0 beta_st0_map0
    15:8 beta_st1_map0
    23:16 beta_st2_map0
    31:24 beta_st3_map0
    */
    //userInfo->TCP3D_IC_CFG4 = _rotl(_swap4(_mem4(&beta0[0])),16); // beta_st0_map0 | beta_st1_map0 | beta_st2_map0 | beta_st3_map0
    //userInfo->TCP3D_IC_CFG5 = _rotl(_swap4(_mem4(&beta0[4])),16); // beta_st4_map0 | beta_st5_map0 | beta_st6_map0 | beta_st7_map0
    //userInfo->TCP3D_IC_CFG6 = _rotl(_swap4(_mem4(&beta1[0])),16);
    //userInfo->TCP3D_IC_CFG7 = _rotl(_swap4(_mem4(&beta1[4])),16);
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG4_P0_BETA_ST0_MAP0, beta0[0]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG4_P0_BETA_ST1_MAP0, beta0[1]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG4_P0_BETA_ST2_MAP0, beta0[2]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG4_P0_BETA_ST3_MAP0, beta0[3]);

    userInfo->TCP3D_IC_CFG4 = reg;//_mem4(&beta0[0]);

    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG5_P0_BETA_ST4_MAP0, beta0[4]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG5_P0_BETA_ST5_MAP0, beta0[5]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG5_P0_BETA_ST6_MAP0, beta0[6]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG5_P0_BETA_ST7_MAP0, beta0[7]);

    userInfo->TCP3D_IC_CFG5 = reg;

    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG6_P0_BETA_ST0_MAP1, beta1[0]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG6_P0_BETA_ST1_MAP1, beta1[1]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG6_P0_BETA_ST2_MAP1, beta1[2]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG6_P0_BETA_ST3_MAP1, beta1[3]);

    userInfo->TCP3D_IC_CFG6 = reg;//_mem4(&beta0[0]);

    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG7_P0_BETA_ST4_MAP1, beta1[4]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG7_P0_BETA_ST5_MAP1, beta1[5]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG7_P0_BETA_ST6_MAP1, beta1[6]);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG7_P0_BETA_ST7_MAP1, beta1[7]);

    userInfo->TCP3D_IC_CFG7 = reg;
    //userInfo->TCP3D_IC_CFG5 = _mem4(&beta0[4]);
    //userInfo->TCP3D_IC_CFG6 = _mem4(&beta1[0]);
    //userInfo->TCP3D_IC_CFG7 = _mem4(&beta1[4]);

    
    /* Extrinsic scale value 0-3 */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG8_P0_EXT_SCALE_0, param.extrScale_0);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG8_P0_EXT_SCALE_1, param.extrScale_1);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG8_P0_EXT_SCALE_2, param.extrScale_2);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG8_P0_EXT_SCALE_3, param.extrScale_3);
    userInfo->TCP3D_IC_CFG8 = reg;
    
    /* Extrinsic scale value 4-7 */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG9_P0_EXT_SCALE_4, param.extrScale_4);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG9_P0_EXT_SCALE_5, param.extrScale_5);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG9_P0_EXT_SCALE_6, param.extrScale_6);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG9_P0_EXT_SCALE_7, param.extrScale_7);
    userInfo->TCP3D_IC_CFG9 = reg;

    /* Extrinsic scale value 8-11 */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG10_P0_EXT_SCALE_8, param.extrScale_8);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG10_P0_EXT_SCALE_9, param.extrScale_9);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG10_P0_EXT_SCALE_10, param.extrScale_10);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG10_P0_EXT_SCALE_11, param.extrScale_11);
    userInfo->TCP3D_IC_CFG10 = reg;

    /* Extrinsic scale value 12-15 */
    reg = 0;
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG11_P0_EXT_SCALE_12, param.extrScale_12);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG11_P0_EXT_SCALE_13, param.extrScale_13);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG11_P0_EXT_SCALE_14, param.extrScale_14);
    CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG11_P0_EXT_SCALE_15, param.extrScale_15);
    userInfo->TCP3D_IC_CFG11 = reg;

    /* LTE or WIMAX */
    if (mode == CSL_TCP3D_CFG_TCP3_MODE_MODE_SEL_LTE )
    {
        Int32 frameLenInd = LTE_interleaver_index(cblen);

        /* ITG param 0-1 */
        reg = 0;

        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG12_P0_ITG_PARAM0, 
                        (UInt16) ((2*TCP3_LteInterleaverTable[frameLenInd][2]) % TCP3_LteInterleaverTable[frameLenInd][0]));
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG12_P0_ITG_PARAM1, TCP3_LteInterleaverTable[frameLenInd][6]);
        userInfo->TCP3D_IC_CFG12 = reg;
    
        /* ITG param 2-3 */
        reg = 0;
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG13_P0_ITG_PARAM2, TCP3_LteInterleaverTable[frameLenInd][3]);
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG13_P0_ITG_PARAM3, TCP3_LteInterleaverTable[frameLenInd][4]);
        userInfo->TCP3D_IC_CFG13 = reg;
    
        /* ITG param 4 */
        reg = 0;
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG14_P0_ITG_PARAM4, TCP3_LteInterleaverTable[frameLenInd][5]);
        userInfo->TCP3D_IC_CFG14 = reg;
    }
    else if (mode == CSL_TCP3D_CFG_TCP3_MODE_MODE_SEL_WIMAX)
    {
        Int32 frameLenInd = WIMAX_interleaver_index (cblen);
        /* ITG param 0-1 */
        reg = 0;

        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG12_P0_ITG_PARAM0, 0);
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG12_P0_ITG_PARAM1, TCP3_WimaxInterleaverTable[frameLenInd][0]);
        userInfo->TCP3D_IC_CFG12 = reg;
    
        /* ITG param 2-3 */
        reg = 0;
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG13_P0_ITG_PARAM2, TCP3_WimaxInterleaverTable[frameLenInd][1]);
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG13_P0_ITG_PARAM3, TCP3_WimaxInterleaverTable[frameLenInd][2]);
        userInfo->TCP3D_IC_CFG13 = reg;
    
        /* ITG param 4 */
        reg = 0;
        CSL_FINS (reg, TCP3D_DMA_TCP3D_IC_CFG14_P0_ITG_PARAM4, TCP3_WimaxInterleaverTable[frameLenInd][3]);
        userInfo->TCP3D_IC_CFG14 = reg;
    }
    else
    {
        /* ITG Params are not required for 3GPP */
        userInfo->TCP3D_IC_CFG12 = 0;
        userInfo->TCP3D_IC_CFG13 = 0;
        userInfo->TCP3D_IC_CFG14 = 0;
    }

    userInfo->TCP3D_TRIG = 1;
    return;
}

