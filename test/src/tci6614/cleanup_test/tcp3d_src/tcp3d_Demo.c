#include <stdio.h>
#include <string.h>
#include <c6x.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_cache.h>
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/src/intc/csl_intc.h>
#include <ti/csl/tistdtypes.h>
#include <ti/csl/csl_cpIntcAux.h>
#include <ti/csl/cslr_tcp3d_cfg.h>
#include <ti/csl/cslr_tcp3d_dma.h>
#include <ti/csl/cslr_tcp3d_dma_offsets.h>

#include "tcp3dDrv.h"
#include "testvector_tc.h"
#include "testvector_hd.h"

#include <xdc/runtime/System.h>

/* Global variable declaration */
Uint32          glbCoreId;
Uint32 			numTests=0;

void tcp3d_init()
{   
    glbCoreId = DNUM;
    tcp3d_config (0);
    DEMO_initEDMA();
    DEMO_setupEDMA();
}
/*
 0 dummy REVT0   
 1 dummy REVT1   
 2 dummy TRIGGER 
 3 LINK_CFG0     
 4 LINK_LLR0     
 5 END_LLR0      
 6 LINK_CFG1     
 7 LINK_LLR1     
 8 END_LLR1      
 9 LINK_HD0      
10 END_HD0       
11 LINK_HD1      
12 END_HD1       
*/
#define LLR0_LINK_INDEX     (4)
#define LLR1_LINK_INDEX     (7)
#define HD0_LINK_INDEX      (9)
#define HD1_LINK_INDEX      (11)
#define TRIGGER_CH_INDEX    (2)

void FourByteSwap(Uint8* DataBuffer, Uint32 cblen)
{
    Uint8 dold[4];
    Uint32 i;

    for( i = 0; i < cblen*3; i=i+4 ) {
        dold[0] = DataBuffer[i];
        dold[1] = DataBuffer[i+1];
        dold[2] = DataBuffer[i+2];
        dold[3] = DataBuffer[i+3];
        DataBuffer[i]   = dold[3];
        DataBuffer[i+1] = dold[2];
        DataBuffer[i+2] = dold[1];
        DataBuffer[i+3] = dold[0];
    }
}

void tcp3d_test()
{
    Uint32  cblen, i, error;
    Uint8 * ptr_outHD1, * ptr_outHD2, * ptr_refHD;
    Uint8 *TestVectorBuff;
    Int8    tailBits[12];
    //    Int8    temp0, temp1;
    EDMA_PARAM_SET_WORD * par;
    CSL_Edma3ChannelHandle  hEdmaCh;

    cblen = 4974;
    TestVectorBuff = (Uint8*)(&tc_testvectorCB6[0]);
    //FourByteSwap(TestVectorBuff, cblen);
    /* The tail bit sequence should be sys[0] */
    for( i = 0; i < 12; i++ ) {
        tailBits[i] = tail[i];
    }

    prepare_user_info ((BCP_userInfo *)tcp3d_cfg[0], tailBits, cblen);

    for( i = 0; i < 12; i++ ) {
        tailBits[i] = tail[i];
    }
    prepare_user_info ((BCP_userInfo *)tcp3d_cfg[1], tailBits, cblen);

    memset(tcp3d_hd,0xff,sizeof(tcp3d_hd));
    for( i = 0; i < (cblen + (4-cblen%4)*2)*3; i++ ) {
        tcp3d_llr[0][i] = TestVectorBuff[i];
        tcp3d_llr[1][i] = TestVectorBuff[i];
    }

    /* setup LLR0 PaRAM fields aCnt, srcBidx, srcCidx */ 
    par = (EDMA_PARAM_SET_WORD *)DEMO_EdmaTbl[LLR0_LINK_INDEX].PaRAM_Addr;
    par->opt   |= 1<<23;  //itcchEn = 1
    par->abCnt |= cblen + (4-cblen%4)*2;
    par->bIdx  |= cblen + (4-cblen%4)*2; //cblen + twice the number required to make it a multiple of 4
    //par->cIdx  |= cblen;

    /* setup LLR1 PaRAM fields aCnt, srcBidx, srcCidx */
    par = (EDMA_PARAM_SET_WORD *)DEMO_EdmaTbl[LLR1_LINK_INDEX].PaRAM_Addr;
    par->opt   |= 1<<23;  
    par->abCnt |= cblen + (4-cblen%4)*2;
    par->bIdx  |= cblen + (4-cblen%4)*2;
    //par->cIdx  |= cblen+16;

    /* setup HD0 PaRAM field aCnt */
    par = (EDMA_PARAM_SET_WORD *)DEMO_EdmaTbl[HD0_LINK_INDEX].PaRAM_Addr;     
    par->opt   |= 1<<20;
    par->abCnt |= cblen/8;

    /* setup HD1 PaRAM field aCnt */
    par = (EDMA_PARAM_SET_WORD *)DEMO_EdmaTbl[HD1_LINK_INDEX].PaRAM_Addr;     
    par->opt   |= 1<<20;
    par->abCnt |= cblen/8;

//    System_printf("\n Triggering EDMA Transfer...\n");

    /* Trigger the EDMA */
    /* Trigger channel */ 
    hEdmaCh = (CSL_Edma3ChannelHandle)DEMO_EdmaTbl[TRIGGER_CH_INDEX].chHandler;
    CSL_edma3HwChannelControl(hEdmaCh,CSL_EDMA3_CMD_CHANNEL_SET,NULL);
    while( !(hEdmaCh->regs->TPCC_IPR&1) );
    error = 0;

    CSL_edma3HwChannelControl(hEdmaCh,CSL_EDMA3_CMD_CHANNEL_SET,NULL);
    while( !(hEdmaCh->regs->TPCC_IPR&2) );
    error = 0;
    ptr_outHD1 = (Uint8 *)tcp3d_hd[0];
    ptr_outHD2 = (Uint8 *)tcp3d_hd[1];
    ptr_refHD = (Uint8 *)hd_testvector;
    //    temp0 = ptr_outHD2[0];
    //    temp1 = ptr_refHD[0];

    for(i=0;i<20000;i++) {
        asm("	NOP	 5"); //Delay
    }

    for( i = 0; i < (cblen/8); i++ ) {
        if( ptr_refHD[i] != ptr_outHD1[i] ) {
            error++;
            System_printf("\n HD1 Iteration Index i = %d", i);
            System_printf("\n Output Data = 0x%x", ptr_outHD1[i]);
            System_printf("\n Reference Data = 0x%x", ptr_refHD[i]);
        }
    }

    for( i = 0; i < (cblen/8); i++ ) {
        if( ptr_refHD[i] != ptr_outHD2[i] ) {
            error++;
            System_printf("\n HD2 Iteration Index i = %d", i);
            System_printf("\n Output Data = 0x%x", ptr_outHD2[i]);
            System_printf("\n Reference Data = 0x%x", ptr_refHD[i]);
        }
    }

    if( error > 0 ) {
        System_printf("\n Error Count = %d", error);
        System_printf("\r\nTCP3d Test FAILED");
    }
    else {
        System_printf("\r\nTCP3d Test PASSED");
    }
}

