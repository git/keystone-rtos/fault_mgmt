/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  Created 2004, (C) Copyright 2003 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_parameters.h
*  Description    : Configuration Parameters for VCP2
\*****************************************************************************/

#define VCP2_NUM_USERS                      2
#define VCP2_NB_32BITWORD_INPUT_PARAMS      6
#define VCP2_NB_32BITWORD_OUTPUT_PARAMS     4

VCP2_Params vcpParameters[VCP2_NUM_USERS] = {
/* User0 */
{
    VCP2_RATE_1_2,                            /* rate             */
    5,                                        /* constLen         */
    0x30,                                     /* poly0            */
    0xd0,                                     /* poly1            */
    0x00,                                     /* poly2            */
    0x00,                                     /* poly3            */
    100,                                      /* yamTh            */
    2044,                                     /* frameLen         */
    0,                                        /* relLen           */
    0,                                        /* convDist         */
    0,                                        /* tbstindex        */    
    0,                                        /* tbstvalue        */
    0,                                        /* outbit_order     */
    256,                                      /* maxSm            */
    -1000,                                    /* minSm            */
    8,                                        /* stateNum         */
    16,                                       /* bmBuffLen        */
    32,                                       /* decBuffLen       */
    1,                                        /* traceBack        */
    1,                                        /* readFlag         */
    0,                                        /* decision         */
    0,                                        /* numBranchMetrics */
    0},                                       /* numDecisions     */

/* User1 */
{
    VCP2_RATE_1_2,                            /* rate             */
    5,                                        /* constLen         */
    0x30,                                     /* poly0            */
    0xd0,                                     /* poly1            */
    0x00,                                     /* poly2            */
    0x00,                                     /* poly3            */
    100,                                      /* yamTh            */
    2044,                                     /* frameLen         */
    0,                                        /* relLen           */
    0,                                        /* convDist         */
    0,                                        /* tbstindex        */   
    0,                                        /* tbstvalue        */
    0,                                        /* outbit_order     */
    256,                                      /* maxSm            */
    -1000,                                    /* minSm            */
    8,                                        /* stateNum         */
    16,                                       /* bmBuffLen        */
    32,                                       /* decBuffLen       */
    1,                                        /* traceBack        */
    1,                                        /* readFlag         */
    0,                                        /* decision         */
    0,                                        /* numBranchMetrics */
    0}                                        /* numDecisions     */
    
};

/* User 0 global variable declarations */
extern Uint32 refDecUser0[];
extern Uint8  vcpChannelDataUser0[];

/* User 1 global variable declarations */
extern Uint32 refDecUser1[];
extern Uint8  vcpChannelDataUser1[];

Uint32 *referenceDec[VCP2_NUM_USERS] = {&refDecUser0[0],
                                        &refDecUser1[0],
                                        };

VCP2_UserData *vcpUserData[VCP2_NUM_USERS] =
                                        {(VCP2_UserData *)&vcpChannelDataUser0[0],
                                        (VCP2_UserData *)&vcpChannelDataUser1[0],
                                        };
/*****************************************************************************\
* End of vcp2_parameters.h
\*****************************************************************************/

