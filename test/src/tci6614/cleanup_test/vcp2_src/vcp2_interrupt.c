/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  (C) Copyright 2009 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_interrupt.c
*  Description    : Interrupt setup and handling functions for the VCP2 test
\*****************************************************************************/

#include <stdio.h>
#include <ti/csl/csl.h>
#include <ti/csl/csl_tsc.h>
#include <ti/csl/csl_types.h>
#include <ti/csl/csl_error.h>
#include <ti/csl/src/intc/csl_intc.h>
#include <ti/csl/csl_edma3.h>
#include <ti/csl/csl_vcp2.h>
#include <ti/csl/csl_vcp2Aux.h>
#include <ti/csl/soc.h>
#include <ti/csl/csl_cpIntcAux.h>
#include "vcp2_edma3.h"
#include "vcp2_testInit.h"

// Global variable declaration
CSL_IntcHandle              hIntcEdma;
CSL_IntcHandle              hIntcVcp;
CSL_IntcObj                 intcObjEdma;
CSL_IntcObj                 intcObjVcp;
CSL_CPINTC_Handle           hCpintc;
CSL_IntcEventHandlerRecord  eventHandler[2];
extern CSL_IntcGlobalEnableState   state;  //dummy var since not used to restore state later
CSL_Edma3CmdIntr            regionIntrQuerry;

// External global variables
extern CSL_Edma3Handle hModule;

extern Uint32 decisions[4][1024];
extern VCP2_Params vcpParameters[20];

extern volatile Uint32 VcpError;
extern volatile Uint32 RxCount;

extern volatile Uint32 TxEdmaChannelBCount;
extern volatile Uint32 ChannelTXCount;
extern volatile Uint32 ChannelRXCount;

extern VCP2Handle hVcp2;
extern Uint32 coreNum;

// Function declarations
interrupt void EDMA_ISR(void);
interrupt void VCP2_ISR(void);

//-------------------------------------------------------------
// Setup_Interrupt() - Performs setup for GEM INTC and CP_INTC
//-------------------------------------------------------------
void Setup_Interrupt()
{
    //---------------------------------
    // Setup GEM internal INTC
    //---------------------------------

    // Declare variables
    CSL_IntcParam vectId1;
    CSL_IntcParam vectId2;
    CSL_IntcGlobalEnableState state;
    CSL_IntcContext intContext; 

    // Setup the global Interrupt
    intContext.numEvtEntries = 2;
    intContext.eventhandlerRecord = eventHandler; 
    CSL_intcInit(&intContext);

    // Enable NMIs
    CSL_intcGlobalNmiEnable();

    // Enable Global Interrupts
    CSL_intcGlobalEnable(&state);

    // Select interrupt vector IDs to use on DSP core
    vectId1 = CSL_INTC_VECTID_9;
    vectId2 = CSL_INTC_VECTID_10;

    // Map GEM input interrupt used for EDMA completion to vectId1  
    hIntcEdma = CSL_intcOpen(&intcObjEdma,
            CSL_GEM_INTC0_OUT_8_PLUS_16_MUL_N,
            &vectId1,
            NULL);

    // Map GEM input interrupt used for VCP error to vectId2  
    hIntcVcp = CSL_intcOpen(&intcObjVcp,
            CSL_GEM_INTC0_OUT_9_PLUS_16_MUL_N,
            &vectId2,
            NULL);

    // Hook the ISR's
    CSL_intcHookIsr(vectId1,&EDMA_ISR);
    CSL_intcHookIsr(vectId2,&VCP2_ISR);

    // Clear any pending interrupts
    CSL_intcHwControl(hIntcEdma,CSL_INTC_CMD_EVTCLEAR,NULL);
    CSL_intcHwControl(hIntcVcp,CSL_INTC_CMD_EVTCLEAR,NULL);

    // Enable the event and interrupt
    CSL_intcHwControl(hIntcEdma,CSL_INTC_CMD_EVTENABLE,NULL);
    CSL_intcHwControl(hIntcVcp,CSL_INTC_CMD_EVTENABLE,NULL);


    //----------------------------------------------------------
    // Setup CP_INTC0
    //----------------------------------------------------------

    // Open a handle to CP_INTC instance 0
    hCpintc = CSL_CPINTC_open(CSL_CP_INTC_0);
    if (hCpintc == 0)
    {
       // printf ("Error: Unable to open CPINT instance 0\n");
        return;       
    }

    // Map EDMA completion and VCP error system interrupts to proper output interrupts (channels) and enable
    if (coreNum == 0) {
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT0, (16 * coreNum) + 8);
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_VCP0INT,          (16 * coreNum) + 9);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT0);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_VCP0INT);
    } else if (coreNum == 1) {
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT1, (16 * coreNum) + 8);
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_VCP1INT,          (16 * coreNum) + 9);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT1);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_VCP1INT);
    } else if (coreNum == 2) {
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT2, (16 * coreNum) + 8);
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_VCP2INT,          (16 * coreNum) + 9);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT2);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_VCP2INT);
    } else { // coreNum == 3
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT3, (16 * coreNum) + 8);
        CSL_CPINTC_mapSystemIntrToChannel (hCpintc, CSL_INTC0_VCP3INT,          (16 * coreNum) + 9);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_CPU_3_2_TPCCINT3);
        CSL_CPINTC_enableSysInterrupt (hCpintc, CSL_INTC0_VCP3INT);
    }

    /*
    // Clear pending system interrupts
    if (CSL_CPINTC_isInterruptPending(hCpintc) == TRUE)
    {
    // Get the source of the interrupt
    sysIntr = CSL_CPINTC_getPendingInterrupt(hCpintc);

    // Clear the interrupt
    CSL_CPINTC_clearSysInterrupt(hCpintc, sysIntr);
    }
    */

    // Enable output (host) interrupts to GEM
    CSL_CPINTC_enableHostInterrupt (hCpintc, (16 * coreNum) + 8);
    CSL_CPINTC_enableHostInterrupt (hCpintc, (16 * coreNum) + 9);

    // Enable global host interrupt enable
    CSL_CPINTC_enableAllHostInterrupt(hCpintc); 
}


/*************************************************************************\
 *                    EDMA ISR
 \*************************************************************************/
interrupt void EDMA_ISR(void)
{

    CSL_CPINTC_clearSysInterrupt(hCpintc, CSL_INTC0_CPU_3_2_TPCCINT0+coreNum);
    CSL_intcHwControl(hIntcEdma,CSL_INTC_CMD_EVTCLEAR,NULL);

    regionIntrQuerry.region = coreNum; // CSL_EDMA3_REGION_1;
    regionIntrQuerry.intr = 0xffffffff;
    regionIntrQuerry.intrh=0x0;  

    /* Read EDMA IPR */
    CSL_edma3GetHwStatus(hModule,CSL_EDMA3_QUERY_INTRPEND,&regionIntrQuerry); 
    /* Clear EDMA IPR */
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTRPEND_CLEAR,&regionIntrQuerry);

    do
    {
        /* Sample Timer Value after O/P Parameter Transfer is over */
        if(regionIntrQuerry.intr & (1 << VCP2_RCV_OPPARAM_TCC(coreNum)))
        {
            ChannelRXCount++;
        }

        /* Start VCP BM Transfer */
        if(regionIntrQuerry.intr & (1 << VCP2_XMT_TCC(coreNum)))
        {
            ChannelTXCount++;
        }

        CSL_edma3GetHwStatus(hModule,CSL_EDMA3_QUERY_INTRPEND,&regionIntrQuerry); 
        CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTRPEND_CLEAR,&regionIntrQuerry);
    
    }while (regionIntrQuerry.intr !=0);

    /* Set Interrupt EVAL */

    regionIntrQuerry.region = coreNum;
    CSL_edma3HwControl(hModule, CSL_EDMA3_CMD_INTR_EVAL,&regionIntrQuerry.region);
}


/*************************************************************************\
 *                    VCP2 ISR
 \*************************************************************************/
interrupt void VCP2_ISR(void)
{
    CSL_CPINTC_clearSysInterrupt(hCpintc, CSL_INTC0_VCP0INT+coreNum);
    CSL_intcHwControl(hIntcVcp,CSL_INTC_CMD_EVTCLEAR,NULL);

    //hVcp2->cfgregs->VCPERR = 0;

    VcpError++;
}


/******************************************************************************\
 * End of vcp2_interrupt.c
\******************************************************************************/

