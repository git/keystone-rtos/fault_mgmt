/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  Created 2004, (C) Copyright 2003 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_edma3.h
*  Description    : Common definitions and prototyeps for configuring the 
*                   EDMA3 for VCP2
\*****************************************************************************/


#ifndef _VCP2_EDMA3H_
#define _VCP2_EDMA3H_

#include <ti/csl/csl_edma3.h>

/* Test Case Header file */

//Enable DRAE bit 0-3 
#define CURIE_TEST_GEM_DRAELO  0xffffffff
#define CURIE_TEST_GEM_DRAEHI  0x0

#define CSL_EDMA3_VCPCONFIGREG_SIZE     24
#define CSL_EDMA3_VCPOUTREG_SIZE        8

#define VCP2_RCV_OPPARAM_TCC_VCP0       0
#define VCP2_XMT_IPCONFIG_TCC_VCP0      1

#define VCP2_XMT_TCC(vcp2Inst)          CSL_EDMA3_CHA_VCP2XEVT(vcp2Inst)
#define VCP2_XMT_IPCONFIG_TCC(vcp2Inst) (VCP2_XMT_IPCONFIG_TCC_VCP0 + (vcp2Inst * 2))
#define VCP2_RCV_TCC(vcp2Inst)          CSL_EDMA3_CHA_VCP2REVT(vcp2Inst)
#define VCP2_RCV_OPPARAM_TCC(vcp2Inst)  (VCP2_RCV_OPPARAM_TCC_VCP0 + (vcp2Inst * 2))

#define VCP2_DEC_RX_CHANNEL_NUM_VCP0    18
#define VCP2_BM_TX_CHANNEL_NUM_VCP0     19

#define VCP2_DEC_RX_CHANNEL_NUM(vcp2Inst) (VCP2_DEC_RX_CHANNEL_NUM_VCP0 + (vcp2Inst * 2))
#define VCP2_BM_TX_CHANNEL_NUM(vcp2Inst)  (VCP2_BM_TX_CHANNEL_NUM_VCP0 + (vcp2Inst * 2)) 

/** DMA Channel Setup  */
#define CSL_EDMA3_DMA_CHANNELSETUP_DEFAULT   {       \
   {CSL_EDMA3_QUE_0,0}, \
   {CSL_EDMA3_QUE_0,1}, \
   {CSL_EDMA3_QUE_0,2}, \
   {CSL_EDMA3_QUE_0,3}, \
   {CSL_EDMA3_QUE_0,4}, \
   {CSL_EDMA3_QUE_0,5}, \
   {CSL_EDMA3_QUE_0,6}, \
   {CSL_EDMA3_QUE_0,7}, \
   {CSL_EDMA3_QUE_0,8}, \
   {CSL_EDMA3_QUE_0,9}, \
   {CSL_EDMA3_QUE_0,10}, \
   {CSL_EDMA3_QUE_0,11}, \
   {CSL_EDMA3_QUE_0,12}, \
   {CSL_EDMA3_QUE_0,13}, \
   {CSL_EDMA3_QUE_0,14}, \
   {CSL_EDMA3_QUE_0,15}, \
   {CSL_EDMA3_QUE_0,16}, \
   {CSL_EDMA3_QUE_0,17}, \
   {CSL_EDMA3_QUE_0,18}, \
   {CSL_EDMA3_QUE_0,19}, \
   {CSL_EDMA3_QUE_0,20}, \
   {CSL_EDMA3_QUE_0,21}, \
   {CSL_EDMA3_QUE_0,22}, \
   {CSL_EDMA3_QUE_0,23}, \
   {CSL_EDMA3_QUE_0,24}, \
   {CSL_EDMA3_QUE_0,25}, \
   {CSL_EDMA3_QUE_0,26}, \
   {CSL_EDMA3_QUE_0,27}, \
   {CSL_EDMA3_QUE_0,28}, \
   {CSL_EDMA3_QUE_0,29}, \
   {CSL_EDMA3_QUE_0,30}, \
   {CSL_EDMA3_QUE_0,31}, \
   {CSL_EDMA3_QUE_0,32}, \
   {CSL_EDMA3_QUE_0,33}, \
   {CSL_EDMA3_QUE_0,34}, \
   {CSL_EDMA3_QUE_0,35}, \
   {CSL_EDMA3_QUE_0,36}, \
   {CSL_EDMA3_QUE_0,37}, \
   {CSL_EDMA3_QUE_0,38}, \
   {CSL_EDMA3_QUE_0,39}, \
   {CSL_EDMA3_QUE_0,40}, \
   {CSL_EDMA3_QUE_0,41}, \
   {CSL_EDMA3_QUE_0,42}, \
   {CSL_EDMA3_QUE_0,43}, \
   {CSL_EDMA3_QUE_0,44}, \
   {CSL_EDMA3_QUE_0,45}, \
   {CSL_EDMA3_QUE_0,46}, \
   {CSL_EDMA3_QUE_0,47}, \
   {CSL_EDMA3_QUE_0,48}, \
   {CSL_EDMA3_QUE_0,49}, \
   {CSL_EDMA3_QUE_0,50}, \
   {CSL_EDMA3_QUE_0,51}, \
   {CSL_EDMA3_QUE_0,52}, \
   {CSL_EDMA3_QUE_0,53}, \
   {CSL_EDMA3_QUE_0,54}, \
   {CSL_EDMA3_QUE_0,55}, \
   {CSL_EDMA3_QUE_0,56}, \
   {CSL_EDMA3_QUE_0,57}, \
   {CSL_EDMA3_QUE_0,58}, \
   {CSL_EDMA3_QUE_0,59}, \
   {CSL_EDMA3_QUE_0,60}, \
   {CSL_EDMA3_QUE_0,61}, \
   {CSL_EDMA3_QUE_0,62}, \
   {CSL_EDMA3_QUE_0,63} \
}

/** QDMA Channel Setup  */
#define CSL_EDMA3_QDMA_CHANNELSETUP_DEFAULT   {       \
   {CSL_EDMA3_QUE_0,64,CSL_EDMA3_TRIGWORD_DEFAULT}, \
   {CSL_EDMA3_QUE_0,65,CSL_EDMA3_TRIGWORD_DEFAULT}, \
   {CSL_EDMA3_QUE_0,66,CSL_EDMA3_TRIGWORD_DEFAULT}, \
   {CSL_EDMA3_QUE_0,67,CSL_EDMA3_TRIGWORD_DEFAULT}  \
}
/******************************************************************************\
* Global Function Calls for EDMA3 Setup Functions 
\******************************************************************************/
void edma3ModuleInit(Uint32 edma3_region_num);
void edma3CloseHandles();
void edma3CloseChannelHandles();

void edma3ParamSetup (CSL_Edma3ParamHandle hParam, Uint32 option, 
                     Uint32 *srcAddr, Uint32 *dstAddr, Uint32 Acnt,
                     Uint32 Bcnt, Uint32 Ccnt, Uint32 SrcBidx, Uint32 DstBidx,
                     Uint32 Cidx,CSL_Edma3ParamHandle link);                                          
#endif  /* _VCP2_EDMA3H_ */
/******************************************************************************\
* End of vcp2_edma3.h
\******************************************************************************/
