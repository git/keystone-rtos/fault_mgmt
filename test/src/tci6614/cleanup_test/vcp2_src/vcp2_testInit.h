/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  Created 2004, (C) Copyright 2003 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : csl_vcp2.h
*  Description    : VCP2 Common Header file
\*****************************************************************************/
#ifndef _CSL_VCP2_testInit_H_
#define _CSL_VCP2_testInit_H_

#include <ti/csl/soc.h>


//**************************************
//* VCP scope and inline control macros
//****************************************
#undef  USEDEFS
#undef  IDECL
#undef  IDEF

#ifdef  _VCP2_MOD_
  #define IDECL extern far
  #define USEDEFS
  #define IDEF
#else
  #ifdef  _INLINE
    #define IDECL static inline
    #define USEDEFS
    #define IDEF  static inline
  #else
    #define IDECL extern far
  #endif
#endif  
//**************************************
//* VCP global macro declarations
//**************************************

// Select instance of VCP2 (0-3)
#define  CSL_EDMA3_CHA_VCP2REVT(vcp2Inst)   (CSL_TPCC2_VCP0REVT + (vcp2Inst * 2)) 
#define  CSL_EDMA3_CHA_VCP2XEVT(vcp2Inst)   (CSL_TPCC2_VCP0XEVT + (vcp2Inst * 2))
#define  VCP2_CONTROL_BASE(vcp2Inst)        (CSL_VCP2_A_CFG_REGS + (vcp2Inst * 0x4000)) 
#define  VCP2_DATA_BASE(vcp2Inst)           (CSL_VCP2_A_DATA_REGS + (vcp2Inst * 0x100000))
//#define  hVcp2                      hVcp2_0

#define  VCP2_ICMEM_ADDR(vcp2Inst)      (VCP2_DATA_BASE(vcp2Inst) + 0x000000u)
#define  VCP2_OPMEM_ADDR(vcp2Inst)      (VCP2_DATA_BASE(vcp2Inst) + 0x000048u)
#define  VCP2_BMMEM_ADDR(vcp2Inst)      (VCP2_DATA_BASE(vcp2Inst) + 0x000080u)
#define  VCP2_HDMEM_ADDR(vcp2Inst)      (VCP2_DATA_BASE(vcp2Inst) + 0x0000C0u)
#define  VCP2_END_ADDR(vcp2Inst)        (VCP2_CONTROL_BASE(vcp2Inst) + 0x000020)
#define  VCP2_EXE_ADDR(vcp2Inst)        (VCP2_CONTROL_BASE(vcp2Inst) + 0x000018)
#define  VCP2_STATUS0_ADDR(vcp2Inst)    (VCP2_CONTROL_BASE(vcp2Inst) + 0x000040)
#define  VCP2_STATUS1_ADDR(vcp2Inst)    (VCP2_CONTROL_BASE(vcp2Inst) + 0x000044)
#define  VCP2_ERROR_ADDR(vcp2Inst)      (VCP2_CONTROL_BASE(vcp2Inst) + 0x000050)
#define  VCP2_EMU_ADDR(vcp2Inst)        (VCP2_CONTROL_BASE(vcp2Inst) + 0x000060)

#define  PSVR_BASE                 0x02350000 
#define  PSVR_LCKREG               (PSVR_BASE + 0x4)
#define  PSVR_MDCTL0               (PSVR_BASE + 0x8)
#define  PSVR_MDSTAT0              (PSVR_BASE + 0x14)
#define  PSVR_VCP2_EN              0x5
#define  PSVR_VCP2_LCKVAL          0x0F0A0B00
#define  PSVR_VCP2_MDSTAT_MASK     0x38
#define  PSVR_VCP2_MDSTAT_EN       0x8
#define  VCP2_END_SLEEP_EN         0x300

//#define PSC_START_ADDRESS      0x02350000
//#define PSC_PTSTAT             PSC_START_ADDRESS + 0x00000128
//#define PSC_PTCMD              PSC_START_ADDRESS + 0x00000120
//#define PSC_MDCTL10            PSC_START_ADDRESS + 0x00000a28
//#define PSC_PDCTL5             PSC_START_ADDRESS + 0x00000314  

/****************************************\
 * VCP global typedef declarations
\****************************************/
typedef Uint32 VCP2_Standard;
typedef Uint32 VCP2_Mode;
typedef Uint32 VCP2_Map;
typedef Uint8  VCP2_UserData;
typedef Uint8  VCP2_ExtrinsicData;

/****************************************/
/* VCP2 global enable function declarations */
/****************************************/
void enableVCP2();
Uint32 readPeriph();
void writePeriph();


/*****************************************/
/** VCP inline function declarations */
/*****************************************/
#ifdef USEDEFS

// Macros are used for filling Register field from input parameters 

#define CSL_VCP2_IC0_FMKR(poly3,poly2,poly1,poly0) \
    (Uint32)(\
            CSL_FMKR(31,24,poly3) \
            |CSL_FMKR(23,16,poly2) \
            |CSL_FMKR(15,8,poly1) \
            |CSL_FMKR(7,0,poly0))

#define CSL_VCP2_IC1_FMKR(CSL_VCP2_VCPIC1_YAMEN_ENABLE,yamt) \
    (Uint32)(\
            CSL_FMKR(28,28,CSL_VCP2_VCPIC1_YAMEN_ENABLE) \
            |CSL_FMKR(27,16,yamt))

#define CSL_VCP2_IC2_FMKR(r,f) \
    (Uint32)(\
            CSL_FMKR(31,16,r) \
            |CSL_FMKR(15,0,f))

#define CSL_VCP2_IC3_FMKR(out_order,itben,itbi,c) \
    (Uint32)(\
            CSL_FMKR(28,28,out_order) \
            |CSL_FMKR(24,24,itben) \
            |CSL_FMKR(23,16,itbi) \
            |CSL_FMKR(15,0,c))

#define CSL_VCP2_IC4_FMKR(imins,imaxs) \
    (Uint32)(\
            CSL_FMKR(28,16,imins) \
            |CSL_FMKR(12,0,imaxs))

#define CSL_VCP2_IC5_FMKR(sdhd,outf,tb,symr,symx,imaxi) \
    (Uint32)(\
            CSL_FMKR(31,31,sdhd) \
            |CSL_FMKR(30,30,outf) \
            |CSL_FMKR(29,28,tb) \
            |CSL_FMKR(24,20,symr) \
            |CSL_FMKR(19,16,symx) \
            |CSL_FMKR(7,0,imaxi))

/*----------------------------------------------------------------------------*/
#endif  /*USEDEFS*/ 

#endif /* _CSL_VCP2_testInit_H_ */
/******************************************************************************\
 * End of csl_vcp.h
 \******************************************************************************/

