/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  Created 2004, (C) Copyright 2003 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_edma3.c
*  Description    : Common Functions for configuring the EDMA3 for VCP2
\*****************************************************************************/

#include <ti/csl/csl_edma3.h>
#include <ti/csl/csl_vcp2.h>
#include <ti/csl/csl_chip.h>
#include "vcp2_edma3.h"
#include "vcp2_testInit.h"

/* EDMA module Handle */
CSL_Edma3Handle     hModule;
CSL_Edma3Context    context;
CSL_Edma3Obj        edmaObjMain;
CSL_Edma3Context    contextMain;
CSL_Edma3CmdRegion  draeSetup;
Uint32 revision,passStatus = 1;

/* EDMA Handles for Transmit and Receive Channels */
CSL_Edma3ChannelHandle hEdma3VcpXevt = NULL; /* EDMA channel for IC Values */
CSL_Edma3ChannelHandle hEdma3VcpRevt = NULL; /* EDMA channel for Decisions */

/* EDMA PaRAM Handles */
CSL_Edma3ParamHandle hParamIcArray[4], hParamBMetArray[4], hParamDecArray[4],hParamDummyArray[4]; 
CSL_Edma3ParamHandle hParamOpArray[4], hParamDecLastArray[4];
//CSL_Edma3ParamHandle hParamIc, hParamBMetFirst,hParamBMetSecond, hParamDec, hParamOp, hParamDecLast,hParamDummy;
CSL_Edma3ParamHandle hParamIc, hParamBMetFirst, hParamDec, hParamOp, hParamDecLast,hParamDummy;

/* EDMA Channel Objects for Xmt and Rcv */
CSL_Edma3ChannelObj edma3VcpXmtObj, edma3VcpRcvObj;

CSL_Edma3Context       edma3Context;
CSL_Edma3ChannelAttr   chParam;
CSL_Edma3Obj           edma3ShObj;
CSL_Edma3ModuleAttr    *regionParam;

CSL_Edma3CmdIntr       regionIntr;
CSL_Edma3CmdDrae       draeAttr;
CSL_Status             chStatus;
CSL_Status             handleStatus;

Uint32                 intrEn[2];

extern Uint32 coreNum;

/**
 *  @b Description
 *  @n
 *      Utility function which converts a local address to global.
 *
 *  @param[in]  addr
 *      Local address to be converted
 *
 *  @retval
 *      Global Address
 */
uint32_t l2_global_address (uint32_t addr)
{
  uint32_t corenum;

  /* Get the core number. */
  corenum = CSL_chipReadReg(CSL_CHIP_DNUM);

  if ((addr >= 0x00800000) && (addr < 0x00F08000))
  {
    return (addr + (0x10000000 + (corenum * 0x01000000)));
  }

  return (addr);
}

/*------------------------------------------------------------------*/
/* EDMA MODULE Initialization                                       */ 
/*------------------------------------------------------------------*/          
void edma3ModuleInit(Uint32 edma3_region_num)
{
    CSL_Edma3HwSetup hwSetup;
    CSL_Edma3QueryInfo info;
    CSL_Status status;
    CSL_Edma3HwDmaChannelSetup dmahwSetup[CSL_EDMA3_TPCC2_NUM_DMACH ] =
        CSL_EDMA3_DMA_CHANNELSETUP_DEFAULT;

    // Module Initialization
    CSL_edma3Init(&contextMain);  // (Does nothing)

    // Module Level Open
    hModule = CSL_edma3Open(&edmaObjMain,CSL_TPCC_2,NULL,&status);

    //Module Setup
    hwSetup.dmaChaSetup = &dmahwSetup[0];
    hwSetup.qdmaChaSetup = NULL;
    CSL_edma3HwSetup(hModule,&hwSetup);

    // Query Module revision and configuration.
    CSL_edma3GetHwStatus(hModule,CSL_EDMA3_QUERY_INFO,&info);

    // Setup the DRAE Masks
    draeAttr.region = edma3_region_num;    
    draeAttr.drae = ((1<<VCP2_RCV_OPPARAM_TCC(coreNum))|
            (1<<VCP2_XMT_IPCONFIG_TCC(coreNum))|
            (1<<VCP2_RCV_TCC(coreNum))|
            (1<<VCP2_XMT_TCC(coreNum)));
    
    draeAttr.draeh = 0; 
    
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_DMAREGION_ENABLE,&draeAttr);

}    

/*------------------------------------------------------------------*/
/* Populating the EDMA Parameters                                   */ 
/*------------------------------------------------------------------*/          
void edma3ParamSetup (CSL_Edma3ParamHandle hParam, Uint32 option, 
        Uint32 *srcAddr, Uint32 *dstAddr, Uint32 Acnt,
        Uint32 Bcnt, Uint32 Ccnt, Uint32 SrcBidx, 
        Uint32 DstBidx, Uint32 Cidx,CSL_Edma3ParamHandle link )
{
    CSL_Edma3ParamSetup myParamSetup;

    myParamSetup.option = option;

    myParamSetup.srcAddr = (Uint32) srcAddr;
    myParamSetup.aCntbCnt = CSL_EDMA3_CNT_MAKE(Acnt, Bcnt);
    myParamSetup.dstAddr = (Uint32) dstAddr;
    myParamSetup.srcDstBidx = CSL_EDMA3_BIDX_MAKE(SrcBidx, DstBidx);

    myParamSetup.linkBcntrld=CSL_EDMA3_LINKBCNTRLD_MAKE(link,0);
    myParamSetup.srcDstCidx = CSL_EDMA3_CIDX_MAKE(Cidx, Cidx);
    myParamSetup.cCnt = Ccnt;

    CSL_edma3ParamSetup(hParam, &myParamSetup);
}

/*------------------------------------------------------------------*/
/* Closing Handles of EDMA                                          */ 
/*------------------------------------------------------------------*/          
void edma3CloseHandles()
{
    CSL_edma3ChannelClose(hEdma3VcpXevt);
    CSL_edma3ChannelClose(hEdma3VcpRevt);
    CSL_edma3Close(hModule);
}
/*------------------------------------------------------------------*/
/* Closing Channel Handles of EDMA                                          */ 
/*------------------------------------------------------------------*/          
void edma3CloseChannelHandles()
{
    CSL_edma3ChannelClose(hEdma3VcpXevt);
    CSL_edma3ChannelClose(hEdma3VcpRevt);
}
/*------------------------------------------------------------------*/
/* Disable EDMA Channel                                             */
/*------------------------------------------------------------------*/          
void edma3Disable()
{
    CSL_edma3HwChannelControl(hEdma3VcpXevt,CSL_EDMA3_CMD_CHANNEL_DISABLE,NULL);

}

/*------------------------------------------------------------------*/
/* Enable Channel and Interrupts in EDMA                            */ 
/*------------------------------------------------------------------*/          
void edma3MultChnlEnable(Uint32 edmaRegionNum)
{
    /* Interrupt Event*/    
    regionIntr.region = edmaRegionNum; 
    regionIntr.intr= 1 << VCP2_XMT_TCC(coreNum);
    regionIntr.intrh= 0;
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTR_ENABLE,&regionIntr);                                                                             
    CSL_edma3HwChannelControl(hEdma3VcpXevt,CSL_EDMA3_CMD_CHANNEL_ENABLE,NULL);

    regionIntr.region = edmaRegionNum; 
    regionIntr.intr  |= 1 << VCP2_RCV_TCC(coreNum);
    regionIntr.intrh=0;
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTR_ENABLE,&regionIntr);
    CSL_edma3HwChannelControl(hEdma3VcpRevt,CSL_EDMA3_CMD_CHANNEL_ENABLE,NULL);

    regionIntr.region = edmaRegionNum; 
    regionIntr.intr  |= 1 << VCP2_RCV_OPPARAM_TCC(coreNum);
    regionIntr.intrh=0;
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTR_ENABLE,&regionIntr);
    CSL_edma3HwChannelControl(hEdma3VcpRevt,CSL_EDMA3_CMD_CHANNEL_ENABLE,NULL);

    regionIntr.region = edmaRegionNum; 
    regionIntr.intr  |= 1 << VCP2_XMT_IPCONFIG_TCC(coreNum);
    regionIntr.intrh=0;
    CSL_edma3HwControl(hModule,CSL_EDMA3_CMD_INTR_ENABLE,&regionIntr);
    CSL_edma3HwChannelControl(hEdma3VcpRevt,CSL_EDMA3_CMD_CHANNEL_ENABLE,NULL);

}

/****************************************************************************\
 * setupVcpTputEdma3: The EDMA is programmed to service the VCP.            *
 * IC values are configured and all EDMA parameters are calculated here.    *
 * VCP EVENTS trigger a L2-L2 transfer and the completion interrupts of     *
 * this transfer will trigger the actual VCP transfers.                     *
 \****************************************************************************/
void setupVcpTputEdma3(VCP2_ConfigIc *configIc, VCP2_UserData *VCP2_UserData, 
        VCP2_Params *vcpParameters, Uint32 *decisions, 
        Uint32 *outParms, Uint32 *numDec, Uint32 edmaRegionNum)
{
    Uint32 frameLen  = vcpParameters->frameLen;
    Uint32 constLen  = vcpParameters->constLen;
    Uint32 rate      = vcpParameters->rate;
    Uint32 symx      = vcpParameters->bmBuffLen+1;
    Uint32 symr      = vcpParameters->decBuffLen+1;
    Uint32 decision  = vcpParameters->decision;
    Uint32 traceBack = vcpParameters->traceBack; 
    Uint32 convDist  = vcpParameters->convDist;   

    Uint32 numBm,option;
    Uint32 ChannelNum;
    Uint32 Acnt, Bcnt, Ccnt, Bidx, Cidx, SrcBidx, DstBidx;
    Uint32 LastAcnt;
    Uint32 *decAddr;

    /*------------------------------------------------------------------*/
    /*                    SETUP FOR TRANSMIT CHANNEL                    */
    /*------------------------------------------------------------------*/

    /*------------------------------------------------------------------*/
    /* (1) Open VCP2 TX event EDMA channel and get handles to PaRAM entries */
    /*------------------------------------------------------------------*/

    if (*numDec == 0)  // Only open TX event channel for first codeblock of set
    {
        // VCP2 TX event channel open
        chParam.regionNum = edmaRegionNum; // CSL_EDMA3_REGION_1;
        chParam.chaNum = VCP2_XMT_TCC(coreNum);
        hEdma3VcpXevt = CSL_edma3ChannelOpen(&edma3VcpXmtObj,
                CSL_TPCC_2,
                &chParam,
                &chStatus);

        // VCP2 TX event channel setup
        CSL_edma3HwChannelSetupParam(hEdma3VcpXevt, CSL_TPCC2_VCP0XEVT + (coreNum * 2));
        CSL_edma3HwChannelSetupQue(hEdma3VcpXevt, CSL_EDMA3_QUE_0);
    }

    // Get handles to PaRAM entries for IC, BM, and dummy transfers
    ChannelNum = (VCP2_XMT_TCC(coreNum) + (*numDec * 24));
    hParamIcArray[*numDec] = CSL_edma3GetParamHandle(hEdma3VcpXevt, 
            ChannelNum, &handleStatus);
    ChannelNum = (VCP2_XMT_TCC(coreNum) + (*numDec * 24) + 8);
    hParamBMetArray[*numDec] = CSL_edma3GetParamHandle(hEdma3VcpXevt, 
            ChannelNum, &handleStatus);
    ChannelNum = (VCP2_XMT_TCC(coreNum) + (*numDec * 24) + 16);
    hParamDummyArray[*numDec] = CSL_edma3GetParamHandle(hEdma3VcpXevt, 
            ChannelNum, &handleStatus);

    /*------------------------------------------------------------------*/
    /* (2) Setup input config (IC) transfer                             */
    /*------------------------------------------------------------------*/
    option = CSL_EDMA3_OPT_MAKE(FALSE,FALSE,FALSE,TRUE,
            VCP2_XMT_IPCONFIG_TCC(coreNum),CSL_EDMA3_TCC_NORMAL,
            CSL_EDMA3_FIFOWIDTH_NONE,FALSE,CSL_EDMA3_SYNC_A,
            CSL_EDMA3_ADDRMODE_INCR,CSL_EDMA3_ADDRMODE_INCR);

    Acnt = CSL_EDMA3_VCPCONFIGREG_SIZE;
    Bcnt = 1;
    Ccnt = 1;
    Bidx = 0;
    Cidx = 0;

    // IC transfer linked to BM transfer
    edma3ParamSetup( hParamIcArray[*numDec], option,
                     (Uint32*)l2_global_address((Uint32)configIc), 
                     (Uint32*)VCP2_ICMEM_ADDR(coreNum),
                     Acnt, Bcnt, Ccnt, Bidx, Bidx, Cidx,
                     hParamBMetArray[*numDec]);

    /*------------------------------------------------------------------*/
    /* (3) Setup branch metric (BM) transfer                            */
    /*------------------------------------------------------------------*/
    if((traceBack==VCP2_TRACEBACK_TAILED)||(traceBack==VCP2_TRACEBACK_MIXED))   
    {
        // number of Branch Metrics
        numBm = ((1 << (rate - 1))*(frameLen + constLen-1)); 
    } 
    else
    {
        numBm  = ((1 << (rate - 1))*(frameLen + convDist));
    }

    Acnt = 4 * (symx ) * (1 <<(rate-1));
    Bcnt = numBm / Acnt ;
    if (numBm > (Acnt * Bcnt))
    {
        Bcnt = Bcnt + 1;
    } 

    option = CSL_EDMA3_OPT_MAKE(FALSE,FALSE,FALSE,TRUE,VCP2_XMT_TCC(coreNum),
            CSL_EDMA3_TCC_NORMAL,CSL_EDMA3_FIFOWIDTH_NONE,FALSE,
            CSL_EDMA3_SYNC_A,CSL_EDMA3_ADDRMODE_CONST,CSL_EDMA3_ADDRMODE_INCR);
    Ccnt    = 1;
    Bidx    = Acnt;
    SrcBidx = Bidx;
    DstBidx = 0;
    Cidx    = 0;

    // BM transfer linked to dummy transfer
    edma3ParamSetup( hParamBMetArray[*numDec], option,
#ifdef DDR3_INPUT
                     (Uint32*)VCP2_UserData,
#else
                     (Uint32*)l2_global_address((Uint32)VCP2_UserData),
#endif
                     (Uint32*)VCP2_BMMEM_ADDR(coreNum),
                     Acnt, Bcnt, Ccnt, SrcBidx, DstBidx, Cidx,
                     hParamDummyArray[*numDec]);

    /*------------------------------------------------------------------*/
    /* (4) Setup dummy transfer                                         */
    /*------------------------------------------------------------------*/
    option = CSL_EDMA3_OPT_MAKE(FALSE,FALSE,FALSE,FALSE,VCP2_XMT_TCC(coreNum),
            CSL_EDMA3_TCC_NORMAL,CSL_EDMA3_FIFOWIDTH_64BIT,FALSE,
            CSL_EDMA3_SYNC_A,CSL_EDMA3_ADDRMODE_INCR,CSL_EDMA3_ADDRMODE_INCR);
    Acnt = 0;
    Ccnt = 1;
    Bcnt = 0;
    Bidx = 0;
    Cidx = 0;
    SrcBidx = 0;
    DstBidx = 0;

    // Dummy transfer linked to NULL
    edma3ParamSetup( hParamDummyArray[*numDec], option,
#ifdef DDR3_INPUT
                     (Uint32*)VCP2_UserData,
#else
                     (Uint32*)l2_global_address((Uint32)VCP2_UserData),
#endif 
                     (Uint32*)VCP2_BMMEM_ADDR(coreNum),
                     Acnt, Bcnt, Ccnt, SrcBidx, DstBidx, Cidx,
                     (CSL_Edma3ParamHandle)CSL_EDMA3_LINK_NULL);

    // Link previous code block's BM transfer to current code block's IC transfer
    //  (overwriting previous link to dummy transfer)
    if (*numDec > 0)
    {
        hParamBMetArray[*numDec - 1]->LINK_BCNTRLD =
            CSL_EDMA3_LINKBCNTRLD_MAKE(hParamIcArray[*numDec],(hParamBMetArray[*numDec - 1]->LINK_BCNTRLD) >> 16);
    }


    /*------------------------------------------------------------------*/
    /*                    SETUP FOR RECEIVE CHANNEL                     */
    /*------------------------------------------------------------------*/

    /*------------------------------------------------------------------*/
    /* (5) Open VCP2 RX event EDMA channel and get handles to PaRAM entries */
    /*------------------------------------------------------------------*/
    if (*numDec == 0)  // Only open RX event channel for first codeblock of set
    {
        // VCP2 RX event channel open
        chParam.regionNum = edmaRegionNum; // CSL_EDMA3_REGION_1;
        chParam.chaNum = VCP2_RCV_TCC(coreNum);
        hEdma3VcpRevt = CSL_edma3ChannelOpen(&edma3VcpRcvObj,
                CSL_TPCC_2,
                &chParam,
                &chStatus);

        // VCP2 RX event channel setup
        CSL_edma3HwChannelSetupParam(hEdma3VcpRevt,VCP2_RCV_TCC(coreNum));
        CSL_edma3HwChannelSetupQue(hEdma3VcpRevt,CSL_EDMA3_QUE_0);
    }

    // Get handles to PaRAM entries for output decision and output param transfers
    ChannelNum = (VCP2_RCV_TCC(coreNum) + (*numDec * 16));
    hParamDecArray[*numDec] = CSL_edma3GetParamHandle(hEdma3VcpRevt, 
            ChannelNum, &handleStatus);

    ChannelNum = (VCP2_RCV_TCC(coreNum) + (*numDec * 16) + 8);
    hParamOpArray[*numDec] = CSL_edma3GetParamHandle(hEdma3VcpRevt, 
            ChannelNum, &handleStatus);

    /*------------------------------------------------------------------*/
    /* (6) Setup output decision (HDMEM / SDMEM) transfer               */
    /*------------------------------------------------------------------*/
    // Check the TCC and other fields
    option = CSL_EDMA3_OPT_MAKE(FALSE,FALSE,FALSE,FALSE,VCP2_RCV_TCC(coreNum),
            CSL_EDMA3_TCC_NORMAL,CSL_EDMA3_FIFOWIDTH_NONE,
            FALSE, CSL_EDMA3_SYNC_A,CSL_EDMA3_ADDRMODE_INCR,
            CSL_EDMA3_ADDRMODE_CONST);

    Acnt = (symr ) * 8 ;
    if (decision == 1)
    {
        Bcnt = ((frameLen)/Acnt);
    }
    else
    {
        Bcnt = ((frameLen/8)/Acnt);
    }
    if (Bcnt == 0)   /* For Small Framelengths */
    {
        Bcnt = 1;
        LastAcnt = 0;
    } 
    else
    {
        if (decision == 1)
        {
            LastAcnt = (frameLen ) - (Acnt * Bcnt);
        }
        else
        {
            LastAcnt = (frameLen ) - (Acnt * Bcnt * 8);
        }
        if (LastAcnt != 0)
        {
            Bcnt = Bcnt + 1;
            LastAcnt = 0;
        } 
    }

    Ccnt    = 1;
    Bidx    = Acnt;
    SrcBidx = 0;
    DstBidx = Bidx;
    Cidx    = 0;
    decAddr = decisions;

    // Link output decision transfer to output parameter transfer
    edma3ParamSetup( hParamDecArray[*numDec], option,
                     (Uint32*)VCP2_HDMEM_ADDR(coreNum),
#ifdef DDR3_OUTPUT
                     (Uint32*)decAddr,
#else
                     (Uint32*)l2_global_address((Uint32)decAddr),
#endif
                     Acnt, Bcnt, Ccnt, SrcBidx, Acnt, Cidx,
                     hParamOpArray[*numDec]);

    /*------------------------------------------------------------------*/
    /* (7) Setup output parameter (OP) transfer                              */
    /*------------------------------------------------------------------*/
    option = CSL_EDMA3_OPT_MAKE(FALSE,FALSE,FALSE,TRUE,VCP2_RCV_OPPARAM_TCC(coreNum),
            CSL_EDMA3_TCC_NORMAL,CSL_EDMA3_FIFOWIDTH_NONE,FALSE,
            CSL_EDMA3_SYNC_A,CSL_EDMA3_ADDRMODE_INCR,
            CSL_EDMA3_ADDRMODE_INCR);

    Acnt = CSL_EDMA3_VCPOUTREG_SIZE;
    Bcnt = 1;
    Ccnt = 1;
    Bidx = 0;
    Cidx = 0;

    // Link output parameter transfer to NULL
    edma3ParamSetup( hParamOpArray[*numDec], option,
                     (Uint32*)VCP2_OPMEM_ADDR(coreNum),
#ifdef DDR3_OUTPUT
                     (Uint32*)outParms,
#else
                     (Uint32*)l2_global_address((Uint32)outParms),
#endif
                     Acnt, Bcnt, Ccnt, Bidx, Bidx, Cidx,
                     (CSL_Edma3ParamHandle)CSL_EDMA3_LINK_NULL);  

    // Link previous code block's output parameter transfer to current code block's output decision transfer
    //  (overwriting previous link to NULL)
    if(*numDec > 0) 
    {
        hParamOpArray[*numDec - 1]->LINK_BCNTRLD = 
            CSL_EDMA3_LINKBCNTRLD_MAKE(hParamDecArray[*numDec],(hParamOpArray[*numDec - 1]->LINK_BCNTRLD) >> 16);
    }
}

/******************************************************************************\
 * End of vcp2_edma3.c
 \******************************************************************************/
