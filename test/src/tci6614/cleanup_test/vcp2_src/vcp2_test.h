/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  Created 2004, (C) Copyright 2003 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_parameters.h
*  Description    : Header file for VCP2 Local Test Functions 
\*****************************************************************************/

#include <ti/csl/csl.h>
#include <ti/csl/csl_chip.h>
#include <ti/csl/csl_vcp2.h>
#include "vcp2_testInit.h"

#define SUCCESS		0
#define FAILURE		1

#define PRE_HIBERNATION_TEST     0
#define POST_RESUME_TEST         1

/*****************************************************************************\
* Function Decalrations 
\*****************************************************************************/
extern void edmaModuleInit(Uint32 edma_region_num);

extern void edmaCloseHandles();

extern Int32 vcpCheckResults( Uint32 *actual,
                              Uint32 *reference,
                              Uint32 size);

interrupt void edmaIsr(void);

interrupt void VCP2_ISR(void);

void runVcp2Test(Uint32 vcp2Id);

void vcp2Init(Uint32 vcpIdx);

Uint32 enableVcp2 (Int32 vcp2Id);

Uint32 disableVcp2 (Int32 vcp2Id);

void VcpCslCompat( VCP2_Params * restrict ipConfigParams,
                   VCP2_Params * restrict opConfigParams);

void setupVcpTputEdma3( VCP2_ConfigIc *configIc,
                        VCP2_UserData *VCP2_UserData, 
                        VCP2_Params *vcpParameters,
                        Uint32 *decisions, 
                        Uint32 *outParms,
                        Uint32 *numDec,
                        Uint32 edmaRegionNum);

void edma3MultChnlEnable(Uint32 edmaRegionNum);

void Setup_Interrupt();

void CalcTimeStamp( Uint32 TimerStartLow,
                    Uint32 TimerStartHigh,
                    Uint32 TimerStopLow,
                    Uint32 TimerStopHigh);

Int32 vcpCheckResults( Uint32 *actual,
                       Uint32 *reference,
                       Uint32 size);

//Uint32 global_address (Uint32 addr);

/*****************************************************************************\
* End of vcp2_test.h 
\*****************************************************************************/
