/*****************************************************************************\
*           TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
*                                                                            
*  Property of Texas Instruments 
*  For  Unrestricted  Internal  Use  Only 
*  Unauthorized reproduction and/or distribution is strictly prohibited.  
*  This product is protected under copyright law and trade secret law 
*  as an unpublished work.  
*  (C) Copyright 2009 Texas Instruments.  All rights reserved.
*------------------------------------------------------------------------------
*  Filename       : vcp2_test.c
*  Description    : Performs VCP2 Decoding with EDMA3
*                   Scenario : r = 1/4 K = 9 FL = 64 HD Tailed Mode
*                   Scenario : r = 1/2 K = 5 FL = 186 SD Tailed Mode
*                   Scenario : r = 1/3 K = 7 FL = 6000 HD Convergent Mode
*                   Scenario : r = 1/2 K = 9 FL = 6000 HD Mixed Mode
\*****************************************************************************/

#include <stdio.h>
#include <ti/csl/csl.h>
#include <ti/csl/csl_edma3.h>
#include <ti/csl/csl_vcp2.h>
#include <ti/csl/csl_vcp2Aux.h>
#include <ti/csl/src/intc/csl_intc.h>
#include <ti/csl/csl_chip.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/csl_cacheAux.h>

#include "vcp2_testInit.h"
#include "vcp2_parameters.h"
#include "vcp2_edma3.h"
#include "vcp2_test.h"
#include "string.h"

#include <xdc/runtime/System.h>

// Constant definitions
#define NUM_USERS   2

// Global variable declaration
VCP2Handle  hVcp2;
VCP2Obj     pVcp2Obj;
extern Uint32      coreNum;

volatile Uint32 VcpError = 0;

volatile Uint32 ChannelTXCount = 0;
volatile Uint32 ChannelRXCount = 0;

volatile Uint32 flag = 0;

#pragma DATA_ALIGN(configIcArray, 8)   /* Should be double-word aligned   */
VCP2_ConfigIc configIcArray[4] ;  /* VCP Input Configuration Reg */

Uint32 refoutParams[4][2] = {{0x06f207a2, 0x00000000},
    {0x06f207a2, 0x00000000}};

Uint32 NumofDecisions[2] = {64, 64};

/* The coreNum dimension is required only for DDR3 endpoints as DDR3 is shared
 * by all cores unlike L2 which is core specific but its defined uncoditionally
 * to keep the listing simple.
 */
#ifdef DDR3_OUTPUT
#pragma DATA_SECTION(decisions, ".ddrData")
#else
//#pragma DATA_SECTION(decisions, ".asmBM")
#endif
#pragma DATA_ALIGN(decisions, 8) /* Should be double-word aligned */
Uint32 decisions[4][4][2048]; //[coreNum][blocknum][]

/* The coreNum dimension is required only for DDR3 endpoints as DDR3 is shared
 * by all cores unlike L2 which is core specific but its defined uncoditionally
 * to keep the listing simple.
 */
#ifdef DDR3_OUTPUT
#pragma DATA_SECTION(outParams, ".ddrData")
#else
//#pragma DATA_SECTION(outParams, ".asmBM")
#endif
#pragma DATA_ALIGN(outParams, 8)  /* Should be double-word aligned */
Uint32 outParams[4][4][2]; //[coreNum][][]

// External global variables
extern CSL_IntcHandle hIntcEdma;
extern CSL_IntcHandle hIntcVcp;
extern Uint32 *profilePtr;



/******************************************************************************
 * 
 * Function: runVcp2Test ()
 *
 *****************************************************************************/

void runVcp2Test(Uint32 vcp2Id)
{
    Uint32 i;
    int pStatus;


    // Get handle to desired VCP2 instance
    hVcp2 = VCP2_init(&pVcp2Obj, vcp2Id, &pStatus);
    
    memset(decisions,0,sizeof(decisions));
    VcpError = 0;
    ChannelTXCount = 0;
    ChannelRXCount = 0;

    VCP2_start(hVcp2);

	//Poll till the first word in the buffer matches the reference output
	while ( 1)
	{
	
		if( decisions[0][0][0] == referenceDec[0][0])
		break;
	}
	
	//Add a small delay
	for( i = 0 ; i < 1000000; i++);

    //Check Results

    /* VCP_NUM_USERS */
    for (i = 0; i < NUM_USERS  ; i++)
    {
        /* Verify output decisions */
        VcpError |= vcpCheckResults(decisions[coreNum][i], referenceDec[i], NumofDecisions[i]);

        if (VcpError)
        {
            /* Close the Interrup and EDMA3 Handles */
            edma3CloseHandles();
            CSL_intcClose(hIntcEdma);
            CSL_intcClose(hIntcVcp);
            System_printf("\r\nVCP2 Test FAILED");
            return;
        }

        /* Verify output parameters */
        VcpError += vcpCheckResults(outParams[coreNum][i], refoutParams[i], 2);
        if (VcpError)
        {
            /* Close the Interrup and EDMA3 Handles */
            edma3CloseHandles();
            CSL_intcClose(hIntcEdma);
            CSL_intcClose(hIntcVcp);
            System_printf("\r\nVCP2 Test FAILED");
            return;
        }


    } /* end for i */


    //Set the EDMA Transfer for the next Run. This EDMA Config is saved and restored later.
    for (i = 0; i < NUM_USERS; i++)
    {

    	//--- Setup DMA Channels for VCP Processing
        setupVcpTputEdma3( &configIcArray[i],
                vcpUserData[i],
                &vcpParameters[i],
                decisions[coreNum][i],
                outParams[coreNum][i],
                &i,
                coreNum);

    }

    System_printf("\r\nVCP2 Test PASSED");

} /* End of runVcp2Test() */


/******************************************************************************
 * 
 * Function: enableVcp2 (Uint32 vcp2Id)
 *
 *****************************************************************************/
Uint32 enableVcp2 (Int32 vcp2Id)
{
    Uint32 pdNum;
    Uint32 lpscNum;

#ifndef SIMULATOR_SUPPORT
    if((vcp2Id < CSL_VCP2_A) && (vcp2Id > CSL_VCP2_D))
    {
       // printf("\n[Core%d][enableVcp2] : Invalid VCP2 Id.", coreNum);
        return FAILURE;
    }

    pdNum   = (vcp2Id == CSL_VCP2_A) ? CSL_PSC_PD_ALWAYSON : CSL_PSC_PD_PD_VCP_BCD; 
    lpscNum = (vcp2Id == CSL_VCP2_A) ? CSL_PSC_LPSC_VCP2_A : (CSL_PSC_LPSC_VCP2_B + (vcp2Id-1)); 

    /* Enable VCP2-B/C/D power domain */
    CSL_PSC_enablePowerDomain (pdNum);

    /* Enable the clocks */
    CSL_PSC_setModuleNextState (lpscNum, PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (pdNum);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (pdNum));

    if ((CSL_PSC_getPowerDomainState (pdNum) == PSC_PDSTATE_ON) &&
            (CSL_PSC_getModuleState (lpscNum) == PSC_MODSTATE_ENABLE))
    {
        return SUCCESS;
    }
    else
    {
        return FAILURE;
    }
#else
    return SUCCESS;
#endif

}


/******************************************************************************
 * 
 * Function: disableVcp2 (Uint32 vcp2Id)
 *
 *****************************************************************************/
Uint32 disableVcp2 (Int32 vcp2Id)
{
    Uint32 pdNum;
    Uint32 lpscNum;

#ifndef SIMULATOR_SUPPORT
    if((vcp2Id < CSL_VCP2_A) && (vcp2Id > CSL_VCP2_D))
    {
       // printf("\n[Core%d][enableVcp2] : Invalid VCP2 Id.", coreNum);
        return FAILURE;
    }
    
    pdNum   = (vcp2Id == CSL_VCP2_A) ? CSL_PSC_PD_ALWAYSON : CSL_PSC_PD_PD_VCP_BCD; 
    lpscNum = (vcp2Id == CSL_VCP2_A) ? CSL_PSC_LPSC_VCP2_A : (CSL_PSC_LPSC_VCP2_B + (vcp2Id-1)); 

    /* Disable VCP2-B/C/D power domain */
    CSL_PSC_disablePowerDomain (pdNum);

    /* Disable the clocks */
    CSL_PSC_setModuleNextState (lpscNum, PSC_MODSTATE_DISABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (pdNum);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (pdNum));

    if (vcp2Id == CSL_VCP2_A)
    {
        return SUCCESS;
    }
    else if ((CSL_PSC_getPowerDomainState (pdNum) == PSC_PDSTATE_OFF) &&
            (CSL_PSC_getModuleState (lpscNum) == PSC_MODSTATE_SWRSTDISABLE))
    {
        return SUCCESS;
    }
    else
    {
        return FAILURE;
    }
#else
    return SUCCESS;
#endif

}

/****************************************************************************\
 * vcpCheckResults: Verify that the hard decisions and output parameters sent*
 * by the VCP are correct. If the hard decisions are not correct, the index *
 * of the first incorrect value is returned. If all values are correct then *
 * a 0 is returned.                                 *
 \****************************************************************************/
Int32 vcpCheckResults(Uint32 *actual, Uint32 *reference, Uint32 size)
{
    Uint32 i;
    Uint32 mismatch=0;
    Uint32 numWords = size;

    for (i=0; i<numWords; i++) {
        if (actual[i]!=reference[i]) {
            mismatch++;
            break;
        } /* end if actual != reference */
    } /* end for i */

    if ((!mismatch) && (size & 1)) {
        if ((short)actual[i] != (short)reference[i]) 
        {
            mismatch++;
        }

    } /* end if */

    if (mismatch) 
    {    
        return(i+1); /* return index for 1st error that occured. */
        /* Index will range from 1 to framelength   */
    }
    else
    {
        //return(PASS);      /* no error */
        return 0;            /* no error */
    }

} /* end checkResults() */

/******************************************************************************\
 * End of vcp2_util.c
 \******************************************************************************/
void VcpCslCompat (VCP2_Params * restrict ipConfigParams,
        VCP2_Params * restrict opConfigParams
        )
{
    Uint16 TemptraceBackEn;
    TemptraceBackEn = ipConfigParams->traceBackEn;
    opConfigParams->traceBackEn = ipConfigParams->traceBackIndex;
    opConfigParams->traceBackIndex = TemptraceBackEn;
    opConfigParams->bmBuffLen = ipConfigParams->bmBuffLen-1;  
    opConfigParams->decBuffLen = ipConfigParams->decBuffLen-1;

    opConfigParams->rate = ipConfigParams->rate;
    opConfigParams->constLen = ipConfigParams->constLen;
    opConfigParams->poly0 = ipConfigParams->poly0;
    opConfigParams->poly1 = ipConfigParams->poly1;
    opConfigParams->poly2 = ipConfigParams->poly2;
    opConfigParams->poly3 = ipConfigParams->poly3;
    opConfigParams->yamTh = ipConfigParams->yamTh;
    opConfigParams->frameLen = ipConfigParams->frameLen;
    opConfigParams->relLen = ipConfigParams->relLen;
    opConfigParams->convDist = ipConfigParams->convDist;
    opConfigParams->outOrder = ipConfigParams->outOrder;
    opConfigParams->minSm = ipConfigParams->minSm;
    opConfigParams->maxSm = ipConfigParams->maxSm;
    opConfigParams->stateNum = ipConfigParams->stateNum;
    opConfigParams->traceBack = ipConfigParams->traceBack;
    opConfigParams->readFlag = ipConfigParams->readFlag;
    opConfigParams->decision = ipConfigParams->decision;
    opConfigParams->numBmFrames = opConfigParams->numBmFrames;
    opConfigParams->numDecFrames = opConfigParams->numDecFrames;


}
#if 0
/******************************************************************************
 * Function:  void global_address (Uint32 addr)
 *
 * Purpose:   Returns the gloabl equivalent of a local address
 * Inputs:    Uint32 addr - Local address
 *
 * Returns:   Uint32 addr - Computed global address
 ******************************************************************************/

Uint32 global_address (Uint32 addr)
{
    Uint32  upcastAddr = 0;
    Uint32  coreId = DNUM;

    /* Check if the address is in L2SRAM */
    if ( ( addr >= 0x00800000 ) && ( addr < 0x00900000 ) )
    {
        /* Get the global map based on coreID */
        if ( coreId <= 3 )
        {
            upcastAddr = (Uint32)( (0x10 | coreId) << 24 );
        }
    }

    return ( addr | upcastAddr );
}
#endif

/*****************************************************************************\
 * End of vcp2_test.c
 \*****************************************************************************/



void vcp2Init(Uint32 vcpIdx)
{
	Uint32 i;
	//Power up VCP2

    if(enableVcp2(vcpIdx) != SUCCESS)
    {
        printf("\n[Core%d][main]: Error, Couldn't enable VCP2-%d", coreNum, vcpIdx);

    }

    //Initialize EDMA3 (instance CSL_TPCC_2)
    edma3ModuleInit(coreNum);

// This is one time set up of the ConfigParameters. Also, the EDMA set up is done for the first test ( pre - hibernation test)
    /* VCP_NUM_USERS */
    for (i = 0; i < NUM_USERS; i++)
    {
		/* Calculate the VCP IC values */
		VcpCslCompat(&vcpParameters[i],&vcpParameters[i]);
		VCP2_genIc(&vcpParameters[i], &configIcArray[i]);
		
        setupVcpTputEdma3( &configIcArray[i],
                vcpUserData[i],
                &vcpParameters[i],
                decisions[coreNum][i],
                outParams[coreNum][i],
                &i,
                coreNum);

    } /* end for i */

	//Setup_Interrupt(); 
	//Enable INterrupts and Channels
	
	edma3MultChnlEnable(coreNum);
	
	return;

}
