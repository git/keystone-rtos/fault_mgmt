/**
 *   @file  fm_cleanup_test.c
 *
 *   @brief   
 *      Test code to test IPs then crash a DSP.  Used to test the
 *      FM DSP cleanup and recovery code.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2013-2014 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/* BIOS/XDC Include Files. */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/SysMin.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* CSL Include Files */
#include <ti/csl/csl_chip.h>
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_chipAux.h>

/* Platform Include Files */
#include <ti/platform/platform.h>

/* Remoteproc include file */
#include <ti/instrumentation/remoteproc/remoteproc.h>

/* Fault Management Include File */
#include <ti/instrumentation/fault_mgmt/fault_mgmt.h>

/* Test code headers */
#include "nav_test.h"
#include "paExample.h"
#include "tcp3d_demo.h"
#include "vcp2_test.h"

/* Defines the core number responsible for system initialization. */
#define CORE_SYS_INIT               0

/* Global Variable which keeps track of the core number executing the
 * application. */
uint32_t coreNum = 0xFFFF;

/* Add trace buffer information to the resource table */
extern char * xdc_runtime_SysMin_Module_State_0_outbuf__A;
#define TRACEBUFADDR (uint32_t)&xdc_runtime_SysMin_Module_State_0_outbuf__A
#define TRACEBUFSIZE sysMinBufSize
 
#pragma DATA_SECTION(resources, ".resource_table")
#pragma DATA_ALIGN(resources, 4096)
struct rproc_resource resources[] = {
 {TYPE_TRACE, 0, TRACEBUFADDR,0,0,0, TRACEBUFSIZE, 0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"trace:appleton"},
 {TYPE_COREDUMP_NOTE, 0, (uint32_t) &fault_mgmt_data[0],0,0,0, FAULT_MGMT_DATA_SIZE, 0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"exception_trace_dump"},
};

/**********************************************************************
 ************************* Extern Definitions *************************
 **********************************************************************/

extern Fm_GlobalConfigParams fmGblCfgParams;

/**********************************************************************
 ************************* Unit Test Functions ************************
 **********************************************************************/

Void myExceptionHook(Void)
{
    uint32_t   i, my_core_id, efr_val;
    /* Copy register status into fault management data region for Host */
    Fault_Mgmt_getLastRegStatus();
	
	my_core_id = CSL_chipReadDNUM();

	efr_val    = CSL_chipReadEFR();
	if (efr_val & 0x80000000)
	{
		/* Exception is triggered to be assumed by other core for NMI exceptions
		 * so, don't need to notify other cores since the parent core where the
		 * exception has triggered by an event would notify them, thus eliminating
		 * the recursive exceptions */

		/* Notify Host of crash and return */
	    Fault_Mgmt_notify();
	    return;
	}

    for (i = 0; i < fmGblCfgParams.maxNumCores; i++)
	{
       /* Skip the notification if it is same core */
	   if (i == my_core_id)
	       continue;
		   
	   /* Notify remote DSP cores for exception - WARNING: This will generate NMI
           pulse to the remote DSP cores */
       Fault_Mgmt_notify_remote_core(i);
	}	
    /* Notify Host of crash */
    Fault_Mgmt_notify();
}

/* Exception inducing function */
void exceptionFunc (void)
{
	System_printf ("Causing exception\n");

	/* Attempt to use B0 twice in the same execution stage.  This will
	 * cause a resource sharing exception to occur */
    asm(" \n \
		mvk 1, a1; \n \
		mvk 1, a0; \n \
	  [a1]	mvk 0, b0; \n \
	||[a0]	mv a0, b0; \n \
    ");

    /* The crash dump should show NRP as corresponding to this printf.
     * NRP is used as the PC in the crash dump since that is the return
     * address from the non-maskable interrupt generated by the
     * exception */
    System_printf ("Print after causing the exception\n");
}

/* Task which causes DSP exception.  Need to create a call stack first for
 * crash dump analysis testing purposes. */
void testTask ()
{
    volatile uint32_t j;
    uint32_t          k;

    if (coreNum == CORE_SYS_INIT) {
        /* Initialize peripherals for tests */
        nav_test_setup();
#if !RUNNING_WITH_LINUX         
        Netcp_SetUp();
#endif
#ifdef _BIG_ENDIAN
        tcp3d_init();
#endif
        vcp2Init(coreNum); // Init VCP2_A

        /* run peripheral tests */
        nav_test_run();
#if !RUNNING_WITH_LINUX 
        Netcp_Test();
#endif
#ifdef _BIG_ENDIAN        
        tcp3d_test();
#endif
        runVcp2Test(coreNum);

        System_printf("\nCore %d: Completed IP tests.\n", coreNum);

        /* Call function to cause DSP exception */
	    exceptionFunc();
    }
    else {
        /* Slave cores processing loop */
        while(1) {
            j=0;
            for(k=0; k<500000;k++) {
                j++;
            }
        }
    }
}

int32_t main(int32_t argc, char* argv[])
{
    platform_init_flags  init_flags;
    platform_init_config init_config;    
    Task_Params          taskParams;

    System_printf ("**********************************************\n");
    System_printf ("*************** FM Cleanup Test **************\n");
    System_printf ("**********************************************\n");

    /* Get the core number. */
    coreNum = CSL_chipReadReg (CSL_CHIP_DNUM);

    if (coreNum == CORE_SYS_INIT) {
        /* Turn on all the platform initialize flags */
        memset(&init_config, 0, sizeof(platform_init_config));
        memset(&init_flags, 0x01, sizeof(platform_init_flags));

        init_flags.ddr = 0;
        init_flags.pll = 0;
        init_flags.phy = 0;
        init_flags.ecc = 0;
    }
    else {
        memset(&init_config, 0, sizeof(platform_init_config));
        memset(&init_flags, 0x00, sizeof(platform_init_flags));
    }

    /* Initialize the platform */
    platform_init(&init_flags, &init_config);

    /* Enable the caches. */
    CACHE_setL2Size(CACHE_256KCACHE);

    System_printf ("\nPrint resource table value to avoid compiler optimizing out table: %d\n", 
                   resources[0].type);

    /* Initialize the Task Parameters. */
    Task_Params_init(&taskParams);

    /* Create a task that causes an exception */
    Task_create((Task_FuncPtr)testTask, &taskParams, NULL);

    BIOS_start();
    return 0;
}

