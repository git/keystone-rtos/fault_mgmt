#ifndef PAEXAMPLE_H
#define PAEXAMPLE_H

Int Netcp_SetUp(void); // Function to setup the NETCP subsystem - Power up, Init CPSW, INit QMSS CPPI , INIT PA
Int Netcp_Test(void);  // Simple test which pushes a pkt to Q 640 and checks if it made through to the RX Q after MAC/IP/UDP match

#endif
