/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* expa.c
 *
 * Example PA setup
 */
#include <stdio.h>
#include <stdint.h>

#include "pa1.h"
#include <ti/drv/pa/pa.h>
#include <ti/drv/rm/rm.h>

/* Firmware images */
#include <ti/drv/pa/fw/pafw.h>

#include <ti/csl/cslr_device.h>
#include <ti/drv/cppi/cppi_desc.h>

/* Hibernation Queue to store the command packets*/
extern Int32  gHiberQ;

/*Prototypes*/
/* Functions to delete L2L3 and L4 entries from LUT1 and LUT2 respectively*/

Int deleteL2L3 (Uint32 index , Uint32 dest);
Int deleteL4(Uint32 index);

Int popQ(Int idx)
{
    Cppi_HostDesc *hd;

    hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gHiberQ)) & ~0xf);
    printf("idx: %d, desc: 0x%8x\n", idx, hd);
    if (((uint32_t)hd & 0x10000000) && ((uint32_t)hd & 0x00800000)) {
        Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_HEAD);
    }
    else {
        return (1);
    }
    return(0);
}

/* Download the PA firmware*/
Int paDownloadFirmware (void)
{
  Int i;

  Pa_resetControl (paInst, pa_STATE_RESET);

  /* PDPSs 0-2 use image c1 */
  for (i = 0; i < 3; i++)
    Pa_downloadImage (paInst, i, (Ptr)c1, c1Size);

  /* PDSP 3 uses image c2 */
  Pa_downloadImage (paInst, 3, (Ptr)c2, c2Size);

  /* PDSPs 4-5 use image m */
  for (i = 4; i < 6; i++)
    Pa_downloadImage (paInst, i, (Ptr)m, mSize);

  Pa_resetControl (paInst, pa_STATE_ENABLE);

  return (0);

}

/* Function to add the MAC entry to the LUT1_0 table*/
Int setMacs (void)
{
  Int     i, j;
  Uint16  csize;

  Cppi_HostDesc *hd;
  Qmss_Queue     q;

  /*Ethernet Info , Routing Info, Fail Route Info . The defines are in config.h file*/
  paEthInfo_t   ethInfo[]   = CONFIG_ADDMAC_ETH_INFO;
  paRouteInfo_t routeInfo[] = CONFIG_ADDMAC_ROUTE;
  paRouteInfo_t nfailInfo[] = CONFIG_ADDMAC_NFAIL;
  paCmdReply_t  reply       = CONFIG_PACOM_REPLY;
  
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 , 0x00000000 };
  Uint32 myswinfo2[] = { 0x00000000, 0x00000000 , 0x00000000 };
  
  paReturn_t       paret;
  paEntryHandle_t  retHandle;
  Int              htype;
  Int              cmdDest;

  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */
  Uint32      psCmd2 = 0;

  for (i = 0; i < CONFIG_N_ADDMACS; i++)  {

    /* Pop a descriptor with linked buffer off the packet receive queue and use it
     * to form the command. After use it will be returned to the recycle queue */
    hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
    q.qMgr = 0;
    q.qNum = gvPaTxRecycleQ;
    Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

    csize = hd->buffLen;
    //reply.replyId = CONFIG_PA_CMD_REPLY_ID + i;  /* unique for each add mac command */  
    reply.replyId = 0;  // Save the CmdDst here. This is used in the netcpHibernationRestore function.For AddMAc, this is 0.

    paret = Pa_addMac  ((Pa_Handle)paInst,
                        pa_LUT1_INDEX_NOT_SPECIFIED,
                        &ethInfo[i],
                        &routeInfo[i],
                        &nfailInfo[i],
                        &gvPaL2Handles[i],
                        (paCmd_t) hd->buffPtr,
                        &csize,
                        &reply,
                        &cmdDest);
    
    /* This sets the extended info for descriptors, and this is required so PS info
     * goes to the right spot */                   
    Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

    /* Set the buffer length to the size used. It will be restored when the descriptor
     * is returned */
    Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
    hd->buffLen = csize;
    
    Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);
    

    /* Mark the packet as a configuration packet */
    Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, 4);
       

    if (paret != pa_OK)  {
      //printf ("function setMacs: call to Pa_addMac returned error code %d\n", paret);
      return (-1);
    }
    
    /* Send the command to the PA and wait for the return */
    Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

    /* wait for return buffer */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaTxRecycleQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaTxRecycleQ)) & ~0xf);
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        hd->buffLen = hd->origBufferLen;
        hd->buffPtr = hd->origBuffPtr;
        Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo2);
        Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd2, 4);
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);
        Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, hd->buffLen);

        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        break;
      }  
    }      
    
    if (j == 100)  {
      //printf ("function setMacs: Timeout waiting for return buffer from complete queue to Pa_addMac command\n");
      return (-1);
    }
    
    /* Wait for the PA to return a response */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);
        if (hd->softwareInfo0 != reply.replyId)  {
          //printf ("function setMacs: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n",
              //    hd->softwareInfo0, reply.replyId);
          hd->buffLen = hd->origBufferLen;
          Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

          return (-1);
        }

        paret = Pa_forwardResult ((Pa_Handle)paInst, (void *)hd->buffPtr, &retHandle, &htype, &cmdDest);
        
        /* Reset the buffer lenght and put the descriptor back on the free queue */
       // hd->buffLen = hd->origBufferLen;
       // Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

        if (paret != pa_OK)  {
         // printf ("function setMacs: PA sub-system rejected Pa_addMac command\n");
          return (-1);
        }
        else
        {
        	  //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
        	  Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
//			  	post_write_uart("\r\n\ Saving in Hiber Q - MAC");
        }

        break;
      }
    }

    if (j == 100)  {
     // printf ("function setMacs: Timeout waiting for reply from PA to Pa_addMac command\n");
      return (-1);
    }

  }

  return (0);

}

/* Delete an Entry from the LUT.
 * index is the entry in the LUT
 * dest : 0 - L2 Handle - Delete MAC
 * dest : 1 - L3 Handle - Delete Ip
 */
Int deleteL2L3 (Uint32 index, Uint32 dest)
{
  Int     j;
  Uint16  csize;

  Cppi_HostDesc *hd;
  Qmss_Queue     q;

  paCmdReply_t  reply = CONFIG_PACOM_REPLY;

  Uint32 myswinfo[] = { 0x11112222, 0x33334444 };

  paReturn_t       paret;
  paEntryHandle_t  retHandle;
  Int              htype;
  Int              cmdDest;

  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */


	hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
	q.qMgr = 0;
	q.qNum = gvPaTxRecycleQ;
	Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

	csize = hd->buffLen;

	reply.replyId = dest;  // The dest is stored in the replyId. This is used while save-restore.

	if (dest == 0)
	{
		/* Delete MAC entry if dest = 0*/
		  paret = Pa_delHandle((Pa_Handle)paInst,
					  &gvPaL2Handles[index],
					  (paCmd_t) hd->buffPtr,
					  &csize,
					  &reply,
					  &cmdDest);
	}
	else
	{
	/*Delete Outer IP entry if dest = 1*/
		  paret = Pa_delHandle((Pa_Handle)paInst,
					  &gvPaL3Handles[index],
					  (paCmd_t) hd->buffPtr,
					  &csize,
					  &reply,
					  &cmdDest);
	}

	/* This sets the extended info for descriptors, and this is required so PS info
	 * goes to the right spot */
	Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

	/* Set the buffer length to the size used. It will be restored when the descriptor
	 * is returned */
	Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
	hd->buffLen = csize;

	Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);


	/* Mark the packet as a configuration packet */
	Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, 4);

	if (paret != pa_OK)
    {
	    // System_printf ("function setMacs: call to Pa_addMac returned error code %d\n", paret);
	     return (-1);
	}

	/* Send the command to the PA and wait for the return */
	Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

	/* Wait for the PA to return a response */
	/* Wait for the PA to return a response */
	for (j = 0; j < 100; j++)
    {
	  CycleDelay (1000);

	  if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)
      {
	    hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);

	    if (hd->softwareInfo0 != reply.replyId)
        {
	      //System_printf ("function setMacs: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n",
	              // hd->softwareInfo0, reply.replyId);
	      hd->buffLen = hd->origBufferLen;
	      Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

	      return (-1);
	    }

	    paret = Pa_forwardResult ((Pa_Handle)paInst, (void *)hd->buffPtr, &retHandle, &htype, &cmdDest);

	    /* Reset the buffer lenght and put the descriptor back on the free queue */
	    // hd->buffLen = hd->origBufferLen;
	    // Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

	    if (paret != pa_OK)
        {
	      // System_printf ("function setMacs: PA sub-system rejected Pa_addMac command\n");
	      return (-1);
	    }
	    else
	    {
	    //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
	    	  Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
	    }

	    break;
	  }
	}

	if (j == 100)
    {
	  // System_printf ("function setMacs: Timeout waiting for reply from PA to command\n");
	  return (-1);
	}

  return (0);
}


/* Delete an L4 - UDP entry from the LUT2 table. index corresponds to the order of the port number in the LUT2*/
Int deleteL4(Uint32 index)
{
  Int     j;
  Uint16  csize;

  Cppi_HostDesc *hd;
  Qmss_Queue     q;
  paCmdReply_t  reply = CONFIG_PACOM_REPLY;
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 };

  paReturn_t       paret;
  paEntryHandle_t  retHandle;
  Int              htype;
  Int              cmdDest;

  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */




  {


			 hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
			q.qMgr = 0;
			q.qNum = gvPaTxRecycleQ;
			Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

			csize = hd->buffLen;

			reply.replyId = 3; // The dest is stored in the replyId. This is used while save-restore

			paret = Pa_delL4Handle((Pa_Handle)paInst,
							  gvPaL4Handles[index],
							  (paCmd_t) hd->buffPtr,
							  &csize,
							  &reply,
							  &cmdDest);




		  /* This sets the extended info for descriptors, and this is required so PS info
		   * goes to the right spot */
		  Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

		  /* Set the buffer length to the size used. It will be restored when the descriptor
		   * is returned */
		  Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
		  hd->buffLen = csize;

		  Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);


		  /* Mark the packet as a configuration packet */
		  Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, 4);

		  if (paret != pa_OK)  {
		       //System_printf ("function setMacs: call to Pa_addMac returned error code %d\n", paret);
		       return (-1);
		     }

		     /* Send the command to the PA and wait for the return */
		     Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

		     /* Wait for the PA to return a response */
		     /* Wait for the PA to return a response */
		     for (j = 0; j < 100; j++)  {
		       CycleDelay (1000);

		       if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
		         hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);

		         if (hd->softwareInfo0 != reply.replyId)  {
		           //System_printf ("function setMacs: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n",
		                   // hd->softwareInfo0, reply.replyId);
		           hd->buffLen = hd->origBufferLen;
		           Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

		           return (-1);
		         }

		         paret = Pa_forwardResult ((Pa_Handle)paInst, (void *)hd->buffPtr, &retHandle, &htype, &cmdDest);

		         /* Reset the buffer lenght and put the descriptor back on the free queue */
		        // hd->buffLen = hd->origBufferLen;
		        // Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

		         if (paret != pa_OK)  {
		          // System_printf ("function setMacs: PA sub-system rejected Pa_addMac command\n");
		           return (-1);
		         }
		         else
		         {
				 //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
		         	  Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
		         }

		         break;
		       }
		     }

		     if (j == 100)  {
		      // System_printf ("function setMacs: Timeout waiting for reply from PA  command\n");
		       return (-1);
		     }

		   }

  return (0);

}


/* Configure IP addresses  */
Int setIps (void)
{
  Int     i, j;
  Uint16  csize;

  Cppi_HostDesc *hd;
  Qmss_Queue     q;
 
  paIpInfo_t    ipInfo[]    = CONFIG_ADDIP_IP_INFO;
  Int           macLink[]   = CONFIG_ADDIP_MAC_HANDLE_IDX;
  paRouteInfo_t routeInfo[] = CONFIG_ADDIP_ROUTE;
  paRouteInfo_t nfailInfo[] = CONFIG_ADDIP_NFAIL;
  paCmdReply_t  reply       = CONFIG_PACOM_REPLY;

  paReturn_t      paret;
  paEntryHandle_t retHandle;
  Int             htype;
  Int             cmdDest;
  
  
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 , 0x00000000};
  Uint32 myswinfo2[] = { 0x00000000, 0x00000000 , 0x00000000};


  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */

  Uint32      psCmd2 = 0;

  for (i = 0; i < CONFIG_N_ADDIPS; i++)  {

    /* Pop a descriptor with linked buffer off the packet receive queue and use it
     * to form the command. After use it will be returned to the recycle queue */
    hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
    q.qMgr = 0;
    q.qNum = gvPaTxRecycleQ;
    Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

    csize = hd->buffLen;
   // reply.replyId = CONFIG_PA_CMD_REPLY_ID + i;  /* unique for each add ip command */ //KK
    reply.replyId =  1;


    paret = Pa_addIp ((Pa_Handle)paInst,
                      pa_LUT_INST_NOT_SPECIFIED,
                      pa_LUT1_INDEX_NOT_SPECIFIED,
                      &ipInfo[i],
                      gvPaL2Handles[macLink[i]],
                      &routeInfo[i],
                      &nfailInfo[i],
                      &gvPaL3Handles[i],
                      (paCmd_t)hd->buffPtr,
                      &csize,
                      &reply,
                      &cmdDest);

    /* Set the buffer length to the size used. It will be restored when the descriptor
     * is returned */
    Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
    hd->buffLen = csize;
    
    /* This sets the extended info for descriptors, and this is required so PS info
     * goes to the right spot */                   
    Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);
    

    /* Mark the packet as a configuration packet */
    Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, sizeof(psCmd));
    
    Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);

    if (paret != pa_OK)  {
      //printf ("function setIps: Pa_addIp returned with error code %d\n", paret);
      return (-1);
    }

    /* Send the command to the PA and wait for the return */
    Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
    
    
    /* wait for return buffer */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaTxRecycleQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaTxRecycleQ)) & ~0xf);
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        hd->buffLen = hd->origBufferLen;
        hd->buffPtr = hd->origBuffPtr;
        Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo2);
        Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd2, 4);
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);
        Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, hd->buffLen);

        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        break;
      }  
    }      
    
    if (j == 100)  {
     // printf ("function setIps: Timeout waiting for return buffer from complete queue to Pa_addIp command\n");
      return (-1);
    }
    
   
    /* Wait for the PA to return a response */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);

        if (hd->softwareInfo0 != reply.replyId)  {
         // printf ("function setIps: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n",
                //  hd->softwareInfo0, reply.replyId);
          hd->buffLen = hd->origBufferLen;
          Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
          return (-1);
        }

        paret = Pa_forwardResult ((Pa_Handle)paInst, (void *)hd->buffPtr, &retHandle, &htype, &cmdDest);
        
        /* Reset the buffer lenght and put the descriptor back on the free queue */
        //Kishore commenting
//        hd->buffLen = hd->origBufferLen;
//        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

        if (paret != pa_OK)  {
         //printf ("function setIps: PA sub-system rejected Pa_addIp command\n");
          return (-1);
        }
        else
        {
        	 //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
        	  Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
//			  post_write_uart("\r\n\ Saving in Hiber Q - IP");
        }

        break;
      }
    }

    if (j == 100)  {
     // printf ("function setIps: Timeout waiting for reply from PA to Pa_addIp command\n");
      return (-1);
    }

  }

  return (0);

}

/*Set the UDP port entries in LUT2*/
Int setPorts (void)
{
  Int     i, j;
  Uint16  csize;

  Cppi_HostDesc *hd;
  Qmss_Queue     q;

  Int           ipLink[]    = CONFIG_ADDPORT_IP_HANDLE_IDX;
  Uint16        ports[]     = CONFIG_ADDPORT_PORTS;
  paRouteInfo_t routeInfo[] = CONFIG_ADDPORT_ROUTE;
  paCmdReply_t  reply       = CONFIG_PACOM_REPLY;
  
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 ,0x00000000 };

  Uint32 myswinfo2[] = { 0x00000000, 0x00000000 ,0x00000000 };

  paReturn_t       paret;
  paEntryHandle_t  retHandle;
  Int              htype;
  Int              cmdDest;

  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */

  Uint32 psCmd2 = 0;

  for (i = 0; i < CONFIG_N_ADDPORTS; i++)  {

    /* Pop a descriptor with linked buffer off the packet receive queue and use it
     * to form the command. After use it will be returned to the recycle queue */
    hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
    q.qMgr = 0;
    q.qNum = gvPaTxRecycleQ;
    Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

    csize = hd->buffLen;
  // reply.replyId = CONFIG_PA_CMD_REPLY_ID + i;  /* unique for each add port command */ //KK

    reply.replyId =  3;

    paret = Pa_addPort ((Pa_Handle)paInst,
                        pa_LUT2_PORT_SIZE_16,
                        ports[i],
                        gvPaL3Handles[ipLink[i]],
                        FALSE,                      /* New Entry required */
                        pa_PARAMS_NOT_SPECIFIED,    /* No queue diversion */
                        &routeInfo[i],
                        gvPaL4Handles[i],
                        (paCmd_t)hd->buffPtr,
                        &csize,
                        &reply,
                        &cmdDest);

    /* Set the buffer length to the size used. It will be restored when the descriptor
     * is returned */
    Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
    hd->buffLen = csize;
    
    /* This sets the extended info for descriptors, and this is required so PS info
     * goes to the right spot */                   
    Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);
 

    /* Mark the packet as a configuration packet */
    Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, sizeof(psCmd));
    
    Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);

    if (paret != pa_OK)  {
     // printf ("function setPorts: Pa_addPort returned with error code %d\n", paret);
      return (-1);
    }
    

    /* Send the command to the PA and wait for the return */
    Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

    /* wait for return buffer */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaTxRecycleQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaTxRecycleQ)) & ~0xf);
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        hd->buffLen = hd->origBufferLen;
        hd->buffPtr = hd->origBuffPtr;
        Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo2);
        Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd2, 4);
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);
        Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, hd->buffLen);
        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        break;
      }  
    }      
    
    if (j == 100)  {
      //printf ("function setPorts: Timeout waiting for return buffer from complete queue to Pa_addIp command\n");
      return (-1);
    }
    
    /* Wait for the PA to return a response */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);

        if (hd->softwareInfo0 != reply.replyId)  {
         // printf ("function setIps: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n",
               //    hd->softwareInfo0, reply.replyId);
          hd->buffLen = hd->origBufferLen;
          Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

          return (-1);
        }

        paret = Pa_forwardResult ((Pa_Handle)paInst, (void *)hd->buffPtr, &retHandle, &htype, &cmdDest);
        
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        //Kishore commenting
        //        hd->buffLen = hd->origBufferLen;
//        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        

        if (paret != pa_OK)  {
         // printf ("function setPorts: PA sub-system rejected Pa_addPort command\n");
          return (-1);
        }
        else
        {
        // //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
        	  Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
//			  post_write_uart("\r\n\ Saving in Hiber Q - PORT");
        }

        break;
      }
    }

    if (j == 100)  {
      //printf ("function setPorts: Timeout waiting for reply from PA to Pa_addPort command\n");
      return (-1);
    }

  }

  return (0);

}
                       

/* Restore Function:
cmDst - 0 is LUT1_0 MAC
		1 is LUT1_1 Outer IP
		2 is LUT1_2 Inner Ip
		3 is LUT2
		*/
Int restoreMacIpPort(Uint16 cmdDst ,Uint8* cmdBuff, Uint32 cmdSize)
{

	  Int     j;

	  Cppi_HostDesc *hd;
	  Qmss_Queue     q;

	  Uint32 myswinfo[] = { 0x11112222, 0x33334444 , 0x00000000};

	  Uint32 retVal;

	  Uint8* pData;

	  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */

	  //reply.replyId = CONFIG_PA_CMD_REPLY_ID ;  /* unique for each add mac command */ 

	    /* Pop a descriptor with linked buffer off the packet receive queue and use it
	     * to form the command. After use it will be returned to the recycle queue */
	    hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
	    q.qMgr = 0;
	    q.qNum = gvPaTxRecycleQ;
	    Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);


	    /* This sets the extended info for descriptors, and this is required so PS info
	     * goes to the right spot */
	    Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

	    /* Set the buffer length to the size used. It will be restored when the descriptor
	     * is returned */
	    Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, cmdSize);
	    hd->buffLen = cmdSize;

	    Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)hd, NULL);


	    /* Mark the packet as a configuration packet */
	    Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, 4);


	    Cppi_setData((Cppi_DescType)Cppi_DescType_HOST , (Cppi_Desc *)hd, (uint8_t *) cmdBuff , hd->buffLen );

	    /* Send the command to the PA and wait for the return */
	    Qmss_queuePush (gvTxQ[cmdDst - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);


	    /* wait for return buffer */
	       for (j = 0; j < 100; j++)  {
	         CycleDelay (1000);

	         if (Qmss_getQueueEntryCount (gvPaTxRecycleQ) > 0)   {
	           hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaTxRecycleQ)) & ~0xf);
	           /* Reset the buffer lenght and put the descriptor back on the free queue */
	           hd->buffLen = hd->origBufferLen;
	           Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
	           break;
	         }
	       }

	       if (j == 100)  {
	        // printf ("function restoreMacIpPort: Timeout waiting for return buffer from TX complete queue\n");
	         return (-1);
	       }



	    /* Wait for the PA to return a response */
	    for (j = 0; j < 100; j++)  {
	      CycleDelay (1000);

	      if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
	        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);



            pData = (uint8_t*)hd->buffPtr;   //retval should be 0 to be a valid packet

            retVal = (Uint32)((pData[0] << 24)  | (pData[1] << 16) | (pData[2] << 8) | pData[3]);

	        if ( retVal != pa_OK)  {
	         // System_printf ("function setMacs: PA sub-system rejected Pa_addMac command\n");
	          return (-1);
	        }
	        else
	        {
				// //Push the Response packet to the Hibernation Queue to saved before hibernation and restored after.
	        	Qmss_queuePush (gHiberQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
//				post_write_uart("\r\n\ Restored successfully from  Hiber Q ");
	        }

	        break;
	      }
	    }

	    if (j == 100)  {
	     // printf ("function setMacs: Timeout waiting for reply from PA to Pa_addMac command\n");
	      return (-1);
	    }

	    return 0;
}

#if RM
extern Rm_Handle rm_handle;
#endif

Int setupPa (void)
{
  paSizeInfo_t  paSize;
  paConfig_t    paCfg;
  int           ret;
  int           sizes[pa_N_BUFS];
  int           aligns[pa_N_BUFS];
  void*         bases[pa_N_BUFS];
#if RM
  paStartCfg_t  paStartCfg;
#endif  
  
  memset(&paSize, 0, sizeof(paSizeInfo_t));
  memset(&paCfg, 0, sizeof(paConfig_t));

  paSize.nMaxL2 = CONFIG_MAX_L2_HANDLES;
  paSize.nMaxL3 = CONFIG_MAX_L3_HANDLES;
  paSize.nUsrStats = 0;
  
  
  ret = Pa_getBufferReq(&paSize, sizes, aligns);

  if (ret != pa_OK)  {
   // printf ("setupPa: Pa_getBufferReq() return with error code %d\n", ret);
    return (-1);
  }
  
  /* The first buffer is used as the instance buffer */
  if ((Uint32)paInstBuf & (aligns[0] - 1))  {
   // printf ("setupPa: Pa_getBufferReq requires %d alignment for instance buffer, but address is 0x%08x\n", aligns[0], (Uint32)paInstBuf);
    return (-1);
  }

  if (sizeof(paInstBuf) < sizes[0])  {
   // printf ("setupPa: Pa_getBufferReq requires size %d for instance buffer, have only %d\n", sizes[0], sizeof(paInstBuf));
    return (-1);
  }

  bases[0] = (void *)paInstBuf;

  /* The second buffer is the L2 table */
  if ((Uint32)memL2Ram & (aligns[1] - 1))  {
   // printf ("setupPa: Pa_getBufferReq requires %d alignment for buffer 1, but address is 0x%08x\n", aligns[1], (Uint32)memL2Ram);
    return (-1);
  }

  if (sizeof(memL2Ram) <  sizes[1])  {
    //printf ("setupPa: Pa_getBufferReq requires %d bytes for buffer 1, have only %d\n", sizes[1], sizeof(memL2Ram));
    return (-1);
  }

  bases[1] = (void *)memL2Ram;

  /* The third buffer is the L3 table */
  if ((Uint32)memL3Ram & (aligns[2] - 1))  {
    //printf ("setupPa: Pa_getBufferReq requires %d alignment for buffer 1, but address is 0x%08x\n", aligns[2], (Uint32)memL3Ram);
    return (-1);
  }

  if (sizeof(memL3Ram) <  sizes[2])  {
   // printf ("setupPa: Pa_getBufferReq requires %d bytes for buffer 1, have only %d\n", sizes[2], sizeof(memL3Ram));
    return (-1);
  }

  bases[2] = (void *)memL3Ram;
  
  /* There is no User-defined statistics */
  bases[3] = 0;
  
  paCfg.initTable = TRUE;
  paCfg.initDefaultRoute = TRUE;
  paCfg.baseAddr = CSL_PA_SS_CFG_REGS;
  paCfg.sizeCfg = &paSize;
  
  ret = Pa_create (&paCfg, bases, &paInst);
  if (ret != pa_OK)  {
   // printf ("setupPa: Pa_create returned with error code %d\n", ret);
    return (-1);
  }

#if RM
  paStartCfg.rmHandle = rm_handle;
  Pa_startCfg(paInst, &paStartCfg);
#endif
  
  /* Download the firmware */
  if (paDownloadFirmware ())
    return (-1);


  /* Add the MAC addresses */
  if (setMacs ())
    return (-1);

  /* Add the IP addresses */
  if (setIps())
    return (-1);

  /* Add the ports (TCP/UDP) */
  if (setPorts())
    return (-1);

/* Delete an Entry from the LUT.
 * index is the entry in the LUT
 * dest : 0 - L2 Handle - Delete MAC
 * dest : 1 - L3 Handle - Delete Ip
 */ 

/*parameter 1 = index
  parameter 2 = dest*/
 
  deleteL2L3( 1, 1);

  //delete a MAC entry
  deleteL2L3( 1, 0);

  /*delete an UDP entry from LUT2, 
  The parameter passed is the index  which is used to delete a port number. */

  deleteL4(1);

  return (0);

}






