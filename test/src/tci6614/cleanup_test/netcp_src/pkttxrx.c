/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <stdio.h>
#include <stdint.h>

#include <xdc/runtime/System.h>

#include "pa1.h"

#include <ti/drv/pa/pa.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>
#include <ti/drv/qmss/qmss_drv.h>

/* Example test Packet*/

#pragma DATA_ALIGN(pktMatch, 16)
unsigned char pktMatch[] = {
                            0x10, 0x11, 0x12, 0x13, 0x14, 0x15,                      /* Dest MAC */
                            0x00, 0xe0, 0xa6, 0x66, 0x57, 0x04,                      /* Src MAC  */
                            0x08, 0x00,                                              /* Ethertype = IPv4 */
                            0x45, 0x00, 0x00, 0x6c,                                  /* IP version, services, total length */
                            0x00, 0x00, 0x00, 0x00,                                  /* IP ID, flags, fragment offset */
                            0x05, 0x11, 0xa5, 0x97,                                  /* IP ttl, protocol (UDP), header checksum */
                            0x9e, 0xda, 0x6d, 0x0a,                                  /* Source IP address */
                            0x01, 0x02, 0x03, 0x04,                                  /* Destination IP address */
                            0x12, 0x34, 0x05, 0x55,                                  /* UDP source port, dest port */
                            0x00, 0x58, 0xe1, 0x98,                                  /* UDP len, UDP checksum */
                            0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,          /* 80 bytes of payload data */
                            0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41,
                            0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
                            0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51,
                            0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
                            0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61,
                            0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
                            0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71,
                            0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
                            0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81  };


/* Send the Test Packet - Push to Q 640 or 648 */
Int sendPacket (void)
{
  Cppi_HostDesc *hd;
  Qmss_Queue     q;
  Int            len4;
  
  /* Pop a descriptor without a linked buffer */
  hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvDescQ)) & ~0xf);
  /* Setup the return for the descriptor */
  q.qMgr = 0;
  q.qNum = gvDescQ;
  Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);
    
  len4 = sizeof(pktMatch);
  Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)gAddr((Uint32)pktMatch), len4);
  Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, len4);
  
  /* Send the packet out the mac. It will loop back to PA if the mac/switch have been
   * configured properly */  
  if(cpswLpbkMode == CPSW_LOOPBACK_PA) 
    Qmss_queuePush (gvTxQ[0], hd, len4, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
  else
    Qmss_queuePush (gvTxQ[8], hd, len4, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
    
  /* Give some time for the PA to process the packet */
  CycleDelay (10000);
  
  return (0);
}


/* Check if the packet goes through MAC, OuterIP and UDP classification and reaches the intended Receive Q- destination Q*/

Int findPacket (void)
{
	Cppi_HostDesc *hd;
	Uint8         *uc;
	Int            i;
	Int            j;
	
	/* Wait for a data packet from PA */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvRxPktQ[0]) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxPktQ[0])) & ~0xf);

        if (hd->softwareInfo0 != CONFIG_RCV_PKT_ID)  {
          //printf ("function findPacket: Found an entry in receive queue with swinfo0 = 0x%08x, expected 0x%08x\n", 
                  // hd->softwareInfo0, CONFIG_RCV_PKT_ID);
                 System_printf("\r\nFound an entry in receive queue with swinfo0 wrong");   
          hd->buffLen = hd->origBufferLen;
          Qmss_queuePush (gvRxBufQ, (Ptr)hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
          
          return (-1);
        }
        
        /* See if the packet matches */
        uc = (Uint8 *)hd->buffPtr;
        for (i = 0; i < sizeof (pktMatch); i++)  {
        	if (pktMatch[i] != uc[i])  {
        		//printf ("function findPacket: Byte %d expected 0x%02x, found 0x%02x\n", i, pktMatch[i], uc[i]);
        		//System_flush();
                hd->buffLen = hd->origBufferLen;
        		Qmss_queuePush (gvRxBufQ, (Ptr)hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        		return (-1);
        	}
        }
        
       // System_printf ("function findPacket: Correct packet found\n");
//        System_printf("\r\n\ function findPacket: Correct packet found");
        
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        hd->buffLen = hd->origBufferLen;
        Qmss_queuePush (gvRxBufQ, (Ptr)hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

        break;
      }
    }

    if (j == 100)  {
     // printf ("function findPacket: Timeout waiting for data packet\n");
      return (-1);
    }
    
    return (0);
}


Int getPaStats (void)
{
  Cppi_HostDesc *hd;
  Qmss_Queue     q;
  
  Uint16       csize;
  paReturn_t   paret;
  paCmdReply_t  reply      = CONFIG_PACOM_REPLY;
  Int          cmdDest;
  
  paSysStats_t *stats;
  
  Int    j;
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 };
  Uint32      psCmd = ((Uint32)(4 << 5) << 24);  /* Command word - will be moved to common pa/sa file */
  
  hd     = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvRxBufQ)) & ~0xf);
  q.qMgr = 0;
  q.qNum = gvPaTxRecycleQ;
  Cppi_setReturnQueue (Cppi_DescType_HOST, (Cppi_Desc *)hd, q);

  csize = hd->buffLen;
  reply.replyId = CONFIG_PA_CMD_REPLY_ID;
  
  paret = Pa_requestStats (paInst,
  			 			   TRUE,
  						   (paCmd_t) hd->buffPtr,
  						   &csize,
  						   &reply,
  						   &cmdDest);

  if (paret != pa_OK)  {
    printf ("function getPaStats: call to Pa_requestStats returned error code %d\n", paret);
    return (-1);
  }
  
  /* This sets the extended info for descriptors, and this is required so PS info
   * goes to the right spot */                   
  Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

  /* Set the buffer length to the size used. It will be restored when the descriptor
   * is returned */
  Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc *)hd, csize);
  hd->buffLen = csize;
  
  /* Mark the packet as a configuration packet */
  Cppi_setPSData (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)&psCmd, 4);

   /* Send the request to the PA */
  Qmss_queuePush (gvTxQ[cmdDest - pa_CMD_TX_DEST_0], (Uint32 *)gAddr((Uint32)hd), csize, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
 
   CycleDelay (100000);
   
    /* wait for return buffer */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaTxRecycleQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaTxRecycleQ)) & ~0xf);
        /* Reset the buffer lenght and put the descriptor back on the free queue */      
        hd->buffLen = hd->origBufferLen;
        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        break;
      }  
    }      
    
    if (j == 100)  {
     // printf ("function getPaStats: Timeout waiting for return buffer from complete queue to Pa_addIp command\n");
      return (-1);
    }
 
   /* Wait for the PA to return a response */
    for (j = 0; j < 100; j++)  {
      CycleDelay (1000);

      if (Qmss_getQueueEntryCount (gvPaRespQ) > 0)   {
        hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvPaRespQ)) & ~0xf);

        if (hd->softwareInfo0 != reply.replyId)  {
         // printf ("function getPaStats: Found an entry in PA reply queue with swinfo0 = 0x%08x, expected 0x%08x\n", 
                   //hd->softwareInfo0, reply.replyId);
          hd->buffLen = hd->origBufferLen;
       	  Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
 
          return (-1);
        }


        stats = (paSysStats_t *)Pa_formatStatsReply (paInst, (paCmd_t)hd->buffPtr);
        if (stats == NULL)  {
        	//printf ("function getPaStats: Pa_formatStats returned invalid stats\n");
        	hd->buffLen = hd->origBufferLen;
        	Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        	return (-1);
        }
        
        hd->buffLen = hd->origBufferLen;
        Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);
        
        break;
      }
    }
    
   return (0);
}
