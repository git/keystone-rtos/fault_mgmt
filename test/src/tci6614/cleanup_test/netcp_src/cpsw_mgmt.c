/**  
 * @file cpsw_mgmt.c
 *
 * @brief 
 *  This file holds all the Ethernet subsystem (CPSW + MDIO + SGMII) components
 *  initialization and setup code.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2009, Texas Instruments, Inc.
 *  @n   Use of this software is controlled by the terms and conditions found 
 *  @n   in the license agreement under which this software has been supplied.
 *  ===========================================================================
 *  \par 
 */
/* C Standard library Include */
#include <string.h>

/* Chip Level definitions include */
#include <ti/csl/csl_chip.h>

/* CSL EMAC include */
#include <ti/csl/csl_cpsw.h>
#include <ti/csl/csl_cpsgmii.h>
#include <ti/csl/csl_cpsgmiiAux.h>
#include <ti/csl/csl_mdio.h>
#include <ti/csl/csl_mdioAux.h>

/* BootCfg module include */
#include <ti/csl/csl_bootcfg.h>
#include <ti/csl/csl_bootcfgAux.h>

/** Number of ports in the ethernet subsystem */
#define         NUM_PORTS                   3u

/** Number of MAC/GMII ports in the ethernet switch */
#define         NUM_MAC_PORTS               2u

/** ============================================================================
 *   @n@b Init_SGMII
 *
 *   @b Description
 *   @n SGMII peripheral initialization code.
 *
 *   @param[in]  
 *   @n macPortNum      MAC port number for which the SGMII port setup must
 *                      be performed.
 * 
 *   @return
 *   @n None
 * =============================================================================
 */
void Initialize_SGMII (unsigned int macPortNum)
{  
    CSL_SGMII_ADVABILITY    sgmiiCfg;

    /* Reset the port before configuring it */
    CSL_SGMII_doSoftReset (macPortNum);        
    while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);   

    /* Hold the port in soft reset and set up
     * the SGMII control register:
     *      (1) Enable Master Mode
     *      (2) Enable Auto-negotiation
     */
    CSL_SGMII_startRxTxSoftReset (macPortNum);        
    CSL_SGMII_enableMasterMode (macPortNum);
    CSL_SGMII_enableAutoNegotiation (macPortNum);
    CSL_SGMII_endRxTxSoftReset (macPortNum);        

    /* Setup the Advertised Ability register for this port:
     *      (1) Enable Full duplex mode
     *      (2) Enable Auto Negotiation
     *      (3) Enable the Link
     */
    sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
    sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
    sgmiiCfg.bLinkUp        =   1;
    CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

    /* Configure the Tx, Rx, and Aux Configuration Registers. */
    CSL_SGMII_setTxConfig (macPortNum, 0x81921);
    CSL_SGMII_setRxConfig (macPortNum, 0x81023);
    CSL_SGMII_setAuxConfig (macPortNum, 0x9);

    /* Configure the SERDES */
    CSL_BootCfgSetSGMIIConfigPLL (0x229);
    CSL_BootCfgSetSGMIIRxConfig (0, 0x701);
    CSL_BootCfgSetSGMIITxConfig (0, 0x101);
    CSL_BootCfgSetSGMIIRxConfig (1, 0x701);
    CSL_BootCfgSetSGMIITxConfig (1, 0x101);

    /* All done with configuration. Return Now. */
    return;
}

/** ============================================================================
 *   @n@b Init_MAC
 *
 *   @b Description
 *   @n This API initializes the CPGMAC Sliver (MAC Port) port.
 *
 *   @param[in]  
 *   @n macPortNum      MAC port number for which the initialization must be done.
 *
 *   @param[in]  
 *   @n macAddress      MAC address to configure on this port.
 * 
 *   @param[in]  
 *   @n mtu             Maximum Frame length to configure on this port.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
void Init_MAC (unsigned int macPortNum, unsigned char macAddress[6], unsigned int mtu)
{
    /* Reset MAC Sliver 0 */            
    CSL_CPGMAC_SL_resetMac (macPortNum);
    while (CSL_CPGMAC_SL_isMACResetDone (macPortNum) != TRUE);

    /* Setup the MAC Control Register for this port:
     *      (1) Enable Full duplex
     *      (2) Enable GMII
     *      (3) Enable Gigabit 
     *      (4) Enable External Configuration. This enables 
     *          the "Full duplex" and "Gigabit" settings to be
     *          controlled externally from SGMII
     *      (5) Don't enable any control/error/short frames
     */
    CSL_CPGMAC_SL_enableFullDuplex (macPortNum);
    CSL_CPGMAC_SL_enableGMII (macPortNum);
    CSL_CPGMAC_SL_enableGigabit (macPortNum);
    CSL_CPGMAC_SL_enableExtControl (macPortNum);

    /* Configure the MAC address for this port */
    CSL_CPSW_3GF_setPortMACAddress (macPortNum, macAddress);

    /* Configure VLAN ID/CFI/Priority.
     *
     * For now, we are not using VLANs so just configure them
     * to all zeros.
     */
    CSL_CPSW_3GF_setPortVlanReg (macPortNum, 0, 0, 0);

    /* Configure the Receive Maximum length on this port,
     * i.e., the maximum size the port can receive without
     * any errors.
     *
     * Set the Rx Max length to the MTU configured for the
     * interface.
     */
    CSL_CPGMAC_SL_setRxMaxLen (macPortNum, mtu);  

    /* Done setting up the MAC port */
    return;
}

/** ============================================================================
 *   @n@b Init_MDIO
 *
 *   @b Description
 *   @n Not supported at moment. MDIO is not simulated yet.
 *
 *   @param[in]  
 *   @n None
 *
 *   @return
 *   @n None
 * =============================================================================
 */
void Init_MDIO (void)
{
    /* Return success. */        
    return;        
}

/** ============================================================================
 *   @n@b Init_Switch
 *
 *   @b Description
 *   @n This API sets up the ethernet switch subsystem and its Address Lookup 
 *      Engine (ALE) in "Switch" mode.
 *
 *   @param[in]  
 *   @n mtu             Maximum Frame length to configure on the switch.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
void Init_Switch (unsigned int mtu)
{
    CSL_CPSW_3GF_PORTSTAT               portStatCfg;

    /* Enable the CPPI port, i.e., port 0 that does all 
     * the data streaming in/out of EMAC.
     */
    CSL_CPSW_3GF_enablePort0 ();
    CSL_CPSW_3GF_disableVlanAware ();
    CSL_CPSW_3GF_setPort0VlanReg (0, 0, 0);
    CSL_CPSW_3GF_setPort0RxMaxLen (mtu);

    /* Enable statistics on both the port groups:
     *
     * MAC Sliver ports -   Port 1, Port 2
     * CPPI Port        -   Port 0
     */
    portStatCfg.p0AStatEnable   =   1;
    portStatCfg.p0BStatEnable   =   1;
    portStatCfg.p1StatEnable    =   1;
    portStatCfg.p2StatEnable    =   1;
    CSL_CPSW_3GF_setPortStatsEnableReg (&portStatCfg);

    /* Setup the Address Lookup Engine (ALE) Configuration:
     *      (1) Enable ALE.
     *      (2) Clear stale ALE entries.
     *      (3) Disable VLAN Aware lookups in ALE since
     *          we are not using VLANs by default.
     *      (4) No Flow control
     *      (5) Configure the Unknown VLAN processing 
     *          properties for the switch, i.e., which 
     *          ports to send the packets to.
     */
    CSL_CPSW_3GF_enableAle ();
    CSL_CPSW_3GF_clearAleTable ();
    CSL_CPSW_3GF_disableAleVlanAware ();
    CSL_CPSW_3GF_disableAleTxRateLimit ();
    CSL_CPSW_3GF_setAlePrescaleReg (125000000u/1000u);
    CSL_CPSW_3GF_setAleUnkownVlanReg (7, 3, 3, 7);

    /* Done with switch configuration */
    return;
}


/** ============================================================================
 *   @n@b Switch_update_addr
 *
 *   @b Description
 *   @n This API add/delete entries in the Address Lookup Engine (ALE) in "Switch" mode.
 *
 *   @param[in]  
 *   @n portNum         Switch port number.
 
 *   @param[in]  
 *   @n macAddress      MAC address to configure on the switch.
 * 
 *   @param[in]  
 *   @n add             0:add; 1:delete.
 *
 *   @return
 *   @n None
 *
 *   @Note  It supports "add" operation only now.           
 * =============================================================================
 */
int Switch_update_addr (unsigned int portNum, unsigned char macAddress[6], Uint16 add)
{
    unsigned int                              i;
    CSL_CPSW_3GF_ALE_PORTCONTROL        alePortControlCfg;
    CSL_CPSW_3GF_ALE_UNICASTADDR_ENTRY  ucastAddrCfg;


    /* Configure the address in "Learning"/"Forward" state */
    alePortControlCfg.portState             =   ALE_PORTSTATE_FORWARD;
    alePortControlCfg.dropUntaggedEnable    =   0;
    alePortControlCfg.vidIngressCheckEnable =   0;
    alePortControlCfg.noLearnModeEnable     =   0;
    alePortControlCfg.mcastLimit            =   0;
    alePortControlCfg.bcastLimit            =   0;

    CSL_CPSW_3GF_setAlePortControlReg (portNum, &alePortControlCfg);

    /* Program the ALE with the MAC address.
     *
     * The ALE entries determine the switch port to which any
     * matching received packet must be forwarded to.
     */
    /* Get the next free ALE entry to program */
    for (i = 0; i < CSL_CPSW_3GF_NUMALE_ENTRIES; i++)
    {
        if (CSL_CPSW_3GF_getALEEntryType (i) == ALE_ENTRYTYPE_FREE)
        {
            /* Found a free entry */
            break;                    
        }
    }
    if (i == CSL_CPSW_3GF_NUMALE_ENTRIES)
    {
        /* No free ALE entry found. return error. */            
        return -1;            
    }
    else
    {
        /* Found a free ALE entry to program our MAC address */            
        memcpy (ucastAddrCfg.macAddress, macAddress, 6);    // Set the MAC address
        ucastAddrCfg.ucastType      =      ALE_UCASTTYPE_UCAST_NOAGE;   // Add a permanent unicast address entryALE_UCASTTYPE_UCAST_NOAGE.
        ucastAddrCfg.secureEnable   =      FALSE;   
        ucastAddrCfg.blockEnable    =      FALSE;   
        ucastAddrCfg.portNumber     =      portNum;   // Add the ALE entry for this port

        /* Setup the ALE entry for this port's MAC address */
        CSL_CPSW_3GF_setAleUnicastAddrEntry (portNum, &ucastAddrCfg);            
    }

    /* Done with upading address */
    return 0;
}


/** ============================================================================
 *   @n@b Init_Cpsw
 *
 *   @b Description
 *   @n This API sets up the entire ethernet subsystem and all its associated
 *      components.
 *
 *   @param[in]  
 *   @n None
 * 
 *   @return
 *   @n None
 * =============================================================================
 */
int Init_Cpsw (void)
{                      
    unsigned int       macPortNum, mtu = 1518;
    unsigned char        macSrcAddress [2][6] =  {{0x10, 0x11, 0x12, 0x13, 0x14, 0x15},
                                          {0x20, 0x21, 0x22, 0x23, 0x24, 0x25}};
    unsigned char        macAddress0 [6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05};  /* MAC address for (CPPI) Port 0 */ 
    unsigned char        macAddress1 [6] = {0x10, 0x11, 0x12, 0x13, 0x14, 0x15};  /* MAC address for (EMAC1) Port 1 */ 
    unsigned char        macAddress2 [6] = {0x20, 0x21, 0x22, 0x23, 0x24, 0x25};  /* MAC address for (EMAC2) Port 2 */
    
    /* Initialize the SGMII/Sliver submodules for the 
     * two corresponding MAC ports.
     */
    for (macPortNum = 0; macPortNum < NUM_MAC_PORTS; macPortNum++)
    {
        Initialize_SGMII (macPortNum);            
        Init_MAC (macPortNum, &macSrcAddress[macPortNum][0], mtu);
    }

    /* Setup the Phys by initializing the MDIO */
    Init_MDIO ();

    /* Setup the Ethernet switch finally. */
    Init_Switch (mtu);
    
    Switch_update_addr(0, macAddress0, 0);
    Switch_update_addr(1, macAddress1, 0);
    Switch_update_addr(2, macAddress2, 0);

    /* CPSW subsystem setup done. Return success */
    return 0;    
}
