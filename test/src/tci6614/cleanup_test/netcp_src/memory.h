/*
 *
 * Copyright (C) 2010-2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



#ifndef _MEMORY_H
#define _MEMORY_H

/* memory.h 
 * global memory declaration */
#include <stdio.h>
#include <stdint.h>
//#include <xdc/std.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/pa/pa.h>
#include "config.h"

#define CACHE_LINESZ    128
#define SYS_ROUND_UP(x,y)   ((x) + ((y) -1))/(y)*(y)


/* PASS CDMA handle */
extern Cppi_Handle gvCppiHandle;  


/* PASS CDMA rx/tx channel handles */
extern Cppi_ChHnd  gvTxChHnd[10];
extern Cppi_ChHnd  gvRxChHnd[24];


/* Queue handles */
extern Int32  gvDescQ;                       /* Queue holding unassigned descriptors */
extern Int32  gvTxQ[10];                     /* 10 PA hard assigned transmit queues */
extern Int32  gvRxPktQ[CONFIG_N_RX_QS];      /* Queues to receive data packets */
extern Int32  gvPaRespQ;                     /* Queue to received PA command responses */
extern Int32  gvPaTxRecycleQ;                /* Return queue for transmit descriptors/buffers */
extern Int32  gvRxBufQ;                      /* Queue of buffers for receive packets */

/* Flow handle */
extern Cppi_FlowHnd gvFlowHnd;

/* Memory used for the linking RAM and descriptor RAM */
//extern Uint64 memLinkRam[CONFIG_NUM_DESC];
extern Uint8 memDescRam[CONFIG_NUM_DESC * CONFIG_SIZE_DESC];
extern Uint8 memRxBuffer[CONFIG_N_RX_BUFFERS][CONFIG_RX_BUFFER_SIZE];

/* PA instance */
extern Uint8 paInstBuf[SYS_ROUND_UP(CONFIG_BUFSIZE_PA_INST, CACHE_LINESZ)];
extern Pa_Handle paInst;

/* Memory used for PA handles */
extern Uint8 memL2Ram[SYS_ROUND_UP(CONFIG_BUFSIZE_L2_TABLE, CACHE_LINESZ)];
extern Uint8 memL3Ram[SYS_ROUND_UP(CONFIG_BUFSIZE_L3_TABLE, CACHE_LINESZ)];

extern paHandleL2L3_t gvPaL2Handles[CONFIG_MAX_L2_HANDLES];
extern paHandleL2L3_t gvPaL3Handles[CONFIG_MAX_L3_HANDLES];
extern paHandleL4_t   gvPaL4Handles[CONFIG_MAX_L4_HANDLES];




#endif /* _MEMORY_H */

