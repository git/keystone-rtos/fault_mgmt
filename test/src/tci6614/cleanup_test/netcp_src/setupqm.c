/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <stdio.h>
#include <stdint.h>
#include "pa1.h"
#include <ti/drv/cppi/cppi_desc.h>

#include <xdc/runtime/System.h>

/* This function depends upon the navigator test initializing the
 * QMSS LLD, and will only add to it. */
Int32 setupQmMem (void)
{
  Qmss_MemRegInfo  memInfo;
  Cppi_DescCfg     descCfg;
  Int32            result;
  Int32            n;

  memset (memDescRam,      0, sizeof (memDescRam));

  /* Setup a single memory region for descriptors */
  memset (memDescRam, 0, sizeof(memDescRam));
  memInfo.descBase       = (Uint32 *)gAddr((Uint32)memDescRam);
  memInfo.descSize       = CONFIG_SIZE_DESC;
  memInfo.descNum        = CONFIG_NUM_DESC;
  memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
  memInfo.memRegion      = Qmss_MemRegion_MEMORY_REGION1;
  memInfo.startIndex     = 4096;

  result = Qmss_insertMemoryRegion (&memInfo);
  if (result < QMSS_SOK)  {
   // System_printf ("function setupQmMem: Qmss_insertMemoryRegion returned error code %d\n", result);
    return (-1);
  }

  
  /* Initialize the descriptors. This function opens a general
   * purpose queue and initializes the memory from region 0, placing
   * the initialized descriptors onto that queue */
  descCfg.memRegion         = (Qmss_MemRegion)result;
  descCfg.descNum           = CONFIG_NUM_DESC;
  descCfg.destQueueNum      = CONFIG_INIT_DESC_QUEUE;
  descCfg.queueType         = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
  descCfg.initDesc          = Cppi_InitDesc_INIT_DESCRIPTOR;
  descCfg.descType          = Cppi_DescType_HOST;
  descCfg.returnQueue.qNum  = QMSS_PARAM_NOT_SPECIFIED;
  descCfg.returnQueue.qMgr  = 0;
  descCfg.epibPresent       = Cppi_EPIB_EPIB_PRESENT;
  
  descCfg.cfg.host.returnPolicy     = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
  descCfg.cfg.host.psLocation       = Cppi_PSLoc_PS_IN_DESC;

  gvDescQ = Cppi_initDescriptor (&descCfg, (Uint32 *)&n);

  if (n != descCfg.descNum)  {
    //printf ("function setupQmMem: expected %d descriptors to be initialized, only %d are initialized\n", descCfg.descNum, n);
    return (-1);
  }

  return (0);
}
  
Int32 setupCpdma (void)
{
  Cppi_CpDmaInitCfg cpdmaCfg;
  Cppi_RxChInitCfg  rxChCfg;
  Cppi_TxChInitCfg  txChCfg;

  Int32   i;
  Uint8 isAlloc;

  memset(&cpdmaCfg, 0, sizeof(Cppi_CpDmaInitCfg));
  cpdmaCfg.dmaNum           = Cppi_CpDma_PASS_CPDMA;

  gvCppiHandle = Cppi_open (&cpdmaCfg);
  if (gvCppiHandle == NULL)  {
   // printf ("function setupCpdma: cppi_Open returned NULL cppi handle\n");
    return (-1);
  }

  /* Open all 24 rx channels */
  rxChCfg.rxEnable = Cppi_ChState_CHANNEL_DISABLE;

  for (i = 0; i < 24; i++)  {
    rxChCfg.channelNum = i;
    gvRxChHnd[i]       = Cppi_rxChannelOpen (gvCppiHandle, &rxChCfg, &isAlloc);

    if (gvRxChHnd[i] == NULL)  {
     // System_printf ("function setupCpdma: cppi_RxChannelOpen returned NULL handle for channel number %d\n", i);
      return (-1);
    }
    
    Cppi_channelEnable (gvRxChHnd[i]);
  }

  /* Open all 9 tx channels.  */
  txChCfg.priority     = 2;
  txChCfg.txEnable     = Cppi_ChState_CHANNEL_DISABLE;
  txChCfg.filterEPIB   = FALSE;
  txChCfg.filterPS     = FALSE;
  txChCfg.aifMonoMode  = FALSE;
  

  for (i = 0; i < 9; i++)  {
    txChCfg.channelNum = i;
    gvTxChHnd[i]       = Cppi_txChannelOpen (gvCppiHandle, &txChCfg, &isAlloc);

    if (gvTxChHnd[i] == NULL)  {
     // System_printf ("function setupCpdma: cppi_TxChannelOpen returned NULL handle for channel number %d\n", i);
      return (-1);
    }
    
    Cppi_channelEnable (gvTxChHnd[i]);
  }
  
  /* Clear CPPI Loobpack bit in PASS CDMA Global Emulation Control Register */
  Cppi_setCpdmaLoopback(gvCppiHandle, 0);   

  return (0);

}


/* Setup all the queues used in the example */
Int32 setupQueues (void)
{
  Int32    i;
  Uint8  isAlloc;
  
  Uint32 myswinfo[] = { 0x11112222, 0x33334444 };

  Cppi_HostDesc *hd;

  
  /* The 9 PA transmit queues (corresponding to the 9 tx cdma channels */
  for (i = 0; i < 9; i++)  {

    gvTxQ[i] = Qmss_queueOpen (Qmss_QueueType_PASS_QUEUE, QMSS_PARAM_NOT_SPECIFIED, &isAlloc);

    if (gvTxQ[i] < 0)  {
      //printf ("function setupQueues: Qmss_queueOpen failed for PA transmit queue number %d\n", 640+i);
      return (-1);
    }
    
    Qmss_setQueueThreshold (gvTxQ[i], 1, 1);
    

  }

  /* The queue to hold the received data packets */
  for (i = 0; i < CONFIG_N_RX_QS; i++)  {
    gvRxPktQ[i] = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, CONFIG_FIRST_RX_PKT_Q + i, &isAlloc);

    if (gvRxPktQ[i] < 0)  {
     // printf ("function setupQueues: Qmss_queueOpen failed for packet receive queue number %d\n", CONFIG_FIRST_RX_PKT_Q);
      return (-1);
    }
  }

  /* The queue to hold configuration responses from the PA */
  gvPaRespQ = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, CONFIG_PKT_PA_CFG_RX_Q, &isAlloc);

  if (gvPaRespQ < 0)  {
   // System_printf ("function setupQueues: Qmss_queueOpen failed for PA config response queue number %d\n", CONFIG_PKT_PA_CFG_RX_Q);
    return (-1);
  }


  /* The queue to recycle descriptors from transmit packets */
  gvPaTxRecycleQ = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, CONFIG_PKT_PA_TX_RECYCLE_Q, &isAlloc);

  if (gvPaTxRecycleQ < 0)  {
   // printf ("function setupQueues: Qmss_queueOpen failed for PA transmit recycle queue number %d\n", CONFIG_PKT_PA_TX_RECYCLE_Q);
    return (-1);
  }


  /* The queue to hold descriptors with linked buffers. These descriptors/buffers will be used by flow 0
   * for packets received from PA */
  gvRxBufQ = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, CONFIG_PKT_PA_RX_BUFFER_Q, &isAlloc);

  if (gvRxBufQ < 0)  {
   // System_printf ("function setupQueues: Qmss_queueOpen failed for receive packet buffer queue %d\n", CONFIG_PKT_PA_RX_BUFFER_Q);
    return (-1);
  }


  /* Pop descriptors off the free descriptor queue, attach buffers and drop the them into the 
   * rx buffer queue */
  for (i = 0; i < CONFIG_N_RX_BUFFERS; i++)  {

    hd = (Cppi_HostDesc *)(((Uint32)Qmss_queuePop (gvDescQ)) & ~0xf);
    if (hd == NULL)  {
     // System_printf ("function setupQueues: Qmss_queuePop returned NULL on pop number %d from queue number %d\n", i, gvDescQ);
      return (-1);
    }

    Cppi_setOriginalBufInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)gAddr((Uint32)(memRxBuffer[i])), CONFIG_RX_BUFFER_SIZE);
    Cppi_setData         (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)gAddr((Uint32)(memRxBuffer[i])), CONFIG_RX_BUFFER_SIZE);
    Cppi_setPacketLen    (Cppi_DescType_HOST, (Cppi_Desc *)hd, CONFIG_RX_BUFFER_SIZE);
    Cppi_setSoftwareInfo (Cppi_DescType_HOST, (Cppi_Desc *)hd, (Uint8 *)myswinfo);

    hd->nextBDPtr = NULL;
    Qmss_queuePush (gvRxBufQ, hd, hd->buffLen, CONFIG_SIZE_DESC, Qmss_Location_TAIL);

  }

  return (0);
  
}


/* Configure flows */
int setupFlow0 (void)
{
  Cppi_RxFlowCfg  rxFlowCfg;
  Uint8           isAlloc;

  uint32_t                rec[8];  // For hibernation save

  /* Configure Rx flow */
  rxFlowCfg.flowIdNum      = 0;
  rxFlowCfg.rx_dest_qnum   = CONFIG_PKT_PA_CFG_RX_Q;   /* Override in PA */
  rxFlowCfg.rx_dest_qmgr   = 0;
  rxFlowCfg.rx_sop_offset  = 0;
  rxFlowCfg.rx_ps_location = Cppi_PSLoc_PS_IN_DESC;
  rxFlowCfg.rx_desc_type   = Cppi_DescType_HOST;
  rxFlowCfg.rx_error_handling = 0;
  rxFlowCfg.rx_psinfo_present = 1;
  rxFlowCfg.rx_einfo_present  = 1;

  rxFlowCfg.rx_dest_tag_lo = 0;
  rxFlowCfg.rx_dest_tag_hi = 0;
  rxFlowCfg.rx_src_tag_lo  = 0;
  rxFlowCfg.rx_src_tag_hi  = 0;

  rxFlowCfg.rx_size_thresh0_en = 0;
  rxFlowCfg.rx_size_thresh1_en = 0;
  rxFlowCfg.rx_size_thresh2_en = 0;
  rxFlowCfg.rx_dest_tag_lo_sel = 0;
  rxFlowCfg.rx_dest_tag_hi_sel = 0;
  rxFlowCfg.rx_src_tag_lo_sel  = 0;
  rxFlowCfg.rx_src_tag_hi_sel  = 0;

  rxFlowCfg.rx_fdq1_qnum = 0;
  rxFlowCfg.rx_fdq1_qmgr = 0;
  rxFlowCfg.rx_fdq0_sz0_qnum = gvRxBufQ;
  rxFlowCfg.rx_fdq0_sz0_qmgr = 0;

  rxFlowCfg.rx_fdq3_qnum = 0;
  rxFlowCfg.rx_fdq3_qmgr = 0;
  rxFlowCfg.rx_fdq2_qnum = 0;
  rxFlowCfg.rx_fdq2_qmgr = 0;

  rxFlowCfg.rx_size_thresh1 = 0;
  rxFlowCfg.rx_size_thresh0 = 0;
    
  rxFlowCfg.rx_fdq0_sz1_qnum = 0;
  rxFlowCfg.rx_fdq0_sz1_qmgr = 0;
  rxFlowCfg.rx_size_thresh2  = 0;

  rxFlowCfg.rx_fdq0_sz3_qnum = 0;
  rxFlowCfg.rx_fdq0_sz3_qmgr = 0;
  rxFlowCfg.rx_fdq0_sz2_qnum = 0;
  rxFlowCfg.rx_fdq0_sz2_qmgr = 0;


  gvFlowHnd = Cppi_configureRxFlow (gvCppiHandle, &rxFlowCfg, &isAlloc);
  if (gvFlowHnd == NULL)  {
    System_printf ("function setupFlow0: cppi_ConfigureRxFlow returned NULL\n");
    return (-1);
  }

/* Take a log of the FLOW 0 config to be saved before Hibernation*/
  memset(rec,0,sizeof(rec));
  rec[0] = (rxFlowCfg.rx_einfo_present << 30) +
		  	  (rxFlowCfg.rx_psinfo_present << 29) +
		  	  (rxFlowCfg.rx_error_handling << 28) +
                 (rxFlowCfg.rx_desc_type << 26) +
                 (rxFlowCfg.rx_sop_offset << 16) +
                 (rxFlowCfg.rx_dest_qmgr << 16) + rxFlowCfg.rx_dest_qnum;
  rec[2] = (rxFlowCfg.rx_src_tag_lo_sel << 24);
  rec[3] = (rxFlowCfg.rx_fdq0_sz0_qmgr << 28) +
                  rxFlowCfg.rx_fdq0_sz0_qnum << 16;

  return (0);
}


Int32 setupQmCdma (void)
{
  if (setupQmMem())
    return (-1);

  if (setupCpdma ())
    return (-1);
    
  if (setupQueues ())
    return (-1);

  if (setupFlow0 ())
    return (-1);

  return (0);
}


void tearDownQmCdma (void)
{
  Int32 i;
 
  Qmss_queueClose (gvDescQ);
  Qmss_queueClose (gvPaRespQ);
  Qmss_queueClose (gvPaTxRecycleQ);
  Qmss_queueClose (gvRxBufQ);

  for (i = 0; i < 9; i++)
    Qmss_queueClose (gvTxQ[i]);

  for (i = 0; i < CONFIG_N_RX_QS; i++)
    Qmss_queueClose (gvRxPktQ[i]);

  for (i = 0; i < 24; i++)
    Cppi_channelClose (gvRxChHnd[i]);

  for (i = 0; i < 9; i++)
    Cppi_channelClose (gvTxChHnd[i]);

}
