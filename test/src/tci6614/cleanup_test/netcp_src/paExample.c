/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/*  pa1.c
 *
 *  A simple example of using the PA/QM/SGMII 
 *
 *
 *  The overall system setup is shown in the following figure:                             /------------------\
 *                                                                                         | Q899             |
 *                                                                                         | Teardown         |
 *                                            /------------------\                         |------------------|
 *                                            |                  |<------------------------| Q900             |
 *                                            |  Packet Created  |-----\                   | Free descriptor  |
 *                                            |  by GEM          |     |                   |------------------|
 *                                            |                  |     |                   | Q901             |
 *                                            \------------------/     |  /------------\   | Free buffer      |-\
 *                                                                     |  |            |   |------------------| | Flow 0
 *                                                                     |  |            |   | Q902             |_|
 *                                                                     |  |            |   | PA cmd reply     | |
 *                                                                    \|/ |            |   |------------------| |
 *  /-----------------------------------------------------------------------\          |   | Q903             | |
 *  | Q640  | Q641  | Q642  | Q643  | Q644  | Q645  | Q646  | Q647  | Q648  |          |   | Pkt Send recycle | |  
 *  | PaTx0 | PaTx1 | PaTx2 | PaTx3 | PaTx4 | PaTx5 | SaTx0 | SaTx1 | SwTx0 |          |   |------------------| |   
 *  \-----------------------------------------------------------------------/          \-->| Q904             |<-  
 *                                                                     |                   | Pkt Receive      |<--\     
 *                                                                     |                   \------------------/   |      
 *                                                                     |                                          |
 *                                                                     |                                          |
 *           /------------\    /------------\    /----------------\    |                                          |
 *           |            |    |            |    |         Port 0 |    |                                          |
 *           |         /--|<---|------------|<---|----------------|<---/                                          |
 *           |  SGMII  |  |    |   EMAC     |    |  Switch        |                                               |
 *           |         \--|--->|------------|--->|-\              |                                               |
 *           |            |    |            |    | |Port 2 Port 1 |                                               |
 *           \------------/    \------------/    \----------------/                                               |
 *                                                 |                                                              |
 *                      /--------------------------/                                                              |
 *                      |                                                                                         |
 *               /------------\                                                                                   |
 *               |            |                                                                                   |
 *               |     PA     |-----------------------------------------------------------------------------------/
 *               |            |
 *               \------------/
 *
 *
 *
 *
 *  The example is meant to be the simplest possible example which demonstrates configuration of the QM, CDMA, flows
 *  PA, and switch/emac/SGMII.
 *
 *
 *  One manual entry is made in the switch tables to forward route mac address 00:01:02:03:04:aa to ports 0 and 1.
 *
 *  The PA is configured to forward packets with headers:
 *
 *          dest mac  = 10:11:12:13:14:15, ethertype = 0x0800 (ipv4)
 *          dest IP   = 1.2.3.4, protocol = any
 *          dest port = 0x0555
 *
 *      To queue 902 using flow 0.
 *
 *
 *  Flow 0 is configured to use Q901 as the free buffer queue. No size selection of source queues is made.
 *
 *
 *  See file pa1config.h for build time configuration.
 *
 */
 
#include <stdio.h>
#include <stdint.h>
#include <xdc/runtime/System.h>
#include <ti/drv/pa/pa.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
 
#include <ti/drv/qmss/qmss_drv.h>

#include "pa1.h"


/* extern's*/

Int32  gHiberQ;
extern Int32  gvRxBufQ;
extern Int paDownloadFirmware (void);
extern Uint32 numLut2;
extern void   setupFlow0();

void netcp_Init(void);
Int Netcp_SetUp (void);
Int Netcp_Test(void);

/* Addresses used to disable the PASS PDSP's */
#define PASS_PDSP0	0x02001000
#define PASS_PDSP1	0x02001100
#define PASS_PDSP2	0x02001200
#define PASS_PDSP3	0x02001300
#define PASS_PDSP4	0x02001400
#define PASS_PDSP5	0x02001500


//#pragma DATA_ALIGN(nav_info, 128);
//#pragma DATA_SECTION(nav_info, ".CMNDDR3")
//Nav_hibernate_save_info nav_info;

/*
 * Function prototypes */

void setModulePower(unsigned int module, unsigned int domain, unsigned int state);
void setDomainAndModulePower(unsigned int module, unsigned int domain, unsigned int state);

void getDescCount();
Int setUp (void);
Int test(void);

/*
 * Default test configuration for the silicon
 *
 * To run test at the CCS simulator
 *    cpswSimTest = 0
 *    cpswLpbkMode = CPSW_LOOPBACK_PA
 */




#ifdef SIMULATOR_SUPPORT
Int cpswLpbkMode = CPSW_LOOPBACK_PA;
Int cpswSimTest = 1;
#else 
Int cpswLpbkMode = CPSW_LOOPBACK_PA;
Int cpswSimTest = 0;
#endif

/***************************************************************************************
 * FUNCTION PURPOSE: Power up PA subsystem
 ***************************************************************************************
 * DESCRIPTION: this function powers up the PA subsystem domains
 ***************************************************************************************/
void passPowerUp (void)
{

    /* PASS power domain is turned OFF by default. It needs to be turned on before doing any 
     * PASS device register access. This not required for the simulator. */

    /* Set PASS Power domain to ON */        
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_PASS);

    /* Enable the clocks for PASS modules */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_PKTPROC, PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_CPGMAC,  PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_Crypto,  PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_PASS);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_PASS));


}



//volatile unsigned int* ptstat = (unsigned int*) 0x02350a1c;
void passPowerDown(void)
{

	volatile unsigned int *passPDSP;
	/* Disable the 6 PDSPs in the PASS */
    //this should be moved to a PA specific save function.
	passPDSP = (unsigned int*)PASS_PDSP0;
	*passPDSP &= 0xFFFFFFFD;

	passPDSP = (unsigned int*)PASS_PDSP1;
	*passPDSP &= 0xFFFFFFFD;

	passPDSP = (unsigned int*)PASS_PDSP2;
	*passPDSP &= 0xFFFFFFFD;

	passPDSP = (unsigned int*)PASS_PDSP3;
	*passPDSP &= 0xFFFFFFFD;

	passPDSP = (unsigned int*)PASS_PDSP4;
	*passPDSP &= 0xFFFFFFFD;

	passPDSP = (unsigned int*)PASS_PDSP5;
	*passPDSP &= 0xFFFFFFFD;

	// Turning off the CPGMAC, Crypto, and PKTPROC modules and domain
		setModulePower(CSL_PSC_LPSC_CPGMAC, CSL_PSC_PD_PASS, 0);										//CPGMAC PD:2 PM:8
		setModulePower(CSL_PSC_LPSC_Crypto, CSL_PSC_PD_PASS, 0);										//Crypto PD:2 PM:9
		setDomainAndModulePower(CSL_PSC_LPSC_PKTPROC, CSL_PSC_PD_PASS, 0);



}



/* NETCP Initialization routine 
1. Power UP NETCP
2. SetUp the QMSS and CPPI
3. Initialize the Data Structures which save the cmds
4. Initialize CPSW
5. SETUP PA subsystem
*/

Int Netcp_SetUp (void)
{
    Uint8  isAlloc;
    /* Power up PA sub-systems */
    if (!cpswSimTest) {
        passPowerUp();
    }

    if (setupQmCdma ()) {
        System_printf ("Function setupQmCdma failed\n");
        return -1;
    }

    gHiberQ = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, QMSS_PARAM_NOT_SPECIFIED, &isAlloc);
    if (gHiberQ < 0) {
        //printf ("HiberQ failed");
        return (-1);
    }

    if (Init_Cpsw()) {
        System_printf ("Function Init_Cpsw failed\n");
        tearDownQmCdma ();
        return -1;
    }

    if (setupPa ()) {
        System_printf ("Function setupPa failed\n");
        tearDownQmCdma ();
        return -1;
    }

    return 0;
}

Int Netcp_Test(void)
{
	  if (sendPacket ())  {
	    System_printf ("\r\nNetCP sendPacket FAILED");
	    tearDownQmCdma ();
	    return -1;
	  }

	  /* Look for the return packet */
	  if (findPacket ())  {
	  	getPaStats ();
	    System_printf ("\r\nNetCP findPacket FAILED");
	    return -1;
	  }

//	  if (getPaStats ())  {
//	  	System_printf ("Function getPaStats failed\n");
//	  	return -1;
//	  }

      System_printf("\r\nNetCP Test PASSED");

	  return 0;
}


/* Debug function. Get the get descriptor count */

void getDescCount()
{


	printf( " gHiberQ has %d\n",Qmss_getQueueEntryCount (gHiberQ));
	printf( " gvRxPktQ has %d\n",Qmss_getQueueEntryCount (gvRxPktQ[0]));
	printf( " gvPaRespQ has %d\n",Qmss_getQueueEntryCount (gvPaRespQ));
	printf( " gvPaTxRecycleQ has %d\n",Qmss_getQueueEntryCount (gvPaTxRecycleQ));
	printf( " gvRxBufQ has %d\n",Qmss_getQueueEntryCount (gvRxBufQ));
	printf( " gvTxQ[0] has %d\n",Qmss_getQueueEntryCount (gvTxQ[0]));
	printf( " gvDescQ has %d\n",Qmss_getQueueEntryCount (gvDescQ));



	return;

}
