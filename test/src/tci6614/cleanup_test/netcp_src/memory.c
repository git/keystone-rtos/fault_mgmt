/*
 *
 * Copyright (C) 2010-2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/* memory.c 
 * global memory declaration */

#include "memory.h"

/* PASS CDMA handle */
Cppi_Handle gvCppiHandle;  


/* PASS CDMA rx/tx channel handles */
Cppi_ChHnd  gvTxChHnd[10];
Cppi_ChHnd  gvRxChHnd[24];


/* Queue handles */
Int32  gvDescQ;                       /* Queue holding unassigned descriptors */
Int32  gvTxQ[10];                     /* 10 PA hard assigned transmit queues */
Int32  gvRxPktQ[CONFIG_N_RX_QS];      /* Queues to receive data packets */
Int32  gvPaRespQ;                     /* Queue to received PA command responses */
Int32  gvPaTxRecycleQ;                /* Return queue for transmit descriptors/buffers */
Int32  gvRxBufQ;                      /* Queue of buffers for receive packets */

/* Flow handle */
Cppi_FlowHnd gvFlowHnd;

/* Memory used for the linking RAM and descriptor RAM */
//#pragma DATA_ALIGN(memLinkRam, 16)
//Uint64 memLinkRam[CONFIG_NUM_DESC];

#pragma DATA_ALIGN(memDescRam, 16)
Uint8 memDescRam[CONFIG_NUM_DESC * CONFIG_SIZE_DESC];

#pragma DATA_ALIGN(memRxBuffer, 16)
Uint8 memRxBuffer[CONFIG_N_RX_BUFFERS][CONFIG_RX_BUFFER_SIZE];

/* PA instance */
#pragma DATA_ALIGN(paInstBuf, CACHE_LINESZ)
Uint8 paInstBuf[SYS_ROUND_UP(CONFIG_BUFSIZE_PA_INST, CACHE_LINESZ)];
Pa_Handle paInst;

/* Memory used for PA handles */
#pragma DATA_ALIGN(memL2Ram, CACHE_LINESZ)
#pragma DATA_ALIGN(memL3Ram, CACHE_LINESZ)
Uint8 memL2Ram[SYS_ROUND_UP(CONFIG_BUFSIZE_L2_TABLE, CACHE_LINESZ)];
Uint8 memL3Ram[SYS_ROUND_UP(CONFIG_BUFSIZE_L3_TABLE, CACHE_LINESZ)];

paHandleL2L3_t gvPaL2Handles[CONFIG_MAX_L2_HANDLES];
paHandleL2L3_t gvPaL3Handles[CONFIG_MAX_L3_HANDLES];
paHandleL4_t   gvPaL4Handles[CONFIG_MAX_L4_HANDLES];

