/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef _CONFIG_H
#define _CONFIG_H

#include <stdio.h>
#include <stdint.h>

/* Build time test configuration */

#define CONFIG_NUM_DESC              64      /* Number of descriptors created in descriptor ram */
#define CONFIG_SIZE_DESC             80      /* Host descriptor with room for control data */
#define CONFIG_INIT_DESC_QUEUE       2900    /* Queue of free descriptors */
#define CONFIG_TEARDOWN_QUE_MGR      0
#define CONFIG_TEARDOWN_QUE_NUM      2899

#define CONFIG_N_RX_QS               1
#define CONFIG_FIRST_RX_PKT_Q        2904

#define CONFIG_PKT_PA_RX_BUFFER_Q    2901     /* Queue to hold buffers for receive packets */
#define CONFIG_PKT_PA_CFG_RX_Q       2902     /* Queue for PA generated replies to configuration commands */
#define CONFIG_PKT_PA_TX_RECYCLE_Q   2903     /* Queue to receive descriptors after packets sent to PA */


#define CONFIG_N_RX_BUFFERS          20                   /* Number of Rx buffers for received packets */
#define CONFIG_RX_BUFFER_SIZE       (((300+15)/16)*16)   /* Big enough for largest PA command reply   */

#define CONFIG_MAX_L2_HANDLES        10
#define CONFIG_MAX_L3_HANDLES        20
#define CONFIG_MAX_L4_HANDLES        40

#define CONFIG_BUFSIZE_PA_INST      256
#define CONFIG_BUFSIZE_L2_TABLE     1000
#define CONFIG_BUFSIZE_L3_TABLE     4000

#define CONFIG_PA_CMD_REPLY_ID      0x11111111
#define CONFIG_RCV_PKT_ID           0xaaaaaaaa

/* Packet routing MAC information */
#define CONFIG_N_ADDMACS            2

/* CONFIG_ADDMAC0_ETH_INFO is a paEthInfo_t structure */
#define CONFIG_ADDMAC0_ETH_INFO    {   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },     /* Src mac = dont care */   \
                                       { 0x10, 0x11, 0x12, 0x13, 0x14, 0x15 },     /* Dest mac */              \
                                       0,                                          /* vlan = dont care */      \
                                       0x0800,                                     /* ether type = IPv4 */     \
                                       0,                                          /* MPLS tag = don't care */ \
                                       0}                                          /* Input EMAC port = dont care */

#define CONFIG_ADDMAC1_ETH_INFO    {   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },     /* Src mac = dont care */   \
                                       { 0x10, 0x10, 0x10, 0x10, 0x10, 0x15 },     /* Dest mac */              \
                                       0,                                          /* vlan = dont care */      \
                                       0x0800,                                     /* ether type = IPv4 */     \
                                       0,                                          /* MPLS tag = don't care */ \
                                       0}

#define HELLO    
                                   
/* CONFIG_ADDMAC0_ROUTE is a paRouteInfo_t structure */
#define CONFIG_ADDMAC0_ROUTE   {       pa_DEST_CONTINUE_PARSE_LUT1,       /* Continue parsing */             \
                                       0,                                 /* Flow Id = dont care */          \
                                       0,                                 /* queue = dont care */            \
                                       0,                                 /* multi route = dont care */      \
                                       0,                                 /* swinfo0 = dont care */          \
                                       0,                                 /* swinfo1 = dont care */          \
                                       0,                                 /* customType = pa_CUSTOM_TYPE_NONE */         \
                                       0,                                 /* customIndex: not used */        \
                                       0,                                 /* pkyType: for SRIO only */       \
                                       NULL}                              /* No commands */

/* CONFIG_ADDMAC0_NFAIL is a paRouteInfo_t structure */                              
#define CONFIG_ADDMAC0_NFAIL   {       pa_DEST_DISCARD,                   /* Toss the packet  */             \
	                                   0,                                 /* Flow Id = dont care */          \
                                       0,                                 /* queue = dont care */            \
                                       0,                                 /* mutli route = dont care */      \
                                       0,                                 /* swinfo0 = dont care */          \
                                       0,                                 /* swinfo1 = dont care */          \
                                       0,                                 /* customType: not used */         \
                                       0,                                 /* customIndex: not used */        \
                                       0,                                 /* pkyType: for SRIO only */       \
                                       NULL}                              /* No commands */
                                                                              
                                       
/* The array of all mac configurations used in the code */
#define CONFIG_ADDMAC_ETH_INFO          { CONFIG_ADDMAC0_ETH_INFO , CONFIG_ADDMAC1_ETH_INFO }
#define CONFIG_ADDMAC_ROUTE             { CONFIG_ADDMAC0_ROUTE , CONFIG_ADDMAC0_ROUTE}
#define CONFIG_ADDMAC_NFAIL             { CONFIG_ADDMAC0_NFAIL , CONFIG_ADDMAC0_NFAIL }

/* Common command reply routing information (used for all PA commands) */
#define CONFIG_PACOM_REPLY    {        pa_DEST_HOST,                      /* Replies go to the host */              \
                                       0,                                 /* User chosen ID to go to swinfo0 */     \
                                       CONFIG_PKT_PA_CFG_RX_Q,            /* Destination queue */                   \
                                       0  }                               /* Flow ID */
                                       
                                       
/* Packet routing IP information */
#define CONFIG_N_ADDIPS             2

/* CONFIG_ADDIP0_IP_INFO is a paIpInfo_t structure */
#define CONFIG_ADDIP0_IP_INFO  {  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },   /* IP source = dont care */   \
                                  { 1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },   /* IP dest */                 \
                                  0,         /* SPI = dont care */                                                    \
                                  0,         /* flow = dont care */                                                   \
                                  pa_IPV4,   /* IP type */                                                            \
                                  0,         /* GRE protocol */                                                       \
                                  0,         /* Ip protocol = dont care (TCP or UDP or anything else) */              \
                                  0,         /* TOS */                                                                \
                                  FALSE,     /* TOS = dont care (seperate field since TOS=0 is valid */               \
                                  0}         /* SCTP port = dont care                                */          


#define CONFIG_ADDIP1_IP_INFO  {  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },   /* IP source = dont care */   \
                                  { 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },   /* IP dest */                 \
                                  0,         /* SPI = dont care */                                                    \
                                  0,         /* flow = dont care */                                                   \
                                  pa_IPV4,   /* IP type */                                                            \
                                  0,         /* GRE protocol */                                                       \
                                  0,         /* Ip protocol = dont care (TCP or UDP or anything else) */              \
                                  0,         /* TOS */                                                                \
                                  FALSE,     /* TOS = dont care (seperate field since TOS=0 is valid */               \
                                  0}         /* SCTP port = dont care                                */



/* CONFIG_ADDIP0_MAC_HANDLE_IDX is the index in the MAC handle table, if any, to associate with this IP */                                       
#define CONFIG_ADDIP0_MAC_HANDLE_IDX    0     /* Link this IP with the first MAC address created */
#define CONFIG_ADDIP1_MAC_HANDLE_IDX    1     /* Link this IP with the first MAC address created */

/* CONFIG_ADDIP0_ROUTE is a paRouteInfo_t structure */
#define CONFIG_ADDIP0_ROUTE    {       pa_DEST_CONTINUE_PARSE_LUT2,       /* Continue parsing */          \
                                       0,                                 /* Flow Id = dont care */       \
                                       0,                                 /* queue = dont care */         \
                                       0,                                 /* multi route = dont care */   \
                                       0,                                 /* swinfo0 = dont care */       \
                                       0,                                 /* swinfo1 = dont care */       \
                                       0,                                 /* customType = pa_CUSTOM_TYPE_NONE */         \
                                       0,                                 /* customIndex: not used */     \
                                       0,                                 /* pkyType: for SRIO only */    \
                                       NULL}                              /* No commands */
                              

/* CONFIG_ADDIP0_NFAIL is a paRouteInfo_t structure */                              
#define CONFIG_ADDIP0_NFAIL   {        pa_DEST_DISCARD,                   /* Toss the packet  */           \
	                                   0,                                 /* Flow Id = dont care */        \
                                       0,                                 /* queue = dont care */          \
                                       0,                                 /* mutli route = dont care */    \
                                       0,                                 /* swinfo0 = dont care */        \
                                       0,                                 /* swinfo1 = dont care */        \
                                       0,                                 /* customType: not used  */      \
                                       0,                                 /* customIndex: not used */      \
                                       0,                                 /* pkyType: for SRIO only */     \
                                       NULL}                              /* No commands */
                                       


/* The array of all IP configurations used in the code */
#define CONFIG_ADDIP_IP_INFO           { CONFIG_ADDIP0_IP_INFO , CONFIG_ADDIP1_IP_INFO}
#define CONFIG_ADDIP_MAC_HANDLE_IDX    { CONFIG_ADDIP0_MAC_HANDLE_IDX , CONFIG_ADDIP1_MAC_HANDLE_IDX }
#define CONFIG_ADDIP_ROUTE             { CONFIG_ADDIP0_ROUTE , CONFIG_ADDIP0_ROUTE }
#define CONFIG_ADDIP_NFAIL             { CONFIG_ADDIP0_NFAIL, CONFIG_ADDIP0_NFAIL }


/* Port configuration */
#define CONFIG_N_ADDPORTS       4

#define CONFIG_ADDPORT0_PORT    0x0555

#define CONFIG_ADDPORT1_PORT    0x0777

#define CONFIG_ADDPORT2_PORT    0x0444

/* CONFIG_ADDPORT0_IP_HANDLE_IDX is the index in the IP handle table, if any, to associate with this port */                                       
#define CONFIG_ADDPORT0_IP_HANDLE_IDX    0     /* Link this IP with the first MAC address created */

#define CONFIG_ADDPORT0_ROUTE  {  pa_DEST_HOST,            /* Route a match to the host */   \
                                  0,                       /* Flow ID 0 */                   \
                                  CONFIG_FIRST_RX_PKT_Q,   /* Destination queue */           \
                                  -1,                      /* Multi route disabled */        \
                                  CONFIG_RCV_PKT_ID,       /* SwInfo 0 */                    \
                                  0,                        /* swinfo1 = dont care */        \
                                  0,                        /* customType = not used */      \
                                  0,                        /* customIndex: not used */      \
                                  0,                        /* pkyType: for SRIO only */     \
                                  NULL}                     /* No commands */
                                  
#define CONFIG_ADDPORT_PORTS          { CONFIG_ADDPORT0_PORT , CONFIG_ADDPORT1_PORT ,CONFIG_ADDPORT0_PORT , CONFIG_ADDPORT2_PORT}
#define CONFIG_ADDPORT_IP_HANDLE_IDX  { CONFIG_ADDPORT0_IP_HANDLE_IDX , CONFIG_ADDPORT0_IP_HANDLE_IDX, CONFIG_ADDPORT0_IP_HANDLE_IDX , CONFIG_ADDPORT0_IP_HANDLE_IDX}
#define CONFIG_ADDPORT_ROUTE          { CONFIG_ADDPORT0_ROUTE , CONFIG_ADDPORT0_ROUTE, CONFIG_ADDPORT0_ROUTE , CONFIG_ADDPORT0_ROUTE}

/* Define LoopBack modes */  
#define CPSW_LOOPBACK_NONE           0
#define CPSW_LOOPBACK_INTERNAL       1
#define CPSW_LOOPBACK_EXTERNAL       2
#define CPSW_LOOPBACK_PA             3

extern int cpswLpbkMode;
extern int cpswSimTest;


#endif /* _CONFIG_H */

