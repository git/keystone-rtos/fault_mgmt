#ifndef MNAV_DESCRIPTORS_H
#define MNAV_DESCRIPTORS_H
/*
 *  Copyright 2009 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  Note - for fields that are memory pointers, normal non-pointer
 *         variables are used.  This is because they must be passed
 *         as values to memtr.
 */

#define MNAV_DESC_TYPE_HOST      0
#define MNAV_DESC_TYPE_MONO      2
#define MNAV_DESC_TYPE_DEFAULT   3 /* for stream use, not flow config */


/*******************************************************************/
/* Define the bit and word layouts for the Host Packet Descriptor. */
/* For a Host Packet, this is used for the first descriptor only.  */
/*******************************************************************/
#ifdef _BIG_ENDIAN
typedef struct
{
  /* word 0 */
  uint32_t type_id         : 2;  //always 0x0 (Host Packet ID)
  uint32_t packet_type     : 5;
  uint32_t reserved_w0     : 2;
  uint32_t ps_reg_loc      : 1;  //0=PS words in desc, 1=PS words in SOP buff
  uint32_t packet_length   : 22; //in bytes (4M - 1 max)

  /* word 1 */
  uint32_t src_tag_hi      : 8;
  uint32_t src_tag_lo      : 8;
  uint32_t dest_tag_hi     : 8;
  uint32_t dest_tag_lo     : 8;

  /* word 2 */
  uint32_t epib            : 1;  //1=extended packet info block is present
  uint32_t reserved_w2     : 1;
  uint32_t psv_word_count  : 6;  //number of 32-bit PS data words
  uint32_t err_flags       : 4;
  uint32_t ps_flags        : 4;
  uint32_t return_policy   : 1;  //0=linked packet goes to pkt_return_qnum,
                                 //1=each descriptor goes to pkt_return_qnum
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t pkt_return_qmgr : 2;
  uint32_t pkt_return_qnum : 12;

  /* word 3 */
  uint32_t reserved_w3     : 10;
  uint32_t buffer_len      : 22;

  /* word 4 */
  uint32_t buffer_ptr;

  /* word 5 */
  uint32_t next_desc_ptr;

  /* word 6 */
  uint32_t orig_buff0_pool : 4;
  uint32_t orig_buff0_refc : 6;
  uint32_t orig_buff0_len  : 22;

  /* word 7 */
  uint32_t orig_buff0_ptr;

} MNAV_HostPacketDescriptor;
#else
typedef struct
{
  /* word 0 */
  uint32_t packet_length   : 22; //in bytes (4M - 1 max)
  uint32_t ps_reg_loc      : 1;  //0=PS words in desc, 1=PS words in SOP buff
  uint32_t reserved_w0     : 2;
  uint32_t packet_type     : 5;
  uint32_t type_id         : 2;  //always 0x0 (Host Packet ID)

  /* word 1 */
  uint32_t dest_tag_lo     : 8;
  uint32_t dest_tag_hi     : 8;
  uint32_t src_tag_lo      : 8;
  uint32_t src_tag_hi      : 8;

  /* word 2 */
  uint32_t pkt_return_qnum : 12;
  uint32_t pkt_return_qmgr : 2;
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t return_policy   : 1;  //0=linked packet goes to pkt_return_qnum,
                                 //1=each descriptor goes to pkt_return_qnum
  uint32_t ps_flags        : 4;
  uint32_t err_flags       : 4;
  uint32_t psv_word_count  : 6;  //number of 32-bit PS data words
  uint32_t reserved_w2     : 1;
  uint32_t epib            : 1;  //1=extended packet info block is present


  /* word 3 */
  uint32_t buffer_len      : 22;
  uint32_t reserved_w3     : 10;

  /* word 4 */
  uint32_t buffer_ptr;

  /* word 5 */
  uint32_t next_desc_ptr;

  /* word 6 */
  uint32_t orig_buff0_len  : 22;
  uint32_t orig_buff0_refc : 6;
  uint32_t orig_buff0_pool : 4;

  /* word 7 */
  uint32_t orig_buff0_ptr;

} MNAV_HostPacketDescriptor;
#endif

#define MNAV_HOST_PACKET_SIZE  sizeof(MNAV_HostPacketDescriptor)


/*******************************************************************/
/* Define the bit and word layouts for the Host Buffer Descriptor. */
/* For a Host Packet, this will used for secondary descriptors.    */
/*******************************************************************/
#ifdef _BIG_ENDIAN
typedef struct
{
  /* word 0 */
  uint32_t reserved_w0;
  /* word 1 */
  uint32_t reserved_w1;

  /* word 2 */
  uint32_t reserved_w2     : 17;
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t pkt_return_qmgr : 2;
  uint32_t pkt_return_qnum : 12;

  /* word 3 */
  uint32_t reserved_w3     : 10;
  uint32_t buffer_len      : 22;

  /* word 4 */
  uint32_t buffer_ptr;

  /* word 5 */
  uint32_t next_desc_ptr;

  /* word 6 */
  uint32_t orig_buff0_pool : 4;
  uint32_t orig_buff0_refc : 6;
  uint32_t orig_buff0_len  : 22;

  /* word 7 */
  uint32_t orig_buff0_ptr;

} MNAV_HostBufferDescriptor;
#else
typedef struct
{
  /* word 0 */
  uint32_t reserved_w0;
  /* word 1 */
  uint32_t reserved_w1;

  /* word 2 */
  uint32_t pkt_return_qnum : 12;
  uint32_t pkt_return_qmgr : 2;
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t reserved_w2     : 17;

  /* word 3 */
  uint32_t buffer_len      : 22;
  uint32_t reserved_w3     : 10;

  /* word 4 */
  uint32_t buffer_ptr;

  /* word 5 */
  uint32_t next_desc_ptr;

  /* word 6 */
  uint32_t orig_buff0_len  : 22;
  uint32_t orig_buff0_refc : 6;
  uint32_t orig_buff0_pool : 4;

  /* word 7 */
  uint32_t orig_buff0_ptr;

} MNAV_HostBufferDescriptor;
#endif

// Host Buffer packet size is always the same as Host Packet size


/*********************************************************************/
/* Define the bit and word layouts for the Monolithic Pkt Descriptor.*/
/*********************************************************************/
#ifdef _BIG_ENDIAN
typedef struct
{
  /* word 0 */
  uint32_t type_id         : 2;  //always 0x2 (Monolithic Packet ID)
  uint32_t packet_type     : 5;
  uint32_t data_offset     : 9;
  uint32_t packet_length   : 16; //in bytes (65535 max)

  /* word 1 */
  uint32_t src_tag_hi      : 8;
  uint32_t src_tag_lo      : 8;
  uint32_t dest_tag_hi     : 8;
  uint32_t dest_tag_lo     : 8;

  /* word 2 */
  uint32_t epib            : 1;  //1=extended packet info block is present
  uint32_t reserved_w2     : 1;
  uint32_t psv_word_count  : 6;  //number of 32-bit PS data words
  uint32_t err_flags       : 4;
  uint32_t ps_flags        : 4;
  uint32_t reserved_w2b    : 1;
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t pkt_return_qmgr : 2;
  uint32_t pkt_return_qnum : 12;

} MNAV_MonolithicPacketDescriptor;
#else
typedef struct
{
  /* word 0 */
  uint32_t packet_length   : 16; //in bytes (65535 max)
  uint32_t data_offset     : 9;
  uint32_t packet_type     : 5;
  uint32_t type_id         : 2;  //always 0x2 (Monolithic Packet ID)

  /* word 1 */
  uint32_t dest_tag_lo     : 8;
  uint32_t dest_tag_hi     : 8;
  uint32_t src_tag_lo      : 8;
  uint32_t src_tag_hi      : 8;

  /* word 2 */
  uint32_t pkt_return_qnum : 12;
  uint32_t pkt_return_qmgr : 2;
  uint32_t ret_push_policy : 1;  //0=return to queue tail, 1=queue head
  uint32_t reserved_w2b    : 1;
  uint32_t ps_flags        : 4;
  uint32_t err_flags       : 4;
  uint32_t psv_word_count  : 6;  //number of 32-bit PS data words
  uint32_t reserved_w2     : 1;
  uint32_t epib            : 1;  //1=extended packet info block is present

} MNAV_MonolithicPacketDescriptor;
#endif

#define MNAV_MONO_PACKET_SIZE  sizeof(MNAV_MonolithicPacketDescriptor)


/*********************************************************************/
/* Define the word layout of the Extended Packet Info Block.  It     */
/* is optional and may follow Host Packet and Monolithic descriptors.*/
/* For Monolithic descriptors, there is an extra NULL word. This null*/
/* is not copied or passed through the Streaming I/F.                */
/*********************************************************************/
typedef struct
{
  /* word 0 */
  uint32_t timestamp;

  /* word 1 */
  uint32_t sw_info0;

  /* word 2 */
  uint32_t sw_info1;

  /* word 3 */
  uint32_t sw_info2;

} MNAV_ExtendedPacketInfoBlock;

#endif /* MNAV_DESCRIPTORS_H */
