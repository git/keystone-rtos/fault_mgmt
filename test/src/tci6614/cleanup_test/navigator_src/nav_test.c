/**
 *   @file  nav_test.c
 *
 *   @brief   
 *      This file creates a test to demonstrate the capability to save the
 *      state of Multicore Navigator prior to hibernation and restore it to
 *      the same state when brought back from hibernation. This test is 
 *      based on a few of the QMSS LLD example tests, so that Acc and QoS
 *      run simultaneously. It has also been fitted with logging calls to
 *      save the state of non-readable registers and memories during runtime.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2012-2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/
#include <stdio.h>
#include <stdint.h>

#include <xdc/runtime/System.h>

/* LLD includes */
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/qmss/qmss_firmware.h>
#include <ti/drv/rm/rm.h>

/* CSL includes */ 
#include <ti/csl/csl_chip.h>

#include "nav_test.h"
#include "mnav_descriptors.h"

/************************ USER DEFINES ********************/
#define NAV_NUM_MONO_DESC           4096
#define NAV_DESC_SIZE               32
#define NAV_PROFILE_DESCS           32
#define NAV_TX_QUEUES               8
#define NAV_TOTAL_SENT             (NAV_PROFILE_DESCS*NAV_TX_QUEUES)
#define NAV_ACC_LIST_SIZE           32
#define NAV_ACC_TIMER_VAL           3500
#define NAV_QOS_TIMER_VAL           491
#define NAV_FDQ                     990
#define NAV_QMSS_TXQ                812 /* 8 queues */
#define NAV_QOS_QUE_BASE            992 /* 64 queues, aligned to mult of 32 */
#define NAV_QOS_IN_QUE             (992+56) /* the last 8 queues from base */
#define NAV_QOS_OUT_QUE             1056
#define NAV_DEAD_XFR_QUE            3840

//Undefine this to exclude the Accumulator from the test
#define ACC_TEST 1

/************************ GLOBAL VARIABLES ********************/

/* QMSS memory regions must be defined in ascending address order. */
#pragma DATA_ALIGN (navMonoDesc, 16)
//pragma DATA_SECTION(navMonoDesc, ".navRegMem");
Uint8                   navMonoDesc[NAV_DESC_SIZE * NAV_NUM_MONO_DESC];

#pragma DATA_ALIGN (navAccList, 16)
//#pragma DATA_SECTION(navAccList, ".navRegMem");
Uint32                  navAccList[(NAV_ACC_LIST_SIZE + 1) * 2];

//#pragma DATA_ALIGN (myCppiHeap, 256)
//#pragma DATA_SECTION(myCppiHeap, ".navRegMem");
Uint8                   myCppiHeap[32000];

/* QMSS configuration */
//#pragma DATA_SECTION(qmssInitConfig, ".navRegMem");
Qmss_InitCfg            qmssInitConfig;
/* Memory region configuration information */
//#pragma DATA_SECTION(memInfo, ".navRegMem");
Qmss_MemRegInfo         memInfo;
/* Memory region configuration status */
//#pragma DATA_SECTION(memRegStatus, ".navRegMem");
Qmss_MemRegCfg          memRegStatus;
/* QM descriptor configuration */
//#pragma DATA_SECTION(descCfg, ".navRegMem");
Qmss_DescCfg            descCfg;
/* Store the queue handle for destination queues on which allocated descriptors are stored */
//#pragma DATA_SECTION(QueHnd, ".navRegMem");
Qmss_QueueHnd           QueHnd[QMSS_MAX_MEM_REGIONS];

/* CPDMA configuration */
//#pragma DATA_SECTION(cpdmaCfg, ".navRegMem");
Cppi_CpDmaInitCfg       cpdmaCfg;
/* Tx channel configuration */
//#pragma DATA_SECTION(txChCfg, ".navRegMem");
Cppi_TxChInitCfg        txChCfg;
/* Rx channel configuration */
//#pragma DATA_SECTION(rxChCfg, ".navRegMem");
Cppi_RxChInitCfg        rxChCfg;
/* Rx flow configuration */
//#pragma DATA_SECTION(rxFlowCfg, ".navRegMem");
Cppi_RxFlowCfg          rxFlowCfg;

//#pragma DATA_SECTION(navFdqHnd, ".navRegMem");
int32_t                 navFdqHnd;
//#pragma DATA_SECTION(navDeadHnd, ".navRegMem");
int32_t                 navDeadHnd;
//#pragma DATA_SECTION(navTxQHnd, ".navRegMem");
int32_t                 navTxQHnd[8];
//#pragma DATA_SECTION(navQosInHnd, ".navRegMem");
int32_t                 navQosInHnd[64];
//#pragma DATA_SECTION(navQosOutQHnd, ".navRegMem");
int32_t                 navQosOutQHnd;

#if RM
Rm_Handle rm_handle;
#endif


/************************ EXTERN VARIABLES ********************/
/* QMSS device specific configuration */
extern Qmss_GlobalConfigParams  qmssGblCfgParams;
/* CPPI device specific configuration */
extern Cppi_GlobalConfigParams  cppiGblCfgParams;
#if RM
/* RM Resource table */
extern const Rm_Resource rmResourceTable;
#endif

/**
 *  @b Description
 *  @n
 *      Utility function which converts a local address to global.
 *
 *  @param[in]  addr
 *      Local address to be converted
 *
 *  @retval
 *      Global Address
 */
uint32_t nav_l2_global_address (uint32_t addr)
{
  uint32_t corenum;

  /* Get the core number. */
  corenum = CSL_chipReadReg(CSL_CHIP_DNUM);

  if ((addr >= 0x00800000) && (addr < 0x00F08000))
  {
    return (addr + (0x10000000 + (corenum * 0x01000000)));
  }

  return (addr);
}

/* This function performs the initial configuration of QMSS. It assumes that
 * it is the first block to request resources, so it's checking is minimal.
 * The test works like this: (in flow order)
 *   FDQ: 990, pushed to:
 *   QMSS Tx queues: 800..807, rxflow directs to:
 *   QoS Ingress queues: 1048..1055 (allocates 992..1055)
 *   QoS Egress queue: 1056, automatically processed by:
 *   Acc HiPri chan1 queue: 1056, list is recycled to:
 *   FDQ: 990
 */
void nav_test_setup(void)
{
    uint32_t                idx;
    uint32_t                temp;
    uint32_t               *ptr;
    uint8_t                 isAllocated;
#if !RUNNING_WITH_LINUX
    int32_t                 queueNum;
    Qmss_QosClusterCfg      clusterCfg;
    Qmss_QosClusterCfgRR   *clusterCfgRR;
    Qmss_QosQueueCfg        qosQueueCfg;
#endif    
    Cppi_Handle             cppiHnd;
    Cppi_InitCfg            initCfg;
    Cppi_StartCfg           cppiCfg;
    Qmss_Result             result;
    MNAV_MonolithicPacketDescriptor *mono;
#ifdef ACC_TEST
    Qmss_AccCmdCfg          accCfg;
#endif
#if RM
    Rm_Result               ret_val;

    ret_val = Rm_init(&rmResourceTable);
    if (ret_val < 0) {
        System_printf ("Rm_init failed error code : %d\n", ret_val);
    }

    rm_handle = Rm_getHandle();
    if (!rm_handle) {
        System_printf ("Rm_getHandle returned NULL\n");
    }

    qmssGblCfgParams.qmRmHandle = rm_handle;
#endif    

    memset ((void *) &qmssInitConfig, 0, sizeof (Qmss_InitCfg));

    /* Set up the linking RAM. Use internal Linking RAM.  */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0x0;
    qmssInitConfig.maxDescNum      = 0x3fff;

#ifdef _BIG_ENDIAN
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = &acc48_be;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_be);

    qmssInitConfig.pdspFirmware[1].pdspId = Qmss_PdspId_PDSP2;
    qmssInitConfig.pdspFirmware[1].firmware = &qos_be;
    qmssInitConfig.pdspFirmware[1].size = sizeof (qos_be);
#else
    qmssInitConfig.pdspFirmware[0].pdspId = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = &acc48_le;
    qmssInitConfig.pdspFirmware[0].size = sizeof (acc48_le);

    qmssInitConfig.pdspFirmware[1].pdspId = Qmss_PdspId_PDSP2;
    qmssInitConfig.pdspFirmware[1].firmware = &qos_le;
    qmssInitConfig.pdspFirmware[1].size = sizeof (qos_le);
#endif

    /* Initialize Queue Manager SubSystem; download the firmware */
    result = Qmss_init (&qmssInitConfig, &qmssGblCfgParams);
    if ((result != QMSS_SOK) && (result != QMSS_RESOURCE_USE_DENIED)) {
        System_printf("\r\nFailed to init QMSS LLD with error %d", result);
        return;
    }

    /* Start Queue Manager SubSystem */
    result = Qmss_start ();
    if (result != QMSS_SOK) {
        System_printf("\r\nFailed to start QMSS LLD");
    }

    /*************************************/
    /* Setup QMSS resources for the test */
    /*************************************/

    /* Setup memory region 0 for monolithic descriptors */
    memset ((void *) &navMonoDesc, 0, NAV_DESC_SIZE * NAV_NUM_MONO_DESC);
    memInfo.descBase = (Uint32 *) nav_l2_global_address ((Uint32) navMonoDesc);
    memInfo.descSize = NAV_DESC_SIZE;
    memInfo.descNum = NAV_NUM_MONO_DESC;
    memInfo.manageDescFlag = Qmss_ManageDesc_UNMANAGED_DESCRIPTOR;
    memInfo.memRegion = Qmss_MemRegion_MEMORY_REGION0;
    memInfo.startIndex = 0;

    result = Qmss_insertMemoryRegion (&memInfo);
    if (result < QMSS_SOK) {
        System_printf("\r\nFailed to configure memory region");
    }

    navFdqHnd = Qmss_queueOpen((Qmss_QueueType)0, NAV_FDQ, &isAllocated);

    /* Open Qmss tx queues */
    for (idx = NAV_QMSS_TXQ; idx < (NAV_QMSS_TXQ+8); idx++) {
        navTxQHnd[idx-NAV_QMSS_TXQ] = Qmss_queueOpen((Qmss_QueueType)0, idx, &isAllocated);
    }

#if !RUNNING_WITH_LINUX
    /* Allocate a range of queues to be used by QoS (the last 8) */
    for (idx = NAV_QOS_QUE_BASE; idx < (NAV_QOS_QUE_BASE+64); idx++) {
        navQosInHnd[idx-NAV_QOS_QUE_BASE] = Qmss_queueOpen((Qmss_QueueType)0, idx, &isAllocated);
    }

    /* Configure the queue thresholds as required by QoS */
    for (queueNum = 56; queueNum < 64; queueNum++) {
        Qmss_setQueueThreshold(navQosInHnd[queueNum], 1, 1);
    }    
#endif    

    /* Open QoS output queue */
    navQosOutQHnd = Qmss_queueOpen((Qmss_QueueType)0, NAV_QOS_OUT_QUE, &isAllocated);

    /* Where transferred descriptors go to die... */
    navDeadHnd = Qmss_queueOpen((Qmss_QueueType)0, NAV_DEAD_XFR_QUE, &isAllocated);

#if !RUNNING_WITH_LINUX
    /********************************/
    /* Setup QMSS resources for QoS */
    /********************************/

    /* Set the QoS FW's base queue */
    if ((result = Qmss_setQosQueueBase(NAV_QOS_QUE_BASE)) != QCMD_RETCODE_SUCCESS) {
        System_printf("\r\nFailed to set QoS queue base with err %d", result);
    }

    /* Set up QoS's timer */
    if ((result = Qmss_configureQosTimer(NAV_QOS_TIMER_VAL) != QCMD_RETCODE_SUCCESS)) {
        System_printf("\r\nFailed to configure QoS timer");
    }

    memset (&qosQueueCfg, 0, sizeof(qosQueueCfg));
    qosQueueCfg.egressQueNum = (uint16_t)navQosOutQHnd;
    qosQueueCfg.iterationCredit = 0; /* not used */
    qosQueueCfg.maxCredit = 0;
    qosQueueCfg.congestionThreshold = 0xffffffff;

    /* Configure applicable QoS queues */
    for (queueNum = 56; queueNum < 64; queueNum++) {
        if ((result = Qmss_configureQosQueue (queueNum, &qosQueueCfg) != QCMD_RETCODE_SUCCESS)) {
            System_printf("\r\nFailed to configure QoS queue");
        }
    }

    /* Configure the QoS cluster to use for the Round-Robin traffic shaping. */
    memset (&clusterCfg, 0, sizeof(clusterCfg));
    clusterCfgRR                       = &clusterCfg.u.cfgRR;
    clusterCfg.mode                    = Qmss_QosMode_RoundRobin;
    clusterCfgRR->maxGlobalCredit      = 5000;
    clusterCfgRR->qosQueHighCnt        = 4;  /* Number of high prio queues, must be set to 4. */
    clusterCfgRR->qosQueNumHigh[0]     = 56; /* Offset to high queue 0, must be set to 56. */
    clusterCfgRR->qosQueNumHigh[1]     = 57; /* Offset to high queue 1, must be set to 57. */
    clusterCfgRR->qosQueNumHigh[2]     = 58; /* Offset to high queue 2, must be set to 58. */
    clusterCfgRR->qosQueNumHigh[3]     = 59; /* Offset to high queue 3, must be set to 59. */
    clusterCfgRR->qosQueLowCnt         = 4;  /* Number of low prio queues, must be set to 4. */
    clusterCfgRR->qosQueNumLow[0]      = 60; /* Offset to low queue 0, must be set to 60. */
    clusterCfgRR->qosQueNumLow[1]      = 61; /* Offset to low queue 1, must be set to 61. */
    clusterCfgRR->qosQueNumLow[2]      = 62; /* Offset to low queue 2, must be set to 62. */
    clusterCfgRR->qosQueNumLow[3]      = 63; /* Offset to low queue 3, must be set to 63. */
    clusterCfgRR->sizeAdjust           = 24; /* Recommended by TI in "Ericsson QoS Transmit Discussion Addendum". */
    clusterCfgRR->egressQueCnt         = 1;
    clusterCfgRR->egressQueNum[0].qMgr = NAV_QOS_OUT_QUE >> 12;
    clusterCfgRR->egressQueNum[0].qNum = NAV_QOS_OUT_QUE & 0x0fff;
    clusterCfgRR->iterationCredit      = 252;
    clusterCfgRR->maxEgressBacklog     = 1000000;
    clusterCfgRR->queueDisableMask     = 0x0;/* Enable all ingress queues from the start. */

    result = Qmss_configureQosCluster(7, &clusterCfg);
    if (result != QCMD_RETCODE_SUCCESS) {
        System_printf("\r\nFailed to configure QoS Cluster 7");
    }

    result = Qmss_enableQosCluster(7);
    if (result != QCMD_RETCODE_SUCCESS) {
        System_printf("\r\nFailed to enable QoS Cluster 7");
    }
#endif
    /********************************/
    /* Setup QMSS resources for Acc */
    /********************************/

#ifdef ACC_TEST
    result = Qmss_configureAccTimer(Qmss_PdspId_PDSP1, NAV_ACC_TIMER_VAL);

    /* program a high priority accumulator channel */
    memset ((void *) &navAccList, 0, sizeof(navAccList));
    accCfg.channel = 0;
    accCfg.command = Qmss_AccCmd_ENABLE_CHANNEL;
    accCfg.queueEnMask = 0;
    accCfg.listAddress = nav_l2_global_address ((Uint32) navAccList);
    accCfg.queMgrIndex = NAV_QOS_OUT_QUE;
    accCfg.maxPageEntries = NAV_ACC_LIST_SIZE + 1;
    accCfg.timerLoadCount = 0;
    accCfg.interruptPacingMode = Qmss_AccPacingMode_NONE;
    accCfg.listEntrySize = Qmss_AccEntrySize_REG_D;
    accCfg.listCountMode = Qmss_AccCountMode_ENTRY_COUNT;
    accCfg.multiQueueMode = Qmss_AccQueueMode_SINGLE_QUEUE;

    result = Qmss_programAccumulator(Qmss_PdspId_PDSP1, &accCfg);
#endif

    /***********************************/
    /* Setup resources for QMSS pktDMA */
    /***********************************/
    /* Initialize CPPI LLD */
    result = Cppi_getHeapReq(&cppiGblCfgParams, &temp);
    initCfg.heapParams.staticHeapBase = &myCppiHeap[0];
    initCfg.heapParams.staticHeapSize = temp;
    initCfg.heapParams.heapAlignPow2  = 8;
    initCfg.heapParams.dynamicHeapBlockSize = -1;

    result = Cppi_initCfg(&cppiGblCfgParams, &initCfg);
    if (result != CPPI_SOK) {
        System_printf("\r\nFailed to Init CPPI LLD");
    }

#if RM
    cppiCfg.rmHandle = rm_handle;
#else
    cppiCfg.rmHandle = NULL;
#endif
    Cppi_startCfg(&cppiCfg);

    /* Set up QMSS CPDMA configuration */
    memset ((void *) &cpdmaCfg, 0, sizeof (Cppi_CpDmaInitCfg));
    cpdmaCfg.dmaNum = Cppi_CpDma_QMSS_CPDMA;
    cpdmaCfg.qm0BaseAddress = 0x34020000;
    cpdmaCfg.qm1BaseAddress = 0x34030000;
    cpdmaCfg.timeoutCount   = 100;

    /* Open QMSS CPDMA */
    cppiHnd = (Cppi_Handle) Cppi_open(&cpdmaCfg);
    if (cppiHnd == NULL) {
        System_printf("\r\nFailed to Open CPPI LLD");
        return;
    }

    /* Setup 8 pktDMA channels */
    memset((void *) &txChCfg, 0, sizeof (Cppi_TxChInitCfg));
    memset((void *) &rxChCfg, 0, sizeof (Cppi_RxChInitCfg));
    memset((void *) &rxFlowCfg, 0, sizeof (Cppi_RxFlowCfg));

    for (idx = 0; idx < 8; idx++)
    {
        Cppi_ChHnd chHnd;
        Cppi_FlowHnd flwHnd;

        /* Set up Tx Channel parameters - Add 12 to get past channels used by Linux */
        txChCfg.channelNum = idx+12;
        txChCfg.txEnable = Cppi_ChState_CHANNEL_ENABLE;

        //Since TxChanCfgRegB reg is not changed, don't need to save it
        chHnd = Cppi_txChannelOpen(cppiHnd, &txChCfg, &isAllocated);
        if ((chHnd == 0) || (isAllocated == 0)) {
            System_printf("\r\nFailed to Open QMSS pktDMA Tx Chan %d", idx);
        }

        rxChCfg.channelNum = idx+12; /* Add 12 to get past channels used by Linux */
        rxChCfg.rxEnable = Cppi_ChState_CHANNEL_ENABLE;

        /* Open Rx Channel */
        chHnd = Cppi_rxChannelOpen(cppiHnd, &rxChCfg, &isAllocated);
        if ((chHnd == 0) || (isAllocated == 0)) {
            System_printf("\r\nFailed to Open QMSS pktDMA Rx Chan %d", idx);
        }

        rxFlowCfg.flowIdNum = idx+12; /* Add 12 to get past channels used by Linux */
#if !RUNNING_WITH_LINUX       
        rxFlowCfg.rx_dest_qnum = (NAV_QOS_IN_QUE+idx) & 0x0fff;
        rxFlowCfg.rx_dest_qmgr = (NAV_QOS_IN_QUE+idx) >> 12;
#else
        rxFlowCfg.rx_dest_qnum = NAV_QOS_OUT_QUE & 0x0fff;
        rxFlowCfg.rx_dest_qmgr = NAV_QOS_OUT_QUE >> 12;
#endif
        rxFlowCfg.rx_sop_offset = 12;
        rxFlowCfg.rx_desc_type = Cppi_DescType_MONOLITHIC; 
        rxFlowCfg.rx_src_tag_lo_sel = 2; //copy flow_id through
        rxFlowCfg.rx_fdq0_sz0_qnum = NAV_FDQ & 0x0fff;
        rxFlowCfg.rx_fdq0_sz0_qmgr = NAV_FDQ >> 12;

        /* Configure Rx flow */
        flwHnd = Cppi_configureRxFlow(cppiHnd, &rxFlowCfg, &isAllocated);
        if ((flwHnd == 0) || (isAllocated == 0)) {
            System_printf("\r\nFailed to Open QMSS pktDMA Rx Flow %d", idx);
        }
    }

    /* Initialize the nav test descriptors and push to the free Queue */
    ptr = (uint32_t *)nav_l2_global_address ((Uint32) navMonoDesc);
    mono = (MNAV_MonolithicPacketDescriptor *)ptr;
    for (idx = 0; idx < NAV_NUM_MONO_DESC; idx++) {
        mono->type_id = Cppi_DescType_MONOLITHIC;
        mono->data_offset = 12;
        mono->packet_length = 20;
        mono->pkt_return_qnum = NAV_FDQ; //Tx recycle back to FDQ
        Qmss_queuePushDesc(navFdqHnd, ptr);
        ptr += NAV_DESC_SIZE/4;
        mono = (MNAV_MonolithicPacketDescriptor *)ptr;
    }
}

#define QM_REG_INTD_INT_COUNT     0x300

/* This function reads a QMSS INTD Int Count Register.
 * Reading has no effect on the register.
 * "intnum" is:  0..31 for High Pri interrupts
 *              32..47 for Low Pri interrupts
 *              48..49 for PKTDMA Starvation interrupts
 */
Uint32 intd_read_intcount(Uint16 intnum)
{
  Uint32 *reg;
  Uint32  value;

  reg = (Uint32 *)(CSL_QM_SS_CFG_INTD_REGS + QM_REG_INTD_INT_COUNT + (intnum * 4));
  value = *reg;

  return(value);
}

/**
 *  
 */
void nav_test_run(void)
{
    Qmss_Result             result;
    Uint32                  idx, idx2;
    uint32_t                fdq_count;
    uint32_t                used_count;
    uint32_t               *desc;
#ifdef ACC_TEST
    uint32_t                temp;
    Uint32                  packetCount;
    uint32_t                pingpong;
    uint32_t               *list;
#else
    uint32_t                qos_count;
#endif
    MNAV_MonolithicPacketDescriptor *mono;

    /***** Start the test! *****/

    result = Qmss_getQueueEntryCount(navFdqHnd);

    if (result == 0) {
        System_printf("\r\nNavigator Test SKIP (no descriptors left)");
        return;
    }

    /* Send descriptors to each Tx queue now. */
    for (idx = 0; idx < NAV_PROFILE_DESCS; idx++) {
        //8 TX channels
        for (idx2 = 0; idx2 < 8; idx2++) {
            desc = (uint32_t *)QMSS_DESC_PTR(Qmss_queuePop(navFdqHnd));
            if (desc) {
                mono = (MNAV_MonolithicPacketDescriptor *)desc;
                mono->packet_length = 16;
                mono->src_tag_lo = idx2+12;
                //keep the return que number set previously.

                /* No cache operation is needed since only the same CPU will read */
                Qmss_queuePushDescSize(navTxQHnd[idx2], desc, NAV_DESC_SIZE);
            }
        }
    }

#ifdef ACC_TEST
    /* Burn some time for the Acc and QoS PDSPs to run. */
    temp = 0;
    for (idx = 0; idx < 1000; idx++) {
        temp = idx;
    }
    temp = 1;

    /* Wait for the packets to arrive. */
    pingpong = 0;
    packetCount = 0;
    while (1) {
        //see if the Accumulator channel 0 triggered
        temp = intd_read_intcount(0);
        if (temp) {
            list = &navAccList[(NAV_ACC_LIST_SIZE + 1) * pingpong];

            for (idx = 0; idx < list[0]; idx++) {
                Qmss_queuePushDesc(navDeadHnd, (void *)(list[idx+1] & 0xfffffff0));
                packetCount ++;
            }

            list[0] = 0; //clear the count
            Qmss_ackInterrupt(0, 1);
            Qmss_setEoiVector(Qmss_IntdInterruptType_HIGH, 0);
            pingpong = (pingpong + 1) & 0x01; //toggle the ping pong
        }

        if (packetCount >= (NAV_PROFILE_DESCS*8)) {
            break;
        }
    }
#else
    //wait for the packets to arrive in the Rx queues...
    while (1) {
        result = Qmss_getQueueEntryCount(NAV_QOS_OUT_QUE);

        if (result >= ((NAV_PROFILE_DESCS*8))) {
            break;
        }
    }
#endif

    fdq_count = Qmss_getQueueEntryCount(navFdqHnd);

    //8 TX channels
    for (idx = 0; idx < 8; idx++) {
        result = Qmss_getQueueEntryCount(NAV_QMSS_TXQ+idx);
        if (result > 0) {
            System_printf("\r\n  ERROR: Tx Chan %d count 0x%x", idx, result);
        }
    }

#ifndef ACC_TEST
    qos_count = Qmss_getQueueEntryCount(NAV_QOS_OUT_QUE);

    //Move the used descriptors to a dead queue so that the state changes
    desc = (uint32_t *)QMSS_DESC_PTR(Qmss_queuePop(NAV_QOS_OUT_QUE));
    while (desc != 0) {
        Qmss_queuePushDesc(navDeadHnd, desc);
        desc = (uint32_t *)QMSS_DESC_PTR(Qmss_queuePop(NAV_QOS_OUT_QUE));
    }
#endif

    used_count = Qmss_getQueueEntryCount(navDeadHnd);

#ifdef ACC_TEST
    if ((fdq_count+used_count) == NAV_NUM_MONO_DESC) {
#else
    if ((qos_count == NAV_TOTAL_SENT) &&
        ((fdq_count+used_count) == NAV_NUM_MONO_DESC)) {
#endif
        System_printf("\r\nNavigator Test PASSED:");
    }
    else {
        System_printf("\r\nNavigator Test FAILED:");
    }

    System_printf("\r\n  Input FDQ Count 0x%x", fdq_count);
    System_printf("\r\n  Used  FDQ Count 0x%x", used_count);
}
