/**
 *   @file  fm_coredump_osal.c
 *
 *   @brief   
 *      This is the OS abstraction layer 
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2012-2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Standard Includes */
#include <stdarg.h>

/* XDC Includes */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/c64p/Hwi.h>

/* CSL Include Files */
#include <ti/csl/csl_cacheAux.h>

/**********************************************************************
 ************************** Local Definitions *************************
 **********************************************************************/

/* Define this option during DEBUG and it can be turned off during final release
 * as it can help debug cache misaligned buffers */
#undef CACHE_ALIGNMENT_CHECK


/**********************************************************************
 *************************** OSAL Functions ***************************
 **********************************************************************/

/**
 *  @b Description
 *  @n
 *      The function is used to indicate that the block of memory has
 *      finished being accessed. If the memory block is cached then the
 *      application would need to ensure that the contents of the cache
 *      are updated immediately to the actual memory.
 *
 *  @param[in]  ptr
 *       Address of memory block
 *  @param[in]  size
 *       Size of memory block
 *  @param[in]  checkAlign
 *       Flags that enables checks for address cache line alignment
 *
 *  @retval
 *      Not Applicable
 */
static void Osal_endMemAccess (void *ptr, uint32_t size, uint32_t checkAlign)
{
	UInt  key;

#ifdef CACHE_ALIGNMENT_CHECK
    /* Validate the address only if required to do so. */
    if (checkAlign == 1)
    {
		/* Check if DDR3 address is 128 byte aligned */
		if ((((int)ptr & 0x80000000) == 0x80000000) && (((int)ptr % 0x80) != 0))
			asm (" swbp 0");

		/* Check if MSMC address is 64 byte aligned */
		if ((((int)ptr & 0x0C000000) == 0x0C000000) && (((int)ptr % 0x40) != 0))
			asm (" swbp 0");
	}
#endif /* CACHE_ALIGNMENT_CHECK */

	/* Disable Interrupts */
	key = Hwi_disable();

	/* Writeback the contents of the cache. */
	CACHE_wbL1d (ptr, size, CACHE_FENCE_WAIT);

	asm	 (" nop	4");
	asm	 (" nop	4");
	asm	 (" nop	4");
	asm	 (" nop	4");

	/* Reenable Interrupts. */
	Hwi_restore(key);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the Fault Management module to writeback the cache.
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be written back
 *  @param[in]  size
 *      Size of the buffer to be written back
 *
 *  @retval
 *      None
 */
void Osal_fault_mgmtEndMemAccess(void* ptr, uint32_t size)
{
	Osal_endMemAccess(ptr, size, 1);
}

/* FUNCTION PURPOSE: Prints a variable list
 ***********************************************************************
 * DESCRIPTION: The function is used to print a string to the console
 */
void Osal_fault_mgmtLog (char *fmt, ... )
{
    VaList ap;
    
    va_start(ap, fmt);
    System_vprintf(fmt, ap);
    va_end(ap);
}

void *Osal_cppiMalloc(uint32_t num_bytes)
{
    return(NULL);
}

void Osal_cppiFree(void *ptr, uint32_t size)
{
}

/**
 * ============================================================================
 *  @n@b Osal_cppiCsEnter
 *
 *  @b  brief
 *  @n  This API ensures multi-core and multi-threaded
 *      synchronization to the caller.
 *
 *      This is a BLOCKING API.
 *
 *      This API ensures multi-core synchronization between
 *      multiple processes trying to access CPPI shared
 *      library at the same time.
 *
 *  @param[in]  
 *  @n  None
 *
 *  @return     
 *  @n  Handle used to lock critical section
 * =============================================================================
 */
Ptr Osal_cppiCsEnter (Void)
{
    return (NULL);
}

/**
 * ============================================================================
 *  @n@b Osal_cppiCsExit
 *
 *  @b  brief
 *  @n  This API needs to be called to exit a previously
 *      acquired critical section lock using @a Osal_cppiCsEnter ()
 *      API. It resets the multi-core and multi-threaded lock,
 *      enabling another process/core to grab CPPI access.
 *
 *  @param[in]  CsHandle
 *      Handle for unlocking critical section.
 *
 *  @return     None
 * =============================================================================
 */
Void Osal_cppiCsExit (Ptr CsHandle)
{

}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that a block of memory is 
 *      about to be accessed. If the memory block is cached then this 
 *      indicates that the application would need to ensure that the 
 *      cache is updated with the data from the actual memory.
 *
 *  @param[in]  blockPtr
 *       Address of the block which is to be invalidated
 *
 *  @param[in]  size
 *       Size of the block to be invalidated

 *  @retval
 *      Not Applicable
 */
void Osal_cppiBeginMemAccess (void *blockPtr, uint32_t size)
{    

}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that the block of memory has 
 *      finished being accessed. If the memory block is cached then the 
 *      application would need to ensure that the contents of the cache 
 *      are updated immediately to the actual memory. 
 *
 *  @param[in]  blockPtr
 *       Address of the block which is to be written back
 *
 *  @param[in]  size
 *       Size of the block to be written back

 *  @retval
 *      Not Applicable
 */
void Osal_cppiEndMemAccess (void *blockPtr, uint32_t size)
{

}

