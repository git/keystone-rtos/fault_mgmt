/******************************************************************************
 * FILE PURPOSE: Build description for the Fault Management
 ******************************************************************************
 * FILE NAME: package.bld
 *
 * DESCRIPTION: 
 *  This file contains the build specification and description for the 
 *  FM Library
 *  
 *  The file takes the following parameters from the command line through the
 *  XDCARGS variable.
 *      XDCARGS[0] = FM Install Type 
 *      Valid Values are "TAR" or "SETUP"
 *      DEFAULT is "SETUP"
 *
 *  Example for a valid command:
 *      xdc XDCARGS="SETUP" release   
 *
 * Copyright (C) 2012-2015, Texas Instruments, Inc.
 *****************************************************************************/

/* List of all subdirectories that combine to make the Fault Management Package. */
var subDirectories = ["src", "docs", "device", "include", "test", "util"];

/* Generate fault_mgmtlibver.h */
var tplt = xdc.loadTemplate("./fault_mgmtlibver.h.xdt");
tplt.genFile("./fault_mgmtlibver.h",lldReleaseVersion);   

/* Determine if we need to create the InstallJammer Application or not? 
 * Fault Management Deliverables be either of the following formats:
 *  - TAR Ball Package
 *  - Setup Executable 
 * DEFAULT is a TAR Executable. */

if ((arguments[0] != "TAR") && (arguments[0] != "SETUP"))
    lldInstallType = "TAR";
else
    lldInstallType = arguments[0];

/* Irrespective of the InstallType we always create a TAR Ball Package as a part
 * of the RTSC Build. Here we determine the name of the TAR Ball Package
 *  Format is as follows:
 *      lld_<version> */
var lldRTSCFileName = "fault_mgmtlib" + "_" + lldPartNumber + "_" +
                      lldReleaseVersion[0] + "_" +  lldReleaseVersion[1] + "_" + 
                      lldReleaseVersion[2]  + "_" + lldReleaseVersion[3];

/******************************************************************
 ************************ Release Banner **************************
 ******************************************************************/

print ("************* Fault Management Library Build Information *************");
print ("Fault Management Lib Install : " + lldInstallType);
print ("Fault Management Lib Version : " + lldReleaseVersion);
print ("C66 Tools Directory          : " + c66ToolsBaseDir);
print ("RTSC File Name	             : " + lldRTSCFileName);
print ("Fault Management Lib Path    : " + lldPath);
print ("CC LE opts                   : " + C66LE.ccOpts.prefix);
print ("***********************************************************");

/* Create the release package for the Fault Management Library */
Pkg.defaultRelease = Pkg.addRelease (lldRTSCFileName, {prefix: "./packages/"});

/* Moving forward we need to set the Archiver of the package to be ZIP. This is currently
 * not supported in the XDC tools being used. Currenly builds need to be done with the 
 * following options:-
 *   xdc MK_FIXLISTOPTS=-t release 
 * ZIP is a better option as it works natively with INSTALL Jammer and we can remove the
 * uncompression into a temporary directory. XDC Tools with xdc-rXX support the ZIP archiver. */
//Pkg.attrs = {archiver : "zip"};

/* Cycle through all the sub-directories and build all the files */
for (var i = 0; i < subDirectories.length; i++) 
{
    /* Load the capsule in the sub directory. */
    var caps = xdc.loadCapsule (subDirectories[i]+"/Module.xs");

    print ("Building directory " + subDirectories[i]);

    /* Build the capsule. */
    caps.modBuild();

    /* Package the module.xs files for building via package */
    Pkg.otherFiles[Pkg.otherFiles.length++] = subDirectories[i]+"/Module.xs";
}

/* Package the remaining files */
Pkg.otherFiles[Pkg.otherFiles.length++] = "config.bld";
Pkg.otherFiles[Pkg.otherFiles.length++] = "package.bld";
Pkg.otherFiles[Pkg.otherFiles.length++] = "package.xdc";
Pkg.otherFiles[Pkg.otherFiles.length++] = "package.xs";
Pkg.otherFiles[Pkg.otherFiles.length++] = "Settings.xdc";
Pkg.otherFiles[Pkg.otherFiles.length++] = "Settings.xdc.xdt";
Pkg.otherFiles[Pkg.otherFiles.length++] = "fault_mgmt.h";
Pkg.otherFiles[Pkg.otherFiles.length++] = "fault_mgmt_osal.h";
Pkg.otherFiles[Pkg.otherFiles.length++] = "fault_mgmtlibver.h";
Pkg.otherFiles[Pkg.otherFiles.length++] = "fault_mgmtlibver.h.xdt";
Pkg.otherFiles[Pkg.otherFiles.length++] = "docs/Doxyfile";
Pkg.otherFiles[Pkg.otherFiles.length++] = "docs/doxyfile.xdt";
Pkg.otherFiles[Pkg.otherFiles.length++] = "build/buildlib.xs";
Pkg.otherFiles[Pkg.otherFiles.length++] = "makefile";

/* Generate Users Manual Doxyfile */
var tplt = xdc.loadTemplate("./docs/doxyfile.xdt");
tplt.genFile("./docs/Doxyfile",lldReleaseVersion); 

/* Generate Settings.xdc */
var tplt = xdc.loadTemplate("./Settings.xdc.xdt");
tplt.genFile("./Settings.xdc",lldReleaseVersion); 

/* Check if we need to create the mini package? */
var miniBuild = java.lang.System.getenv("MINI_PACKAGE");

if (miniBuild == "ON")
{
    /***************************************************************************
     ********************************* MINI Package ****************************
     ***************************************************************************/
    /* Create the MINI RTSC Package */
    var libUtility = xdc.loadCapsule ("build/buildlib.xs");
    libUtility.createMiniPkg(lldRTSCFileName);
}

/********************************************************************* 
 *********************** INSTALL-JAMMER Support **********************
 * In order to create the InstallJammer Application; we need to UNTAR
 * the package into a temporary directory. This is required because 
 * currently the InstallJammer does not support the TAR Files and thus
 * creating an UNTAR of the file. So to work-around the problem we will
 * do the following in the EPILOGUE Section:-
 *  (a) Create a temporary directory called 'tmp'
 *  (b) UNTAR the package into 'tmp'
 *  (c) Run the INSTALL Jammer on 'tmp'
 *  (d) Remove the 'tmp' directory.
 *
 * This can be done only after the 'release' package has been created.
 * Thus all of this work is being done in the EPILOGUE.
 *********************************************************************/
if (lldInstallType == "SETUP")
{
    /* Create the Install Jammer Version Variable. This is used inside the 
     * MPI File to create the Final executable. 
     *  The format supported is as follows:-
     *   - setupwin32_fault_mgmtlib-<part_number>-<version>.exe 
     *      This is for Fault Management Libraries and Header files
     */
    var InstallJammerVersion = "-DVersion " + lldPartNumber + "_" + lldReleaseVersion[0] + "_" +
                               lldReleaseVersion[1] + "_" +  lldReleaseVersion[2]  + "_" + lldReleaseVersion[3];

    /* This is the location where the tmp directory is located; this is used as 
     * the input directory for the Install Jammer. */ 
    var PackageBaseDir = " -DPackageBaseDir " + lldPath + "./tmp";

    /* This is the location where the Fault Management will be installed by default. */
    var WinInstallDir = " -DWinInstallDir C:/ti/fault_mgmtlib" + "_" + 
                            lldPartNumber + "_" + 
                            lldReleaseVersion[0] + "_" +  lldReleaseVersion[1] + "_" +  
                            lldReleaseVersion[2]  + "_" + lldReleaseVersion[3];

    /* Create the actual EPILOGUE Section for the INSTALLER */
    Pkg.makeEpilogue += "release: install_application\n";
    Pkg.makeEpilogue += "install_application:\n";
    Pkg.makeEpilogue += "\t @echo -------------------------------------------------------\n";
    Pkg.makeEpilogue += "\t @echo Creating the Install\n";
    Pkg.makeEpilogue += "\t @echo -------------------------------------------------------\n";
    Pkg.makeEpilogue += "\t -$(MKDIR) tmp\n";
    Pkg.makeEpilogue += "\t -$(MKDIR) tmp/packages\n";
    Pkg.makeEpilogue += "\t -$(MKDIR) tmp/eclipse\n";
    Pkg.makeEpilogue += "\t -$(CP) -R eclipse tmp\n";
    Pkg.makeEpilogue += "\t tar -xf ./packages/" + lldRTSCFileName + ".tar" + " -Ctmp/packages \n";
    Pkg.makeEpilogue += "\t installjammer " + InstallJammerVersion + PackageBaseDir + WinInstallDir + 
                        " --output-dir packages/ --build install/fault_mgmtlib.mpi\n";
    Pkg.makeEpilogue += "\t -$(RMDIR) /S /Q tmp\n\n";
}

/* We need to clean after ourselves; extend the 'clean' target to take care of this. */
Pkg.makeEpilogue += "clean::\n";
Pkg.makeEpilogue += "\t -$(RM) docs/Doxyfile Settings.xdc fault_mgmtlibver.h\n";
Pkg.makeEpilogue += "\t -$(RM) makefile\n";
Pkg.makeEpilogue += "\t -$(RMDIR) docs/doxygen\n";
if (lldInstallType == "SETUP")
{
    Pkg.makeEpilogue += "\t -$(RM) packages/*.exe\n";
    Pkg.makeEpilogue += "\t -$(RM) packages/*.bin\n";
    Pkg.makeEpilogue += "\t -$(RMDIR) eclipse\n\n";
}
if (miniBuild == "ON")
{
    Pkg.makeEpilogue += "\t -$(RM) simpleC66LE.mak\n";
}
